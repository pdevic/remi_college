
def fib(int n) -> void {
  int a = 1, b = 1, c = 0

  println(a, endl, b)

  for(int i = 2; i <= n; i = i + 1) {
    c = a + b
    println(c)

    a = b
    b = c
  }
}

def main() -> void {

  fib(15)
}