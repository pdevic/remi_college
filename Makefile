
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

CXX=g++
CPPFLAGS=-std=c++17 -Wall -Wextra -g

SDIR=src
HDIR=$(SDIR)/header
ODIR=bin/obj
LDIR=$(SDIR)/lib
BDIR=bin

INC=-I$(HDIR) -I$(HDIR)/libraries
LDFLAGS=-lstdc++fs
LIBS=-L$(LDIR) $(LDFLAGS)

VPATH=$(SDIR):$(OBJDIR):$(LDIR)

vpath %.h $(HDIR)
vpath %.o $(ODIR)
vpath %.a $(LDIR)

INCLUDE=$(INC)
LIBRARIES=$(LIBS)

OBJECTS := $(wildcard $(ODIR)/*.o)

SRC=$(wildcard $(SDIR)/*.cpp)
SRC_NAMES=$(subst $(SDIR)/,,$(SRC))
OBJ_INTERMEDIATE=$(subst $(SDIR),$(ODIR),$(SRC))
OBJ=$(OBJ_INTERMEDIATE:.cpp=.o)
OBJ_NAMES=$(SRC_NAMES:.cpp=.o)

target: $(OBJ_NAMES)
	$(CXX) $(OBJ) -static $(LIBRARIES) -o $(BDIR)/remi.exe
	@echo "Building done"

%.o: %.cpp
	$(CXX) $(CPPFLAGS) $(INCLUDE) $< -c -o $(ODIR)/$@

.PHONY: clean
.PHONY: v
.PHONY: rebuild

clean:
	@for obj in $(OBJECTS) ; do \
    echo "Removing" $$obj ; \
		rm $$obj ; \
    done
	@echo "Cleanup done"

v:
	$(CXX) -v

rebuild:
	@echo "Removing existing obj files..."
	@make clean
	@echo "Building the project..."
	@make -j6