#ifndef REMI_SCOPE_SPACE_HPP
#define REMI_SCOPE_SPACE_HPP

#include <Remi/Scope.hpp>

#include <string>
#include <string_view>

#include <map>
#include <unordered_map>

namespace re {

    typedef std::unordered_map<size_t, std::unordered_map<size_t, std::unique_ptr<Scope>>> ScopeMap;

    class ScopeSpace {
    public:
        // Initialize root scope of the space to be a program block scope
        ScopeSpace(Scope* _rootParent);

        Scope* scope() noexcept;
        const Scope* scope() const noexcept;

        const std::map<std::string, Scope*>& scopes() const noexcept;
        const ScopeMap& scope_map() const noexcept;

        bool scope_exists(std::string_view _name) const;

        Scope* find_scope(std::string_view _name);
        Scope* operator[](std::string_view _name);

        const std::unordered_map<size_t, std::unique_ptr<Scope>>& operator[](size_t _depth) const;

        Scope* register_scope(Scope* _parent, ScopeType _type, std::string_view _name = "");

        // Create a new scope that uses the depth and id of the given parent
        // scope and generate a fake offset for the new scope
        Scope& generate_inline_anonymous_scope(Scope* _parent);

    private:
        Scope* m_scope;

        std::map<std::string, Scope*> m_namedScopes;
        ScopeMap m_scopeLevels;
        std::vector<std::unique_ptr<Scope>> m_anonymousScopes;
    };

}

#endif