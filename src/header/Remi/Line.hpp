#ifndef REMI_LINE_HPP
#define REMI_LINE_HPP

#include <Remi/Token.hpp>

#include <vector>

namespace re {

    class Line {
    public:
        Line() = delete;
        Line(size_t _number, size_t _relativeNumber, const std::string& _source);

        const std::string_view source() const noexcept;
        const std::vector<Token>& tokens() const noexcept;
    
        size_t number() const noexcept;
        size_t relative_number() const noexcept;

        const Token& operator[](size_t _pos) const;

    protected:
        friend class Tokenizer;

        void add_token(const Token& _token);
        Line& operator<<(const Token& _token);

    private:
        std::string m_source;
        std::vector<Token> m_tokens;

        size_t m_number;
        size_t m_relativeNumber;
    };
    
}

#endif