#ifndef REMI_REMI_HPP
#define REMI_REMI_HPP

#include <Remi/Default.hpp>
#include <Remi/Error.hpp>
#include <Remi/Clock.hpp>

#include <Remi/Time.hpp>
#include <Remi/Logger.hpp>

#include <Remi/ProgramArguments.hpp>

#include <Remi/Context.hpp>
#include <Remi/Script.hpp>

#include <string_view>
#include <memory>

namespace re {

    //
    // This class encapsulates the whole of a Remi process - reading, parsing,
    // and executing a script or running in interactive mode, managing all of  the
    // required components
    //
    // This class is constructed with console arguments
    // For more information on the available console options run Remi with the [-h] flag or check the [Default.cpp] file
    class Remi {
    public:
        Remi() = delete;
        Remi(int _argc, char* _argv[]);
        Remi(std::unique_ptr<ProgramArguments> _arguments);

        // Returns the reference to the start time of the re::Remi instance
        // Throws a re::ProgramException if the instnace wasn't started with start() beforehand
        const Time& start_time() const;

        const ProgramArguments& arguments() const noexcept;
        const Logger& logger() const;

        // Load the given script or enter interactive mode
        void start();

    private:

        // Init function only needs arguments if the m_arguments instance doesn't aleady exist
        void init(int _argc = 0, char* _argv[] = nullptr);
        void init_logger();

        // Reads, tokenizes, parses and loads the main script without running it
        // Provides time measurements for all of the operations for [--verbosity >= 2]
        void parse_main_script();

        // Runs the main script and provides time measurement for [--verbosity >= 2]
        void execute_main_script(); 
        
        void print_version() const;
        void print_help() const;

        void print_tutorial() const;

        // Print all the received program arguments and their values
        // Called in start before 'operations on the main script' and
        // 'interactive mode' for [verbosity = 4]
        void print_program_arguments() const;

        void print_keywords() const;
        void print_operators() const;

        void print_type_sizes() const;
        void print_basic_type_sizes() const;

        // Passes argc and argv to m_arguments (re::ProgramArguments) for parsing
        void parse_program_arguments(int _argc, char* _argv[]);
        
        // Uses m_arguments (re::ProgramArguments) to set the run mode
        void determine_run_mode();

        // Uses m_arguments (re::ProgramArguments) to set the verbosity
        void determine_verbosity();

        // Uses m_arguments (re::ProgramArguments) to return the logging mode
        LogMode determine_log_mode() const;

        void report_program_argument_lookup_exception(std::string_view _functionName, ProgramArgumentLookupException& _e) const;
        void report_program_argument_input_exception(std::string_view _functionName, ProgramArgumentInputException& _e) const;
        void report_program_argument_use_exception(std::string_view _functionName, ProgramArgumentUseException& _e) const;
        void report_program_argument_exception(std::string_view _functionName, ProgramArgumentException& _e) const;

        void report_script_exception(std::string_view _functionName, ScriptException& _e) const;
        void report_script_lookup_exception(std::string_view _functionName, ScriptLookupException& _e) const;

        void report_scope_lookup_exception(std::string_view _functionName, ScopeLookupException& _e) const;
        void report_scope_duplicate_exception(std::string_view _functionName, ScopeDuplicateException& _e) const;

        void report_variable_lookup_exception(std::string_view _variableName, VariableLookupException& _e) const;
        void report_function_lookup_exception(std::string_view _functionName, FunctionLookupException& _e) const;

    protected:
        friend class Parser;

        ProgramArguments& arguments() noexcept;

    private:
        std::unique_ptr<ProgramArguments> m_arguments;
        std::unique_ptr<Logger> m_logger;

        std::unique_ptr<Context> m_context;

        // Marks the time at which the instance of re::Remi was started
        std::unique_ptr<Time> m_startTime;

        // Determines how Remi will run
        RemiMode m_mode; 

        // Determines how much information is logged
        int m_verbosity;
    };

}

#endif