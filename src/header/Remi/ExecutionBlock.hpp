#ifndef REMI_EXECUTION_BLOCK_HPP
#define REMI_EXECUTION_BLOCK_HPP

#include <memory>
#include <vector>

#include <cstddef>

#include <iostream>

namespace re {

    class Action;

    class ExecutionBlock {
    public:
        ExecutionBlock(size_t _depth, size_t _id, size_t _callDepth, size_t _byteSize);

        inline void* data_block(size_t _byteOffset = 0) const {
            //std::cout << "Block request " << m_depth << ":" << m_id << ":" << m_callDepth << " | offset: " << _byteOffset << std::endl;
            return (void*)(&m_memoryBlock[_byteOffset]);
        }

        inline size_t byte_size() const { return m_byteSize; }

        inline size_t depth() const { return m_depth; }
        inline size_t id() const { return m_id; }
        inline size_t call_depth() const { return m_callDepth; }

        std::string coords_str() const;

        inline void* operator[](size_t _byteOffset) const {
            return data_block(_byteOffset);
        }

    private:
        std::unique_ptr<std::byte[]> m_memoryBlock;
        size_t m_byteSize;

        size_t m_depth;
        size_t m_id;

        size_t m_callDepth;
    };

}

#endif