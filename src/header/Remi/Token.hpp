#ifndef REMI_TOKEN_HPP
#define REMI_TOKEN_HPP

#include <Remi/Default.hpp>

#include <string_view>

namespace re {

    class Token {
    public:
        Token() = delete;
        Token(size_t _start, size_t _length, std::string_view _source);

        std::string_view str() const noexcept;

        size_t start() const noexcept;
        TokenType type() const noexcept;

        bool is_keyword() const;
        bool is_identifier() const;
        bool is_symbol() const;

        bool is_numeric() const;
        bool is_operator() const;

    private:
        TokenType decide_type();

    private:
        TokenType m_type;
        size_t m_start;

        std::string_view m_str;
    };

    bool operator==(const Token& _token, RemiKeyword _keyword);
    bool operator!=(const Token& _token, RemiKeyword _keyword);

    bool operator==(const Token& _token, RemiOperator _operator);
    bool operator!=(const Token& _token, RemiOperator _operator);

    bool operator==(const Token& _token, std::string_view _str);
    bool operator!=(const Token& _token, std::string_view _str);

    bool operator==(const Token& _token, char symbol);
    bool operator!=(const Token& _token, char symbol);
    
}

#endif