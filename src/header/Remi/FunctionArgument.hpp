#ifndef REMI_FUNCTION_ARGUMENT_PLAN_HPP
#define REMI_FUNCTION_ARGUMENT_PLAN_HPP

#include <Remi/VariablePlan.hpp>

#include <string_view>
#include <string>

namespace re {

    class FunctionArgument {
    public:
        FunctionArgument(std::string_view _name, VariablePlan _vplan);

        std::string_view name() const;
        const VariablePlan& variable_plan() const;

    private:
        std::string m_name;
        VariablePlan m_vPlan;
    };

}

#endif