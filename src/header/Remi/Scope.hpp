#ifndef REMI_PARSE_NODE_HPP
#define REMI_PARSE_NODE_HPP

#include <Remi/Default.hpp>

#include <Remi/VariablePlan.hpp>
#include <Remi/FunctionPlan.hpp>

#include <string>
#include <string_view>

#include <vector>
#include <map>

namespace re {

    class Scope {
    public:
        Scope() = delete;
        ~Scope();

        Scope* parent() const;
        ScopeType type() const;

        size_t depth() const;
        size_t id() const;
        size_t fake_offset() const;

        std::string coords_str() const;

        const std::map<std::string, VariablePlan*>& variables() const;
        const std::vector<VariablePlan*>& anonymous_variables() const;

        const std::map<std::string, FunctionPlan*>& functions() const;
        const std::vector<FunctionPlan*>& anonymous_functions() const;

        size_t variable_count() const;

        bool variable_exists(std::string_view _name, SearchType _searchType = SearchType::LocalInline) const;
        bool function_exists(std::string_view _name, SearchType _searchType = SearchType::LocalInline) const;

        VariablePlan& find_variable(std::string_view _name, SearchType _searchType = SearchType::LocalInline) const;
        FunctionPlan& find_function(std::string_view _name, SearchType _searchType = SearchType::LocalInline) const;

        VariablePlan& register_variable(std::string_view _name, VariablePlan&& _variablePlan);
        VariablePlan& register_anonymous_variable(VariablePlan&& _variablePlan);

        FunctionPlan& register_function(std::string_view _name, FunctionPlan&& _functionPlan);
        FunctionPlan& register_anonymous_function(FunctionPlan&& _functionPlan);

    protected:
        friend class ScopeSpace;
        friend class Parser;

        Scope(Scope* _parent, ScopeType _type);

        void set_depth(size_t _depth);
        void set_id(size_t _id);
        void set_fake_offset(size_t _fakeOffset);
        void increase_fake_offset(size_t _fakeOffset);

    private:
        Scope* m_parent;
        ScopeType m_type;

        size_t m_depth;
        size_t m_id;
        size_t m_fakeOffset;

        std::map<std::string, VariablePlan*> m_variables;
        std::vector<VariablePlan*> m_anonymousVariables;

        std::map<std::string, FunctionPlan*> m_functions;
        std::vector<FunctionPlan*> m_anonymousFunctions;
    };

    bool operator==(const Scope& _left, const Scope& _right);
}

#endif