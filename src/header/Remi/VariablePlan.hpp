#ifndef REMI_VARIABLE_PLAN_HPP
#define REMI_VARIABLE_PLAN_HPP

#include <Remi/Default.hpp>

namespace re {

    class Scope;

    class VariablePlan {
    public:
        VariablePlan(const Scope& _scope, Constness _referenceConstness, ReferenceType _refType, Constness _valueConstness, ValueType _valueType, size_t _typeID, size_t _byteOffset = 0);
        VariablePlan(const VariablePlan& _source);

        static VariablePlan uninitialized_variable(const Scope& _scope);
        static VariablePlan any_type(const Scope& _scope);
        static VariablePlan void_variable(const Scope& _scope);

        static VariablePlan normal_variable(const Scope& _scope, Constness _constness, ValueType _vType, size_t _typeID, size_t _byteOffset = 0);

        static VariablePlan const_value_type(const Scope& _scope, size_t _typeID, size_t _byteOffset = 0);
        static VariablePlan variable_reference(const Scope& _scope, Constness _constness, const VariablePlan& _var);
        static VariablePlan const_variable_reference(const Scope& _scope, Constness _constness, const VariablePlan& _var);

        const Scope& scope() const;

        Constness reference_constness() const;
        ReferenceType reference_type() const;

        Constness value_constness() const;
        ValueType value_type() const;

        size_t type_id() const;
        size_t byte_offset() const;

        bool can_equate(const VariablePlan& _other) const;

    protected:
        friend class Scope;
        friend class Parser;
        friend class FunctionTypePlan;

        VariablePlan();

        void set_type_id(size_t _id);
        void set_byte_offset(size_t _offset);

        void set_scope_reference(const Scope* _scope);

    private:
        const Scope* m_scope;

        Constness m_refConstness;
        ReferenceType m_refType;

        Constness m_valueConstness;
        ValueType m_valueType;
        size_t m_typeID;

        size_t m_byteOffset;
    };

    bool operator==(const VariablePlan& _l, const VariablePlan& _r);
    bool operator!=(const VariablePlan& _l, const VariablePlan& _r);

}

#endif