#ifndef REMI_TOKENIZER_HPP
#define REMI_TOKENIZER_HPP

#include <Remi/Line.hpp>

#include <vector>

namespace re {

    class Tokenizer {
    public:
        Tokenizer() = delete;
        Tokenizer(std::vector<Line>& _lines) noexcept;

        void tokenize(bool _printTokens = false);

    private:
        LexingState tokenize_line(Line& _line, LexingState _state);

        inline static bool is_divider(char c);
        inline static bool is_operator_symbol(char c);

    private:
        std::vector<Line>& m_lines;
    };

}

#endif