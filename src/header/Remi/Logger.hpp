#ifndef REMI_LOGGER_HPP
#define REMI_LOGGER_HPP

#include <Remi/Default.hpp>

#include <string_view>
#include <string>

#include <sstream>
#include <fstream>

#include <mutex>

namespace re {

    class Logger {
    public:
        Logger() = delete;
        Logger(LogMode _mode);
        Logger(LogMode _mode, std::string_view _filename);

        LogMode mode() const;
        LogMode set_mode(LogMode _mode);

        void set_filename(std::string_view _filename);

        void log(LogMode _mode, const std::string& _str) const;
        void log(const std::string& _str) const;

        void file_log(const std::string& _str) const;

        static void print(const std::string& _str);
        static void println(const std::string& _str);

        void clear_file() const;

    private:
        // Throws a re::ProgramException if the logger fails to find its file
        void open_file(std::ofstream& _file, std::ios::openmode _option = std::ios::app) const;

    private:
        LogMode m_mode;
        std::string m_filename;
    };

    class LogStream : public std::ostringstream {
    public:
        LogStream(const Logger& _logger);
        ~LogStream();

        void log();

    private:
        const Logger& m_logger;
    };

}

#endif