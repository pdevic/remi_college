#ifndef REMI_DEFAULT_HPP
#define REMI_DEFAULT_HPP

#include <string>

#include <map>
#include <unordered_map>

#include <Remi/ProgramArgumentMeta.hpp>

namespace re {

    #define REMI_VERSION_MAJOR 0
    #define REMI_VERSION_MINOR 0
    #define REMI_VERSION_BUILD 0

    #define REMI_DEVELOPMENT true

    #if REMI_DEVELOPMENT
        #define REMI_DEBUG
    #else
        #define REMI_RELEASE
    #endif

    #define REMI_PRINT_EXEC_TIME true
    #define REMI_PRINT_ERROR_CLASS_INFO false

    #define REMI_PRINT_EXCEPTION_STACK true
    #define REMI_PRINT_CODE_SOURCE_INFO false
    #define REMI_PRINT_BLOCK_INIT false

    class RemiVersion {
        public:
            RemiVersion() = delete;

            // Returns the version of Remi in the following format: "major().minor().build()"
            // Eg. "0.0.0" or "1.2.0"
            static std::string version();

            static size_t major() noexcept;
            static size_t minor() noexcept;
            static size_t build() noexcept;

        private:
            // Returns the Remi version string in the "M.m.b" format
            static std::string generateVersion();

        private:
            static constexpr size_t ms_majorVersion{REMI_VERSION_MAJOR};
            static constexpr size_t ms_minorVersion{REMI_VERSION_MINOR};
            static constexpr size_t ms_buildVersion{REMI_VERSION_BUILD};
    };

    // Enum for release models
    // Models are Release, Debug and Development
    enum class RemiModel {
        Release,
        Debug,
        Development
    };

    // Enum for platform names
    // Eg. Linux and Win32
    enum class RemiPlatform {
        Win64,
        Win32,
        Linux,
        Android,
        Apple
    };

    // Enum for compiler names
    enum class RemiCompiler {
        Msc,
        MinGW64,
        MinGW32,
        Gcc,
        Clang
    };

    const RemiModel RemiCurrentModel {
        #ifdef REMI_DEVELOPMENT
            RemiModel::Development
        #elif REMI_DEBUG
            RemiModel::Debug
        #elif REMI_RELEASE
            RemiModel::Release
        #endif
    };

    const RemiPlatform RemiCurrentPlatform {
        #ifdef _WIN64
            RemiPlatform::Win64
        #elif _WIN32
            RemiPlatform::Win32
        #elif __linux__ && !__ANDROID__
            RemiPlatform::Linux
        #elif __ANDROID__
            RemiPlatform::Android
        #elif __APPLE__
            RemiPlatform::Apple
        #endif
    };

    const RemiCompiler RemiCurrentCompiler {
        #ifdef _MSC_VER
            RemiCompiler::Msc
        #elif __MINGW64__
            RemiCompiler::MinGW64
        #elif __MINGW32__
            RemiCompiler::MinGW32
        #elif __GNUC__
            RemiCompiler::Gcc
        #elif __clang__
            RemiCompiler::Clang
        #endif
    };

    // Returns the name of the release model as a string
    extern std::string remi_model_name(RemiModel _model);
    // Returns the name of the platform name as a string
    extern std::string remi_platform_name(RemiPlatform _platform);
    // Returns the name of the compiler name as a string
    extern std::string remi_compiler_name(RemiCompiler _compiler);

    // The name of the current release model
    extern const std::string& RemiModelName;
    // The name of the platform Remi is compiled for
    extern const std::string& RemiPlatformName;
    // The name of the compiler Remi was compiled with
    extern const std::string& RemiCompilerName;

    // Returns the version of Remi, platform name and release model name as a string
    // Format: Remi X.X.X PlatformName [ReleaseModelName]
    extern std::string remi_release_str();
    // Returns the version (without the name) of the compiler used to compile Remi
    extern std::string remi_compiler_version_str();

    // Match map used to look up an argument name using the associated flag
    const std::unordered_map<std::string, std::string> RemiProgramArgumentFlags { // [1]
        {"d",   "usage_demo"             },
        {"l",   "load_script"            },
        {"h",   "help"                   },
        {"v",   "version"                },
        {"V",   "verbosity"              },
        {"Lf",  "log_file"               },
        {"L",   "log_mode"               },
        {"T",   "print_tokens"           },
        {"K",   "print_keywords"         },
        {"op",  "print_operators"        },
        {"bts", "print_basic_type_sizes" },
        {"ts",  "print_type_sizes"       },
        {"p",   "program_variables"      }
    };

    // Match map used to look up an argument flag using the associated name
    extern const std::unordered_map<std::string, std::string> RemiProgramArgumentNames; // [2]

    // Match map used to look up argument meta data using the associated flag
    extern const std::map<std::string, ProgramArgumentMeta> RemiProgramArguments; // [3]

    //
    // Returns the flag associated with the program argument name
    // Throws a re::ProgramArgumentLookupException if the lookup fails
    extern std::string_view remi_program_argument_flag(const std::string& _argumentName);

    //
    // Returns the name associated with the program argument flag
    // Throws a re::ProgramArgumentLookupException if the lookup fails
    extern std::string_view remi_program_argument_name(const std::string& _flagName);

    //
    // Used by the re::Remi class. The mode in which Remi will run is determined
    // by the arguments provided through the commandline
    enum class RemiMode {
        Version,            // Output program version and exit
        Help,               // Output program argument details and exit
        LoadAndRun,         // Load a script and run it. Exit when the script is done
        InvalidRunMode      // Throw an error if re::Remi is started
    };

    enum class LogMode {
        Console,
        File,
        ConsoleAndFile
    };

    //
    // Status flags for the re::Script class
    // Used to determine if a script instance is in the right state for the requested operation
    enum class RemiScriptStatus {
        Uninitialized,
        Initialized,
        Read,
        Tokenized,
        Parsed
    };

    //
    // Returns the string representation of a RemiScriptStatus enum element
    extern std::string remi_script_status_name(RemiScriptStatus _status);

    //
    // Lexing states are used to determine which characters to accept and which to discard during script line cleanup and during tokenization
    enum class LexingState {
        Normal,
        String,
        Comment
    };

    enum class TokenType {
        None,

        Number,
        Digit,

        Keyword,
        Operator,

        String,
        Letter,

        Symbol
    };

    extern std::string_view remi_token_type_str(TokenType _type);

    const std::string RemiSymbols{".,:;+-*/$%=!?(){}[]<>%\"\'\\|&^@"};
    const std::string RemiOperatorSymbols{"+-*/%<>=!&|:^"};

    const std::string RemiCommentSingle{"//"};
    const std::string RemiCommentMultiBegin{"/*"};
    const std::string RemiCommentMultiEnd{"*/"};

    enum class RemiKeyword {
        Void,
        Bool,
        Char,
        Int,
        Float,

        Const,
        Def,

        True,
        False,

        If,
        Else,
        Then,
        Return,

        While,
        Do,
        For,
        In,

        Load,
    };

    enum class RemiOperator {
        Add,
        Subtract,
        Multiply,
        Divide,

        Modulo,
        Increment,
        Decrement,
        Imply,
        Define,

        Less,
        LessOrEqual,
        Greater,
        GreaterOrEqual,

        And,
        Or,

        Assign,
        AddAssign,
        SubtractAssign,
        MultiplyAssign,
        DivideAssign,

        Equal,
        Not,
        NotEqual
    };

    enum class ScopeType {
        ProgramBlock,
        FunctionBlock,
        InlineBlock
    };

    enum class SearchType {
        Deep,
        LocalStrict,
        LocalInline,
        InlineStrict
    };

    enum class ValueType {
        Value,
        Pointer,
        Array
    };

    enum class ReferenceType {
        Value,
        Reference
    };

    enum class Constness {
        Mutable,
        Const
    };

    enum class AdvanceState {
        Normal,
        LineEnd,
        ScriptEnd
    };

    enum class VariableReferenceModel {
        Dynamic,
        Static,
        OnCaller,
        OnCalled
    };

#define REMI_UNINITIALIZED_TYPE     0u
#define REMI_ANY_TYPE               1u
#define REMI_VOID_TYPE              2u

#define REMI_BOOL_TYPE              3u
#define REMI_CHAR_TYPE              4u
#define REMI_INT_TYPE               5u
#define REMI_FLOAT_TYPE             6u
#define REMI_DOUBLE_TYPE            7u

#define REMI_FUNCTION_TYPE          10u
#define REMI_COMPOUND_TYPE          11u
#define REMI_FREE_TYPE_ID           (REMI_COMPOUND_TYPE + 1)

    extern const std::map<std::string, RemiKeyword> RemiKeywords;
    extern const std::map<std::string, RemiOperator> RemiOperators;

    extern std::string_view remi_keyword_str(RemiKeyword _keyword);
    extern std::string_view remi_operator_str(RemiOperator _operator);
    extern RemiOperator remi_find_operator(std::string_view _str);

    extern bool remi_is_arithmetic_operator(RemiOperator _operator);
    extern bool remi_is_logic_operator(RemiOperator _operator);
    extern bool remi_is_binary_operator(RemiOperator _operator);
    extern bool remi_is_assign_variant(RemiOperator _operator);

    extern bool remi_is_identifier(std::string_view _str);

    template<class T>
    T max(T _rv, T _lv) {
        return (_rv > _lv) ? _rv : _lv;
    }

}

#endif