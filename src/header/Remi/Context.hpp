#ifndef REMI_CONTEXT_HPP
#define REMI_CONTEXT_HPP

#include <Remi/Logger.hpp>
#include <Remi/Script.hpp>

#include <Remi/Scope.hpp>
#include <Remi/ScopeSpace.hpp>

#include <memory>
#include <string>
#include <vector>

namespace re {

    class Context {
    public:
        // Initialize the context setting the main scope parent to nullptr
        Context(const Logger& _logger, size_t _typeCounter = REMI_FREE_TYPE_ID);

        Scope* root() noexcept;
        const Scope* root() const noexcept;

        const std::unordered_map<std::string, std::unique_ptr<Script>>& scripts() const;

        ScopeSpace& scope_space() noexcept;
        const ScopeSpace& scope_space() const noexcept;

        const Logger& logger() const;
        const std::vector<FunctionPlan*>& init_functions() const;

        size_t type_counter() const;
        const std::map<std::string, size_t>& types_by_name() const;

        bool script_exists(std::string_view _name) const;
        Script& find_script(std::string_view _name);
        const Script& find_script(std::string_view _name) const;

        bool type_exists(size_t _typeID) const;
        bool type_exists(std::string_view _typeName) const;

        std::string find_type_name(size_t _typeID) const;
        std::string find_type_name(const VariablePlan& _var) const;
        size_t find_type_id(std::string_view _typeName) const;

        std::string type_prefix(ValueType _vType) const;
        std::string type_prefix(const VariablePlan& _var) const;

        // Generate full type name for a variable - this includes the keyword const
        std::string full_type_name(const VariablePlan& _var) const;
        std::string full_function_declaration(std::string_view _fName, const FunctionPlan& _fun);

        size_t get_type_byte_size(size_t _typeID) const;
        size_t get_scope_byte_size(const Scope& _scope) const;

        // Create and register a script then read its file and tokenize it
        Script& create_script(Script* _parentScript, std::string_view _filename);

        void register_type(std::string_view _typeName, size_t _typeID = REMI_UNINITIALIZED_TYPE);

        static bool is_numeric_type(size_t _typeID);

    protected:
        friend class Parser;
        friend class Remi;

        std::vector<FunctionPlan*>& init_functions();
        void register_init_function(FunctionPlan& _f);

    private:
        std::unique_ptr<ScopeSpace> m_scopeSpace;
        std::unordered_map<std::string, std::unique_ptr<Script>> m_scripts;

        const Logger& m_logger;

        std::vector<Scope*> m_dataScopes;
        std::vector<FunctionPlan*> m_initFunctions;

        std::unordered_map<size_t, std::string> m_typesByID;
        std::map<std::string, size_t> m_typesByName;
        std::map<size_t, size_t> m_typeByteSizes;

        size_t m_typeCounter;
    };

}

#endif