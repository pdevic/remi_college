#ifndef REMI_ACTION_HPP
#define REMI_ACTION_HPP

#include <Remi/Error.hpp>

#include <Remi/ExecutionContext.hpp>
#include <Remi/VariablePlan.hpp>

#include <cstddef>

#include <string_view>
#include <string>

#include <tuple>
#include <vector>

#include <functional>

namespace re {

    class Action {
    public:
        Action();
        virtual ~Action();

        virtual Action* clone() const = 0;
        virtual int execute(ExecutionContext& _context) = 0;
    };



    class VariableReference {
    public:
        VariableReference(const VariableReference& _source);

        static VariableReference dynamic_reference(const VariablePlan& _source);
        static VariableReference static_reference(const VariablePlan& _source);
        static VariableReference on_current_caller(const VariablePlan& _source);
        static VariableReference on_current_called(const VariablePlan& _source);

        size_t depth() const noexcept;
        size_t id() const noexcept;
        size_t byte_offset() const noexcept;

        std::string coords_str() const;

        inline void* get_ptr(ExecutionContext& _context) const {
            return (this->*m_ptrFunction)(_context);
        }

        inline void* get_ptr_dynamic(ExecutionContext& _context) const { return _context.current_function().block()[m_byteOffset]; }
        inline void* get_ptr_static(ExecutionContext& _context) const { return _context.resolve(m_depth, m_id, 0)[m_byteOffset]; }
        inline void* get_ptr_on_current_caller(ExecutionContext& _context) const { return _context.current_caller_function().block()[m_byteOffset]; }
        inline void* get_ptr_on_current_called(ExecutionContext& _context) const { return _context.current_called_function().block()[m_byteOffset]; }

    protected:
        friend class Parser;

        VariableReference(const VariablePlan& _source, VariableReferenceModel _model);

    protected:
        size_t m_depth;
        size_t m_id;
        size_t m_byteOffset;
        void* (VariableReference::*m_ptrFunction)(ExecutionContext& _context) const;
    };



    template<typename T>
    class ActionCloneBase : public Action {
    public:
        using Action::Action;
        //ActionCloneBase(const ActionCloneBase<T>& _source);

        Action* clone() const;
    };

    template<class T>
    class ActionWriteConst : public ActionCloneBase<ActionWriteConst<T>> {
    public:
        ActionWriteConst(VariableReference&& _target, T _data);
        //ActionWriteConst(const ActionWriteConst<T>& _source);

        int execute(ExecutionContext& _context) override;
        const T& data() const;

    private:
        VariableReference m_target;
        T m_data;
    };

    class ActionCopyValue : public ActionCloneBase<ActionCopyValue> {
    public:
        ActionCopyValue(VariableReference&& _source, VariableReference&& _target, size_t _byteSize);
        //ActionCopyValue(const ActionCopyValue& _source);

        const VariableReference& source() const;
        const VariableReference& target() const;
        size_t byte_size() const;

        inline int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_source;
        VariableReference m_target;
        size_t m_byteSize;
    };

    class ActionExecuteActions : public ActionCloneBase<ActionExecuteActions> {
    public:
        ActionExecuteActions() = default;
        ActionExecuteActions(const ActionExecuteActions& _source);
        ActionExecuteActions(std::vector<Action*>&& _actions);
        ActionExecuteActions(const std::vector<Action*>& _actions);
        ~ActionExecuteActions();

        inline int execute(ExecutionContext& _context) override {
            for(Action* p : m_actions) {
                if (p->execute(_context)) return 1;
            }

            return 0;
        }

        const std::vector<Action*> actions() const noexcept;
        void add_action(Action* _action);

    protected:
        std::vector<Action*> m_actions;
    };

    class ActionExpression : public ActionCloneBase<ActionExpression> {
    public:
        ActionExpression(const VariablePlan& _return, std::vector<Action*>&& _actions);
        ActionExpression(const ActionExpression& _source);

        VariablePlan& return_target() noexcept;
        const VariablePlan& return_target() const noexcept;

        const ActionExecuteActions& executer() const noexcept;
        const std::vector<Action*> actions() const noexcept;

        int execute(ExecutionContext& _context) override;
        void add_action(Action* _action);

    private:
        VariablePlan m_return;
        ActionExecuteActions m_executeActions;
    };



    template<class T>
    class ActionPrintVariable : public ActionCloneBase<ActionPrintVariable<T>> {
    public:
        ActionPrintVariable(VariableReference&& _source);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_ref;
    };

    class ActionPrintEndl : public ActionCloneBase<ActionPrintEndl> {
    public:
        int execute(ExecutionContext& _context) override;
    };

    class ActionPrintFlush : public ActionCloneBase<ActionPrintFlush> {
    public:
        int execute(ExecutionContext& _context) override;
    };



    class ActionWriteConstStringPtr : public ActionCloneBase<ActionWriteConstStringPtr> {
    public:
        ActionWriteConstStringPtr(VariableReference&& _varRef, std::string_view _str);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_ref;
        std::string m_str;
    };



    template<class T1, class T2>
    class ActionCastValue : public ActionCloneBase<ActionCastValue<T1, T2>> {
    public:
        ActionCastValue(VariableReference&& _from, VariableReference&& _to);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_from;
        VariableReference m_to;
    };

    template<class T>
    class ActionSpecialBoolCast : public ActionCloneBase<ActionSpecialBoolCast<T>> {
    public:
        ActionSpecialBoolCast(VariableReference&& _from, VariableReference&& _to);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_from;
        VariableReference m_to;
    };

    class ActionInvertBool : public ActionCloneBase<ActionInvertBool> {
    public:
        ActionInvertBool(VariableReference&& _var, VariableReference&& _writeTarget);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_var;
        VariableReference m_writeTarget;
    };




    template<class T, bool ___positive>
    class ActionIncrementValue : public ActionCloneBase<ActionIncrementValue<T, ___positive>> {
    public:
        ActionIncrementValue(VariableReference&& _writeTarget);
        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_writeTarget;        
    };

    template<class T, RemiOperator op>
    class ActionBinaryOperation : public ActionCloneBase<ActionBinaryOperation<T, op>> {
    public:
        ActionBinaryOperation(VariableReference&& _varRefLeft, VariableReference&& _varRefRight, VariableReference&& _refReturn);

        const VariableReference& right_ref() const;
        const VariableReference& left_ref() const;
        const VariableReference& return_ref() const;

        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_refLeft;
        VariableReference m_refRight;
        VariableReference m_refReturn;
    };



    class ActionIfStatement : public ActionCloneBase<ActionIfStatement> {
    public:
        ActionIfStatement();
        ActionIfStatement(const ActionIfStatement& _source);
        ~ActionIfStatement();

        const std::vector<std::tuple<VariableReference, ActionExpression*, ActionExecuteActions*>>& conditional_actions() const;
        const ActionExecuteActions& final_else() const;

        void add_conditional_action(VariableReference&& _conditionRef, ActionExpression* conditionExpr, ActionExecuteActions* _actions);
        void set_final_else(ActionExecuteActions* _actions);

        int execute(ExecutionContext& _context) override;

    private:
        std::vector<std::tuple<VariableReference, ActionExpression*, ActionExecuteActions*>> m_conditionalActions;
        ActionExecuteActions* m_finalElse;
    };



    class ActionWhileLoop : public ActionCloneBase<ActionWhileLoop> {
    public:
        ActionWhileLoop(VariableReference&& _conditionRef, ActionExpression* _conditionExpression, ActionExecuteActions* _actions);
        ActionWhileLoop(const ActionWhileLoop& _source);
        ~ActionWhileLoop();

        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_conditionRef;
        ActionExpression* m_conditionExpression;
        ActionExecuteActions* m_actions;
    };

    class ActionDoWhileLoop : public ActionCloneBase<ActionDoWhileLoop> {
    public:
        ActionDoWhileLoop(VariableReference&& _conditionRef, ActionExpression* _conditionExpression, ActionExecuteActions* _actions);
        ActionDoWhileLoop(const ActionDoWhileLoop& _source);
        ~ActionDoWhileLoop();

        int execute(ExecutionContext& _context) override;

    private:
        VariableReference m_conditionRef;
        ActionExpression* m_conditionExpression;
        ActionExecuteActions* m_actions;
    };



    class ActionFunctionCall : public ActionCloneBase<ActionFunctionCall> {
    public:
        ActionFunctionCall(const FunctionPlan* _fPlan, size_t _blockSize, ActionExecuteActions* _initValues);
        ActionFunctionCall(const ActionFunctionCall& _source);
        ~ActionFunctionCall();

        void set_return_target(ActionExecuteActions* _action);

        int execute(ExecutionContext& _context) override;

    private:
        const FunctionPlan* m_fPlan;
        ActionExecuteActions* m_returnActions;

        //ActionPrepareBlock m_prepareBlock;
        ActionExecuteActions* m_initValues;
    };

    class ActionAbortFunctionCall : public ActionCloneBase<ActionAbortFunctionCall> {
    public:
        ActionAbortFunctionCall();
        //ActionAbortFunctionCall(const ActionAbortFunctionCall& _source);

        int execute(ExecutionContext& _context) override;
    };

}

#endif