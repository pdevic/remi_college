#ifndef REMI_FUNCTION_INSTANCE_HPP
#define REMI_FUNCTION_INSTANCE_HPP

#include <Remi/VariablePlan.hpp>
#include <Remi/FunctionPlan.hpp>

#include <string>
#include <vector>

namespace re {

    class ExecutionBlock;
    class ExecutionContext;

    class FunctionInstance {
    public:
        FunctionInstance(const FunctionPlan& _source);
        FunctionInstance(const FunctionPlan& _source, const ExecutionBlock& _block);
        //~FunctionInstance();

        inline const ExecutionBlock& block() const;
        inline const FunctionPlan& source_plan() const;

        void execute(ExecutionContext& _context);

        void set_execution_block(const ExecutionBlock& _block);
        //void set_caller_function(FunctionInstance* _callerFunction);
        //void stop() noexcept;

    private:
        const ExecutionBlock* m_block;
        const FunctionPlan* m_source;

        //std::vector<Action*> m_actions;
    };

}

#include <Remi/FunctionInstance.hxx>

#endif