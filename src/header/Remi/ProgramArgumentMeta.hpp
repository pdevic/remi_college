#ifndef REMI_PROGRAM_ARGUMENT_META_HPP
#define REMI_PROGRAM_ARGUMENT_META_HPP

#include <string>
#include <string_view>

#include <vector>

namespace re {

    class ProgramArgumentMeta {
    public:
        ProgramArgumentMeta(std::string_view _flag, std::string_view _name, std::string_view _description, std::vector<std::string>&& _arguments);

        std::string_view flag() const noexcept;
        std::string_view name() const noexcept;
        std::string_view description() const noexcept;

        const std::vector<std::string>& arguments() const noexcept;

    private:
        std::string m_flag;
        std::string m_name;
        std::string m_description;

        std::vector<std::string> m_arguments;
    };

}

#endif