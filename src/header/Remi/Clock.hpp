#ifndef REMI_CLOCK_HPP
#define REMI_CLOCK_HPP

#include <chrono>

namespace re {

    // Measures elapsed time. Starts measuring when constructed
    // Use restart() to get elapsed time in precise seconds and start measuring again
    class Clock {
    public:
        Clock();

        // Returns the elapsed time and starts measuring time from 0
        double restart();

        // Returns the amount of seconds passed since last restart or construction if the clock was never restarted
        // Returns a std::chrono::duration<double>
        // Use seconds() to get the amount of elapsed seconds
        std::chrono::duration<double> elapsed() const;

        // Returns the precise amount of seconds passed since last restart
        double seconds() const;

    private:
        // Used internally by the constructor and restart()
        inline void start();

    private:
        std::chrono::system_clock::time_point m_start;
    };

}

#endif
