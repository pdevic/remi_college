#ifndef REMI_ERROR_HPP
#define REMI_ERROR_HPP

#include <Remi/Default.hpp>

#include <string_view>
#include <string>

#include <vector>
#include <memory>

namespace re {

    //
    // This file contains classes used for throwing exceptions. All specialized exception
    // classes derive from the base re::Exception class. This base class is meant to be
    // used exclusively as a base for specialized exception classes. Only specialized
    // classes are thrown
    //
    // All of the specialized exception classes are caught by Remi classes except for
    // re::ProgramException which is caught by the main() function

    //
    // These forward declarations are required for passing class references through exceptions
    // The class references passed are used to get and display valuable data about the class
    // that raised the error

    class Line;
    class Script;

    class ProgramArguments;
    class ProgramArgumentMeta;

    class Context;

    class Scope;
    class ScopeSpace;

    class Line;

    class Exception;

    class ExceptionStack {
    public:
        ExceptionStack() = delete;
        ExceptionStack(std::string_view _name);

        void add_name(const Exception& _e, std::string_view _name);
        const std::vector<std::string>& names() const noexcept;

        size_t depth() const;

    private:
        std::vector<std::string> m_names;
    };

    //
    // Base exception class. All other Remi exceptions derive from this class
    class Exception {
    public:
        Exception(std::unique_ptr<ExceptionStack> _stack, std::string_view _what);
        Exception(Exception& _e);

        std::string_view what() const noexcept;

        virtual std::string_view name() const noexcept;

        ExceptionStack& exception_stack();
        const ExceptionStack& exception_stack() const;

        std::unique_ptr<ExceptionStack>& exception_stack_ptr();
        const std::unique_ptr<ExceptionStack>& exception_stack_ptr() const;

    protected:
        std::string m_what;
        std::unique_ptr<ExceptionStack> m_esPtr;
    };

    //
    // re::ProgramException is used as a fatal exception caught outside of re::Remi. If Remi
    // encounters errors after which it cannot continue working it will throw this exception
    class ProgramException : public Exception {
    public:
        using Exception::Exception;

        std::string_view name() const noexcept override;
    };

    class ProgramArgumentException : public Exception {
    public:
        ProgramArgumentException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _ref, std::string_view _what);
        const ProgramArguments& ref() const;

        std::string_view name() const noexcept override;

    private:
        const ProgramArguments& m_ref;
    };

    //
    // This exception is thrown when an instance of re::ProgramArguments encounters an error 
    class ProgramArgumentInputException : public ProgramArgumentException {
    public:
        ProgramArgumentInputException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _ref, std::string_view _what, std::string_view _input);
        std::string_view input() const;

        std::string_view name() const noexcept override;

    private:
        std::string m_input;
    };

    //
    // This exception is thrown when a program argument name or flag was searched for but it was never defined
    // Used by the lookup function from <Remi/Default.hpp>
    class ProgramArgumentLookupException : public Exception {
    public:
        ProgramArgumentLookupException(std::unique_ptr<ExceptionStack> _stack, std::string_view _requested, std::string_view _what);            
        std::string_view requested() const;

        std::string_view name() const noexcept override;

    private:
        std::string m_requested;
    };

    //
    // This class is a specialization of the re::ProgramArgumentException class
    // Thrown when a flag is passed without proper parameters
    class ProgramArgumentUseException : public ProgramArgumentInputException {
    public:
        ProgramArgumentUseException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _caRef, const ProgramArgumentMeta& _ref, std::string_view _what, std::string_view _input);
        const ProgramArgumentMeta& ref() const;

        std::string_view name() const noexcept override;

    private:
        const ProgramArgumentMeta& m_ref;
    };

    //
    // Thrown by the re::Script class when it catches lexing, parsing or loading exceptions
    //
    // Caught by re::Remi::Remi (constructor)
    // Can be caught by other Script instances to indicate script inclusion and form an error chain
    class ScriptException : public Exception{
    public:
        ScriptException(std::unique_ptr<ExceptionStack> _stack, const Script& _ref, std::string_view _what);
        const Script& ref() const noexcept;

        std::string_view name() const noexcept override;

    private:
        const Script& m_ref;
    };

    class ScriptLookupException : public Exception {
    public:
        ScriptLookupException(std::unique_ptr<ExceptionStack> _stack, const Context& _ref, std::string_view _scriptName, std::string_view __what);
        const Context& ref() const noexcept;

        std::string_view script_name() const noexcept;
        std::string_view name() const noexcept override;

    private:
        const Context& m_ref;
        std::string m_scriptName;
    };

    /*class ScriptStatusException : public ScriptException {
        ScriptStatusException(const Script& _ref, RemiScriptStatus _expectedStatus, std::string_view _what);
        RemiScriptStatus expected_status() const noexcept;

    private:
        RemiScriptStatus m_expectedStatus;
    };*/

    //
    // Thrown by re::CodeSource::read() when it fails to open a file
    // Caught by re::Script::read()
    class CodeSourceException : public Exception {
    public:
        using Exception::Exception;

        std::string_view name() const noexcept override;
    };

    //
    // Thrown by re::CodeSource when a comment or string wasn't terminated and reached the end of the file
    // Thrown in re::CodeSource::read, caught by re::Script
    class LexingException : public Exception {
    public:
        LexingException(std::unique_ptr<ExceptionStack> _stack, const Line& _ref, std::string_view _what);
        const Line& ref() const noexcept;

        std::string_view name() const noexcept override;

    private:
        const Line& m_ref;
    };

    class ParseLookupException : public Exception {
    public:
        ParseLookupException(std::unique_ptr<ExceptionStack> _stack, const Scope& _ref, std::string_view _what, std::string_view _targetName);
        const Scope& ref() const noexcept;
        std::string_view target_name() const noexcept;

        std::string_view name() const noexcept override;

    private:
        const Scope& m_ref;
        std::string m_targetName;
    };

    class VariableLookupException : public ParseLookupException {
    public:
        using ParseLookupException::ParseLookupException;

        std::string_view name() const noexcept override;
    };

    class FunctionLookupException : public ParseLookupException {
    public:
        using ParseLookupException::ParseLookupException;

        std::string_view name() const noexcept override;
    };

    class ParsingException : public Exception {
    public:
        ParsingException(std::unique_ptr<ExceptionStack> _stack, const Line& _ref, size_t _tokenPos, std::string_view _what);
        const Line& ref() const noexcept;
        std::string_view name() const noexcept override;

        size_t token_pos() const noexcept;

    private:
        const Line& m_ref;
        size_t m_tokenPos;
    };

    class ScopeException : public Exception {
    public:
        ScopeException(std::unique_ptr<ExceptionStack> _stack, const ScopeSpace& _ref, std::string_view _what);
        const ScopeSpace& ref() const noexcept;

        std::string_view name() const noexcept override;
    private:
        const ScopeSpace& m_ref;
    };

    class ScopeLookupException : public ScopeException {
    public:
        ScopeLookupException(std::unique_ptr<ExceptionStack> _stack, const ScopeSpace& _ref, std::string_view _what, std::string_view _targetName);
        std::string_view target_name() const noexcept;

        std::string_view name() const noexcept override;
    private:
        std::string m_targetName;
    };

    class ScopeDuplicateException : public ScopeLookupException {
    public:
        using ScopeLookupException::ScopeLookupException;
    
        std::string_view name() const noexcept override;
    };

    class FunctionArgumentException : public Exception {
    public:
        using Exception::Exception;

        std::string_view name() const noexcept override;
    };

}

#endif