#ifndef REMI_CODE_SOURCE_HPP
#define REMI_CODE_SOURCE_HPP

#include <Remi/Line.hpp>
#include <Remi/Tokenizer.hpp>

#include <string_view>
#include <string>

namespace re {

    class CodeSource {
    public:
        const std::vector<Line>& lines() const noexcept;

        void read(std::string_view _filename);
        void tokenize(bool _printTokens = false);

    private:
        LexingState cleanup(LexingState _state, std::string& _str);

    private:
        std::vector<Line> m_lines;
    };

}

#endif