#ifndef REMI_EXECUTION_CONTEXT_HPP
#define REMI_EXECUTION_CONTEXT_HPP

#include <Remi/Context.hpp>
#include <Remi/ExecutionBlock.hpp>

#include <Remi/FunctionCallContext.hpp>
#include <Remi/FunctionInstance.hpp>

#include <deque>
#include <unordered_map>

namespace re {

    class Scope;
    class FunctionPlan;

    class ExecutionContext {
    public:
        ExecutionContext(const Context& _context);

        inline ExecutionBlock& generate_block(size_t _depth, size_t _id, size_t _byteSize);
        inline ExecutionBlock& generate_block(const Scope& _scope);
        inline ExecutionBlock& generate_block(const FunctionPlan& _targetPlan);

        // Uses silent function calls to init program-scope variables
        void execute_init_functions();

        inline void release_block(size_t _depth, size_t _id);

        // Pops the top block from the block stack at the _scope's depth and id
        inline void release_block(const Scope& _scope);

        inline const ExecutionBlock& resolve_stack_top(size_t _depth, size_t _id) const {
            return (*m_dynamicStacks.at(_depth).at(_id).back());
        }

        inline const ExecutionBlock& resolve(size_t _depth, size_t _id, size_t _callDepth) const {
            return (*m_dynamicStacks.at(_depth).at(_id).at(_callDepth));
        }

        inline FunctionInstance& current_function();
        inline FunctionInstance& current_caller_function();
        inline FunctionInstance& current_called_function();

        // Execute the function without adding it to the stack. This is used to
        // initialize program-scope variables. Data block is not deleted and appears
        // at call depth 0
        void silent_function_execute(const FunctionPlan& _calledFunction);

        // Prepares the function for execution, putting in on the stack
        // Call execute_current_function() to execute this newly initialized function
        inline FunctionInstance& init_function_call(const FunctionPlan& _calledFunction);

        // Execute the function adding it to the call stack and removing it from the
        // call stack when it finishes
        inline void execute_current_function();

        // Execute the function adding it to the call stack without removing it from the
        // call stack when it finishes. This function is used when there are aditional
        // return actions to execute after the function executes
        inline void execute_current_function_delayed_end();

        // TO BE USED AFTER A CALL TO execute_current_function_delayed_end()
        inline void remove_current_function();

        // Removes the top function from the stack and releases its block
        inline void end_function_call();

        // Calls init_function_call() for the called function and sets its caller
        // Does not automatically execute. Initialized function is on top of the stack
        inline void init_function_call_context(const FunctionPlan& _calledFunction);

        // Calls end_function_call() and removes the call context from the context stack
        inline void end_function_call_context();

    private:
        const Context& m_context;

        std::vector<FunctionInstance> m_functionStack;
        size_t m_functionCallCount;

        std::vector<FunctionCallContext> m_callContexts;
        std::unordered_map<size_t, std::unordered_map<size_t, size_t>> m_stackCount;

        std::unordered_map<size_t, std::unordered_map<
            size_t, std::deque<std::unique_ptr<ExecutionBlock>
        >>> m_dynamicStacks;
    };

}

#include <Remi/ExecutionContext.hxx>

#endif