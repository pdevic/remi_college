#ifndef REMI_FUNCTION_PLAN_HPP
#define REMI_FUNCTION_PLAN_HPP

#include <Remi/FunctionTypePlan.hpp>

#include <vector>
#include <memory>

#include <string>

namespace re {

    class Scope;
    class Action;

    class FunctionPlan {
    public:
        FunctionPlan(Scope* _scope, FunctionTypePlan&& _typePlan);
        //registerFunctionPlan(FunctionPlan&& _source);
        ~FunctionPlan();

        Scope& scope() noexcept;
        const Scope& scope() const noexcept;

        const std::vector<Action*>& actions() const noexcept;
        const FunctionTypePlan& type_plan() const noexcept;

        std::string_view name() const noexcept;

    protected:
        friend class Remi;
        friend class Parser;

        FunctionTypePlan& type_plan();
        std::vector<Action*>& actions();

        void add_action(Action* _action);
        void set_name(std::string_view _name);

    private:
        Scope* m_scope;
        std::vector<Action*> m_actions;

        FunctionTypePlan m_typePlan;
        std::string m_name;
    };

}

#endif