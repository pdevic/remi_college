#ifndef REMI_TUTORIAL_HPP
#define REMI_TUTORIAL_HPP

#include <string>
#include <vector>

namespace re {

    static const std::vector<std::string> RemiTutorialVariables = {
        "Remi uses the following syntax for variable declarations:\n\n",
        "\n\t<type> name\n\t<type> name = expr\n\t<type> name(expr)\n\n\n",
        "You can use the \',\' operator to declare multiple variables in the same statement\n",
        " -> All variables declared in the same statement have the same type\n",
        " -> You can use the \'=\' operator or parenthesis to initialize variables arbitrarily\n\n",
        "[Examples]:\n\n",
        "\n\tint a = 10\n\tint a, b(a + 3), c(5)\n\tconst float bar = -3.14 * 2\n\n\n",
        "Variable declarations are allowed in the program scope, function blocks\n",
        "(including loop and if blocks) and the initialization of for loops\n\n",
        "You can use the operators ++ and -- before expressions to perform\n",
        "incrementation and decrementation in the order that they appear in code\n",
        " -> Operators ++ and -- after expressions are NOT supported\n\n",
        "You can check the list of supported operators by passing the [-op] flag"
    };

    static const std::vector<std::string> RemiTutorialTypes = {
        "Built-in types and their byte sizes are the following:\n\n",
        ": bool     1\n",
        ": char     1\n",
        ": int      4\n",
        ": float    4\n",
        "\nWhen you assign a value or expression to a variable, type casting is performed\n",
        "automatically if it is necessary. You can trigger a type conversion by directly issuing\n",
        "a cast in an expression:\n\n",
        "\n\t<type>(expr)\n\n\n",
        "[Examples]:",
        "\n\n\n\t... float(var1 + var2)   ...\n",
        "\t... char(71) + char(72)  ...\n",
        "\t...    10 % int(pi)      ...\n"
    };

    static const std::vector<std::string> RemiTutorialFunctions = {
        ""
    };

}

#endif