// This file should be included at the bottom of ExecutionContext.hpp
//
// Inline function definitions are included here

#include <Remi/FunctionInstance.hpp>
#include <Remi/FunctionCallContext.hpp>

namespace re {

    ExecutionBlock& ExecutionContext::generate_block(size_t _depth, size_t _id, size_t _byteSize) {
        size_t& stackCount = m_stackCount[_depth][_id];

        if (m_dynamicStacks[_depth][_id].size() > stackCount)
            return *(m_dynamicStacks[_depth][_id][stackCount++]); // Free bloxx*/

        stackCount++;

        m_dynamicStacks[_depth][_id].push_back(std::make_unique<ExecutionBlock>(_depth, _id, m_dynamicStacks[_depth][_id].size(), _byteSize));
        return *(m_dynamicStacks[_depth][_id].back());
    }

    ExecutionBlock& ExecutionContext::generate_block(const Scope& _scope) {
        return generate_block(_scope.depth(), _scope.id(), m_context.get_scope_byte_size(_scope) + _scope.fake_offset());
    }

    ExecutionBlock& ExecutionContext::generate_block(const FunctionPlan& _targetPlan) {
        return generate_block(_targetPlan.scope());
    }

    FunctionInstance& ExecutionContext::current_function() {
        return m_functionStack[m_functionCallCount - 1];
    }

    FunctionInstance& ExecutionContext::current_caller_function() {
        return m_functionStack[m_callContexts.back().caller_function() - 1];
    }

    FunctionInstance& ExecutionContext::current_called_function() {
        return m_functionStack[m_callContexts.back().called_function() - 1];
    }

    FunctionInstance& ExecutionContext::init_function_call(const FunctionPlan& _calledFunction) {
        m_functionStack.push_back(FunctionInstance(_calledFunction, generate_block(_calledFunction.scope())));
        return m_functionStack.back();
    }

    void ExecutionContext::execute_current_function() {
        execute_current_function_delayed_end();
        remove_current_function();
    }

    void ExecutionContext::execute_current_function_delayed_end() {
        m_functionCallCount++;
        current_function().execute(*this);
    }

    void ExecutionContext::remove_current_function() {
        m_functionCallCount--;
    }

    void ExecutionContext::release_block(size_t _depth, size_t _id) {
        m_stackCount[_depth][_id]--;
    }

    void ExecutionContext::release_block(const Scope& _scope) {
        m_stackCount[_scope.depth()][_scope.id()]--;
    }

    void ExecutionContext::end_function_call() {
        release_block(m_functionStack.back().source_plan().scope());
        m_functionStack.pop_back();
    }

    void ExecutionContext::init_function_call_context(const FunctionPlan& _calledFunction) {
        init_function_call(_calledFunction);
        m_callContexts.push_back(FunctionCallContext(m_functionCallCount, m_functionCallCount + 1));
    }

    void ExecutionContext::end_function_call_context() {
        end_function_call();
        m_callContexts.pop_back();
    }

}