#ifndef REMI_THREAD_STREAM_HPP
#define REMI_THREAD_STREAM_HPP

#include <sstream>
#include <mutex>

namespace re {

    //
    // Thread-safe std::ostream
    // Inspired by https://stackoverflow.com/questions/4446484/a-line-based-thread-safe-stdcerr-for-c/53288135#53288135
    //
    // This class should ensure that streamed content will not get misplaced
    class ThreadStream : public std::ostringstream {
    public:
        ThreadStream(std::ostream& _os);
        ~ThreadStream();

    private:
        static std::mutex s_thread_mutex;
        std::ostream& m_os;
    };

}

#endif