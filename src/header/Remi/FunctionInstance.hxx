// This file should be included at the bottom of FunctionInstance.hpp
//
// Inline function definitions are included here

namespace re {

    const ExecutionBlock& FunctionInstance::block() const {
        return *m_block;
    }

    const FunctionPlan& FunctionInstance::source_plan() const {
        return *m_source;
    }

}