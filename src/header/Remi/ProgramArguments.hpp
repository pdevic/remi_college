#ifndef REMI_PROGRAM_ARGUMENTS_HPP
#define REMI_PROGRAM_ARGUMENTS_HPP

#include <Remi/Default.hpp>

#include <string>

#include <vector>
#include <map>

namespace re {

    class ProgramArguments {
    public:
        ProgramArguments();
        ProgramArguments(int _argc, char* _args[]);

        const std::map<std::string, std::string>& arguments() const;
        std::string_view find_argument(std::string_view _arg) const;

        // Returns the value of the argument named _arg
        // Throws a re::ProgramArgumentException if _arg wasn't previously set
        std::string_view operator[](const std::string& _arg) const;
        std::string_view operator[](std::string_view _arg) const;

        // Returns whether an argument named _arg was set
        bool is_argument_set(std::string_view _arg) const;

        // Throws re::ProgramArgumentException and re::ProgramArgumentUseException
        size_t parse_program_arguments(int _argc, char* _argv[]);

    protected:
        friend class Parser;

        void register_argument(std::string_view _argName, std::string_view _argValue);

    private:
        void init_default();

        decltype(RemiProgramArgumentFlags)::const_iterator deduce_argument_flag(std::string_view _str) const;
        decltype(RemiProgramArgumentNames)::const_iterator deduce_argument_name(std::string_view _str) const;

    private:
        std::map<std::string, std::string> m_arguments;
    };

}

#endif