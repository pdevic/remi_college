#ifndef REMI_SCRIPT_HPP
#define REMI_SCRIPT_HPP

#include <Remi/CodeSource.hpp>
#include <Remi/FunctionPlan.hpp>

#include <vector>
#include <string_view>
#include <string>

#include <memory>

namespace re {

    class Script {
    public:
        Script() = delete;

        // Initialize without making any further actions (doe not call read())
        Script(Script* _parent, std::string_view _filename);

        Script* parent() const noexcept;
        std::string_view filename() const noexcept;

        const std::unique_ptr<CodeSource>& source() const noexcept;
        const std::vector<Line>& lines() const;

        // Reads all lines of the given file and saves them in m_source
        // Throws re::ScriptException if it fails to open the file
        // Throws re::ScriptException if m_source fails to lex the file
        void read(); 

        // Tokenizes all lines in m_source
        // -> generates re::Token[s] that are stored in re::Line[s] in m_source
        void tokenize(bool _printTokens = false); 

    private:
        // Assigns the required resources for further usage
        // Throws re::ScriptException if the script was already initialized
        // Throws re::ScriptException if parent is set to self
        void init(Script* _parent, std::string_view _filename);

        // Used to throw a re::ScriptException if the script isn't currently in _status
        void expect_status(RemiScriptStatus _status) const;

        const FunctionPlan& init_function() const;

    protected:
        friend class Parser;

        FunctionPlan& init_function();
        bool has_init_function() const;

        void set_status(RemiScriptStatus _status) noexcept;
        void set_init_function(std::unique_ptr<FunctionPlan>&& _fPlan);

    private:
        Script* m_parent;
        RemiScriptStatus m_status;

        std::string m_filename;
        std::unique_ptr<CodeSource> m_source;

        std::unique_ptr<FunctionPlan> m_initFunction;
    };

}

#endif