#ifndef REMI_TIME_HPP
#define REMI_TIME_HPP

namespace re {

    typedef unsigned int uint;

    //
    // Provides datetime information as separate values - second, minute, hour, day, month and year
    // Sets all the values to current datetime values on construction
    class Time {
    public:
        enum TimeComponent {
            Second = 0,
            Minute,
            Hour,
            Day,
            Month,
            Year
        };

    public:
        Time();

        // Sets all values to match current date and time
        Time& update();

        uint operator[](TimeComponent _component) const;
        uint& operator[](TimeComponent _component);

        // Returns a re::Time instance containing values matching current date and time
        static Time now();

        uint second() const;
        uint minute() const;
        uint hour() const;

        uint day() const;
        uint month() const;
        uint year() const;

    private:
        uint m_time[6];
    };

}

#endif