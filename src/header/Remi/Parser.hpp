#ifndef REMI_PARSER_HPP
#define REMI_PARSER_HPP

#include <Remi/Error.hpp>

#include <Remi/Script.hpp>
#include <Remi/Context.hpp>

#include <Remi/FunctionPlan.hpp>
#include <Remi/ScriptTraverser.hpp>

#include <Remi/Action.hpp>

#include <string_view>
#include <string>

#include <stack>
#include <vector>
#include <sstream>

namespace re {

    class Remi;

    class Parser {
    public:
        Parser(Remi& _remi, Context& _target);

        Context& target_context();
        const Context& target_context() const;

        void parse_script(Script& _script, FunctionPlan* _previousInitFunction = nullptr);

    private:
        struct ParseSnapshot {
        public:
            ParseSnapshot(const ScriptTraverser& _traverser);

            void throw_standard_parse_error(std::string_view _fname, std::string_view _what) const;

            const Line& line;
            size_t tokenPos;
        };

        void parse_load_statement(ScriptTraverser& _traverser, Script& _scriptParent);

        VariablePlan parse_type(ScriptTraverser& _traverser, Scope& _scope) const;
        void parse_function_declaration(ScriptTraverser& _traverser, Scope& _targetScope);

        VariablePlan& generate_anonymous_variable(const VariablePlan& _vPlan, Scope& _scope) const;

        void parse_statement(ScriptTraverser& _traverser, FunctionPlan& _function) const;
        void parse_function_body(ScriptTraverser& _traverser, FunctionPlan& _function) const;
        ActionFunctionCall* parse_function_call(ScriptTraverser& _traverser, std::string_view _fName, Scope& _callerScope, VariablePlan* _directReturn = nullptr) const;

        void parse_variable_declaration(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const;

        void parse_if_statement(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const;
        void parse_loop_statement(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const;

        ActionExpression* parse_expression(ScriptTraverser& _traverser, Scope& _scope, VariablePlan* _directReturn = nullptr) const;
        VariablePlan& parse_subexpression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const;

        VariablePlan& increment_variable(ScriptTraverser& _traverser, VariablePlan& _var, std::vector<Action*>& _actionVector, Scope& _scope, bool _positivity = true) const;
        VariablePlan& apply_negative_sign(ScriptTraverser& _traverser, const VariablePlan& _var, std::vector<Action*>& _actionVector, Scope& _scope) const;

        VariablePlan& parse_numeric_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, bool _positive = true) const;
        VariablePlan& parse_char_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const;
        VariablePlan& parse_string_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const;

        VariablePlan& parse_engine_directive(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope) const;

        template<class T>
        VariablePlan& numeric_const_expression_generic(std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const;

        VariablePlan& generate_value_type_cast(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _from, VariablePlan& _to, bool _allowDirectWrite = false) const;
        VariablePlan& generate_binary_operation(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _left, RemiOperator _operator, VariablePlan& _directReturn) const;

        template<class T>
        Action* generate_variable_operation_action(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, ParseSnapshot _snapshot) const;
        void generate_variable_operation(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, std::vector<Action*>& _actionVector, ParseSnapshot _snapshot) const;

        void parse_print_statement(ScriptTraverser& _traverser, FunctionPlan& _function) const;
        void generate_print_for_expression(ScriptTraverser& _traverser, ActionExpression& _root, FunctionPlan& _targetFunction) const;

        VariableReference generate_variable_reference(const VariablePlan& _source, const Scope& _callerScope) const;

        void report_parsing_exception(std::string_view _functionName, const Script& _script, ParsingException& _e) const;
        static std::string generate_annotated_error_str(std::string_view _str, const Token& _token, size_t _startOffset = 0);

    private:
        Context& m_targetContext;
        Remi& m_remi;

        std::stack<ScriptTraverser> m_traverserStack;
        std::vector<FunctionPlan*> m_functionStack;
    };

}

#endif