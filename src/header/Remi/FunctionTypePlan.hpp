#ifndef REMI_FUNCTION_TYPE_PLAN_HPP
#define REMI_FUNCTION_TYPE_PLAN_HPP

#include <Remi/FunctionArgument.hpp>

#include <vector>

namespace re {

    class FunctionTypePlan {
    public:
        FunctionTypePlan();

        const std::vector<FunctionArgument>& arguments() const noexcept;
        const VariablePlan& return_type() const noexcept;

        void add_argument(FunctionArgument _arg);
        void set_return_type(VariablePlan _arg);

        const FunctionArgument& operator[](size_t _pos) const;

    protected:
        friend class Parser;

        VariablePlan& return_type() noexcept;

    private:
        std::vector<FunctionArgument> m_arguments;
        VariablePlan m_returnType;
    };

    FunctionTypePlan& operator<<(FunctionTypePlan& _ftplan, FunctionArgument _arg);
}

#endif