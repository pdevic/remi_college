#ifndef REMI_FUNCTION_CALL_CONTEXT_HPP
#define REMI_FUNCTION_CALL_CONTEXT_HPP

#include <Remi/FunctionInstance.hpp>

namespace re {

    class FunctionCallContext {
    public:
        FunctionCallContext(size_t _callerFunction, size_t _calledFunction);

        size_t caller_function() const noexcept;
        size_t called_function() const noexcept;
    
    private:
        size_t m_callerFunction;
        size_t m_calledFunction;
    };

}

#endif