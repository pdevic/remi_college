#ifndef REMI_SCRIPT_TRAVERSER_HPP
#define REMI_SCRIPT_TRAVERSER_HPP

#include <Remi/Default.hpp>
#include <Remi/Script.hpp>

#include <vector>

namespace re {

    class ScriptTraverser {
    public:
        ScriptTraverser(const Script& _script);

        const Token& current_token() const;
        const Line& current_line() const;

        size_t current_token_pos() const;
        AdvanceState state() const;

        AdvanceState next();

    protected:
        friend class Parser;

        void expect_keyword(RemiKeyword _keyword) const;
        void expect_operator(RemiOperator _operator) const;

        RemiKeyword expect_one_of_keywords(std::vector<RemiKeyword> _keywords) const;

        void expect_symbol(char _symbol) const;

        // Iterate over the string argument and expect each character
        // Uses next() for every character stopping AFTER the last one
        void expect_symbols(std::string_view _symbols);

        std::string_view expect_identifier() const;

        void expect_string() const;
        void expect_string(std::string_view _str) const;
        int expect_number() const;

        void throw_standard_parse_error(std::string_view _fname, std::string_view _what) const;

    private:
        const Script& m_targetScript;

        const Token* m_currentToken;
        const Line* m_currentLine;

        std::vector<Token>::const_iterator m_tokensIt;
        std::vector<Line>::const_iterator m_linesIt;

        size_t m_tokenPos;

        AdvanceState m_state;
    };

}

#endif