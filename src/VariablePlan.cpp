#include <Remi/VariablePlan.hpp>

#include <Remi/Scope.hpp>

namespace re {

    VariablePlan::VariablePlan(): m_typeID(REMI_UNINITIALIZED_TYPE) {}

    VariablePlan::VariablePlan(const Scope& _scope, Constness _referenceConstness, ReferenceType _refType, Constness _valueConstness, ValueType _valueType, size_t _typeID, size_t _byteOffset)
    : m_scope(&_scope), m_refConstness(_referenceConstness), m_refType(_refType), m_valueConstness(_valueConstness), m_valueType(_valueType), m_typeID(_typeID) {
        set_byte_offset(_byteOffset);
    }

    VariablePlan::VariablePlan(const VariablePlan& _source)
    : m_scope(&_source.scope()),
        m_refConstness(_source.reference_constness()), m_refType(_source.reference_type()), m_valueConstness(_source.value_constness()),
        m_valueType(_source.value_type()), m_typeID(_source.type_id()), m_byteOffset(_source.byte_offset()) {}

    VariablePlan VariablePlan::uninitialized_variable(const Scope& _scope) {
        return VariablePlan(_scope, Constness::Const, ReferenceType::Value, Constness::Const, ValueType::Value, REMI_UNINITIALIZED_TYPE);
    }

    VariablePlan VariablePlan::any_type(const Scope& _scope) {
        return VariablePlan(_scope, Constness::Mutable, ReferenceType::Value, Constness::Mutable, ValueType::Value, REMI_ANY_TYPE);
    }

    VariablePlan VariablePlan::void_variable(const Scope& _scope) {
        return VariablePlan(_scope, Constness::Const, ReferenceType::Value, Constness::Const, ValueType::Value, REMI_VOID_TYPE);
    }

    VariablePlan VariablePlan::normal_variable(const Scope& _scope, Constness _constness, ValueType _vType, size_t _typeID, size_t _byteOffset) {
        return VariablePlan(_scope, Constness::Mutable, ReferenceType::Value, _constness, _vType, _typeID, _byteOffset);
    }

    VariablePlan VariablePlan::const_value_type(const Scope& _scope, size_t _typeID, size_t _byteOffset) {
        return VariablePlan(_scope, Constness::Const, ReferenceType::Value, Constness::Const, ValueType::Value, _typeID, _byteOffset);
    }

    VariablePlan VariablePlan::variable_reference(const Scope& _scope, Constness _constness, const VariablePlan& _var) {
        return VariablePlan(_scope, _constness, ReferenceType::Reference, _var.value_constness(), _var.value_type(), _var.type_id(), _var.byte_offset());
    }

    VariablePlan VariablePlan::const_variable_reference(const Scope& _scope, Constness _constness, const VariablePlan& _var) {
        return variable_reference(_scope, _constness, _var);
    }

    const Scope& VariablePlan::scope() const {
        return (*m_scope);
    }

    Constness VariablePlan::reference_constness() const {
        return m_refConstness;
    }

    ReferenceType VariablePlan::reference_type() const {
        return m_refType;
    }

    Constness VariablePlan::value_constness() const {
        return m_valueConstness;
    }

    ValueType VariablePlan::value_type() const {
        return m_valueType;
    }

    size_t VariablePlan::type_id() const {
        return m_typeID;
    }

    size_t VariablePlan::byte_offset() const {
        return m_byteOffset;
    }

    void VariablePlan::set_type_id(size_t _id) {
        m_typeID = _id;
    }

    void VariablePlan::set_byte_offset(size_t _offset) {
        m_byteOffset = _offset;
    }

    void VariablePlan::set_scope_reference(const Scope* _scope) {
        m_scope = _scope;
    }

    bool VariablePlan::can_equate(const VariablePlan& _other) const {
        return ((*this) == _other);
    }

    bool operator==(const VariablePlan& _l, const VariablePlan& _r) {
        bool anyType = (_l.type_id() == REMI_ANY_TYPE) || (_r.type_id() == REMI_ANY_TYPE);
        bool uninitType = (_l.type_id() == REMI_UNINITIALIZED_TYPE) || (_r.type_id() == REMI_UNINITIALIZED_TYPE);
        bool typeCompat = (_l.value_type() == _r.value_type()) && (_l.type_id() == _r.type_id());
        bool scopeEq = (_l.scope() == _r.scope());

        return (   scopeEq && (  (anyType && !uninitType)   ||   typeCompat  )   );
    }

    bool operator!=(const VariablePlan& _l, const VariablePlan& _r) {
        return !(_l == _r);
    }

}