#include <Remi/Clock.hpp>

namespace re {

    Clock::Clock() {
        start();
    }
   
    double Clock::restart() {
        double res = elapsed().count();
   
        start();
        return res;
    }
   
    std::chrono::duration<double> Clock::elapsed() const {
        std::chrono::duration<double> res;
        auto elapsed = std::chrono::system_clock::now();
   
        res = elapsed - m_start;
   
        return res;
    }
   
    double Clock::seconds() const {
        return elapsed().count();
    }
   
    inline void Clock::start() {
        m_start = std::chrono::system_clock::now();
    }
    
}
