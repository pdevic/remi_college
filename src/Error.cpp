#include <Remi/Error.hpp>

#include <Remi/Logger.hpp>

#include <sstream>

namespace re {

    ExceptionStack::ExceptionStack(std::string_view _name) {
        std::ostringstream ss;

        ss << "[Source]" << " : " << _name;
        m_names.push_back(std::string(ss.str()));
    }

    void ExceptionStack::add_name(const Exception& _e, std::string_view _name) {
        std::ostringstream ss;

        ss << _e.name() << " : " << _name;
        m_names.push_back(std::string(ss.str()));
    }

    const std::vector<std::string>& ExceptionStack::names() const noexcept {
        return m_names;
    }

    size_t ExceptionStack::depth() const {
        return m_names.size();
    }



    Exception::Exception(std::unique_ptr<ExceptionStack> _stack, std::string_view _what)
    : m_what(_what), m_esPtr(std::move(_stack)) {}

    Exception::Exception(Exception& _e) : m_what(_e.m_what), m_esPtr(std::move(_e.m_esPtr)) {}

    std::string_view Exception::what() const noexcept {
        return std::string_view(m_what);
    }

    std::string_view Exception::name() const noexcept {
        return "re::Exception";
    }

    ExceptionStack& Exception::exception_stack() {
        return (*m_esPtr);
    }

    const ExceptionStack& Exception::exception_stack() const {
        return (*m_esPtr);
    }

    std::unique_ptr<ExceptionStack>& Exception::exception_stack_ptr() {
        return m_esPtr;
    }

    const std::unique_ptr<ExceptionStack>& Exception::exception_stack_ptr() const {
        return m_esPtr;
    }

    std::string_view ProgramException::name() const noexcept {
        return "re::ProgramException";
    }



    ProgramArgumentException::ProgramArgumentException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _ref, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref) {}

    const ProgramArguments& ProgramArgumentException::ref() const {
        return m_ref;
    }

    std::string_view ProgramArgumentException::name() const noexcept {
        return "re::ProgramArgumentException";
    }



    ProgramArgumentInputException::ProgramArgumentInputException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _ref, std::string_view _what, std::string_view _input)
    : ProgramArgumentException(std::move(_stack), _ref, _what), m_input(_input) {}

    std::string_view ProgramArgumentInputException::input() const {
        return m_input;
    }

    std::string_view ProgramArgumentInputException::name() const noexcept {
        return "re::ProgramArgumentInputException";
    }



    ProgramArgumentLookupException::ProgramArgumentLookupException(std::unique_ptr<ExceptionStack> _stack, std::string_view _requested, std::string_view _what)
    : Exception(std::move(_stack), _what), m_requested(_requested) {}

    std::string_view ProgramArgumentLookupException::requested() const {
        return m_requested;
    }

    std::string_view ProgramArgumentLookupException::name() const noexcept {
        return "re::ProgramArgumentLookupException";
    }



    ProgramArgumentUseException::ProgramArgumentUseException(std::unique_ptr<ExceptionStack> _stack, const ProgramArguments& _caRef, const ProgramArgumentMeta& _ref, std::string_view _what, std::string_view _input)
    : ProgramArgumentInputException(std::move(_stack), _caRef, _what, _input), m_ref(_ref) {}

    const ProgramArgumentMeta& ProgramArgumentUseException::ref() const {
        return m_ref;
    }

    std::string_view ProgramArgumentUseException::name() const noexcept {
        return "re::ProgramArgumentUseException";
    }



    ScriptException::ScriptException(std::unique_ptr<ExceptionStack> _stack, const Script& _ref, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref) {}

    const Script& ScriptException::ref() const noexcept {
        return m_ref;
    }

    std::string_view ScriptException::name() const noexcept {
        return "re::ScriptException";
    }



    ScriptLookupException::ScriptLookupException(std::unique_ptr<ExceptionStack> _stack, const Context& _ref, std::string_view _scriptName, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref), m_scriptName(_scriptName) {}
    
    const Context& ScriptLookupException::ref() const noexcept {
        return m_ref;
    }

    std::string_view ScriptLookupException::script_name() const noexcept {
        return m_scriptName;
    }

    std::string_view ScriptLookupException::name() const noexcept {
        return "re::ScriptLookupException";
    }



    std::string_view CodeSourceException::name() const noexcept {
        return "re::CodeSourceException";
    }



    LexingException::LexingException(std::unique_ptr<ExceptionStack> _stack, const Line& _ref, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref) {}
    
    const Line& LexingException::ref() const noexcept {
        return m_ref;
    }

    std::string_view LexingException::name() const noexcept {
        return "re::LexingException";
    }



    ParseLookupException::ParseLookupException(std::unique_ptr<ExceptionStack> _stack, const Scope& _ref, std::string_view _what, std::string_view _targetName)
    : Exception(std::move(_stack), _what), m_ref(_ref), m_targetName(_targetName) {}

    const Scope& ParseLookupException::ref() const noexcept {
        return m_ref;
    }

    std::string_view ParseLookupException::target_name() const noexcept {
        return m_targetName;
    }

    std::string_view ParseLookupException::name() const noexcept {
        return "re::ParseLookupException";
    }



    std::string_view VariableLookupException::name() const noexcept {
        return "re::VariableLookupException";
    }



    std::string_view FunctionLookupException::name() const noexcept {
        return "re::FunctionLookupException";
    }



    ParsingException::ParsingException(std::unique_ptr<ExceptionStack> _stack, const Line& _ref, size_t _tokenPos, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref), m_tokenPos(_tokenPos) {}

    const Line& ParsingException::ref() const noexcept  {
        return m_ref;
    }

    std::string_view ParsingException::name() const noexcept {
        return "re::ParsingException";
    }

    size_t ParsingException::token_pos() const noexcept {
        return m_tokenPos;
    }



    ScopeException::ScopeException(std::unique_ptr<ExceptionStack> _stack, const ScopeSpace& _ref, std::string_view _what)
    : Exception(std::move(_stack), _what), m_ref(_ref) {}

    const ScopeSpace& ScopeException::ref() const noexcept  {
        return m_ref;
    }

    std::string_view ScopeException::name() const noexcept {
        return "re::ScopeException";
    }



    ScopeLookupException::ScopeLookupException(std::unique_ptr<ExceptionStack> _stack, const ScopeSpace& _ref, std::string_view _what, std::string_view _targetName)
    : ScopeException(std::move(_stack), _ref, _what), m_targetName(_targetName) {}

    std::string_view ScopeLookupException::target_name() const noexcept {
        return m_targetName;
    }

    std::string_view ScopeLookupException::name() const noexcept {
        return "re::ScopeLookupException";
    }

    std::string_view ScopeDuplicateException::name() const noexcept {
        return "re::ScopeDuplicateException";
    }

    std::string_view FunctionArgumentException::name() const noexcept {
        return "re::FunctionArgumentException";
    }


}