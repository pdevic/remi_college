#include <Remi/Scope.hpp>

#include <Remi/Error.hpp>

#include <sstream>
#include <iostream>

namespace re {

    Scope::Scope(Scope* _parent, ScopeType _type)
    : m_parent(_parent), m_type(_type), m_fakeOffset(0) {
        if (_parent == nullptr) {
            set_depth(0);
        }
        else {
            set_depth(_parent->depth() + 1);
        }
    }

    Scope::~Scope() {
        for(auto p : m_variables) {
            delete p.second;
            p.second = nullptr;
        }

        for(auto p : m_anonymousVariables) {
            delete p;
            p = nullptr;
        }

        for(auto p : m_functions) {
            delete p.second;
            p.second = nullptr;
        }

        for(auto p : m_anonymousFunctions) {
            delete p;
            p = nullptr;
        }
    }

    ScopeType Scope::type() const {
        return m_type;
    }

    Scope* Scope::parent() const {
        return m_parent;
    }

    size_t Scope::depth() const {
        return m_depth;
    }

    size_t Scope::id() const {
        return m_id;
    }

    size_t Scope::fake_offset() const {
        return m_fakeOffset;
    }

    std::string Scope::coords_str() const {
        std::ostringstream ss;

        ss << m_depth << ':' << m_id;
        return ss.str();
    }

    const std::map<std::string, VariablePlan*>& Scope::variables() const {
        return m_variables;
    }

    const std::vector<VariablePlan*>& Scope::anonymous_variables() const {
        return m_anonymousVariables;
    }

    const std::map<std::string, FunctionPlan*>& Scope::functions() const {
        return m_functions;
    }

    const std::vector<FunctionPlan*>& Scope::anonymous_functions() const {
        return m_anonymousFunctions;
    }

    size_t Scope::variable_count() const {
        return m_variables.size();
    }

    bool Scope::variable_exists(std::string_view _name, SearchType _searchType) const {
        if (m_variables.find(std::string(_name)) != m_variables.end()) {
            return true;
        }
        else if (m_parent != nullptr) {
            if (_searchType == SearchType::Deep) return m_parent->variable_exists(_name, _searchType);
            else if ((_searchType == SearchType::LocalInline) && (m_parent->type() != ScopeType::ProgramBlock)) {
                return m_parent->variable_exists(_name, _searchType);
            }
            else if ((_searchType == SearchType::InlineStrict) && (m_parent->type() == ScopeType::InlineBlock)) {
                return m_parent->variable_exists(_name, _searchType);
            }
        }

        return false;
    }

    bool Scope::function_exists(std::string_view _name, SearchType _searchType) const {
        if (m_functions.find(std::string(_name)) != m_functions.end()) {
            return true;
        }
        else if (m_parent != nullptr) {
            if (_searchType == SearchType::Deep) return m_parent->function_exists(_name, _searchType);
            else if ((_searchType == SearchType::LocalInline) && (m_parent->type() != ScopeType::ProgramBlock)) {
                return m_parent->function_exists(_name, _searchType);
            }
            else if ((_searchType == SearchType::InlineStrict) && (m_parent->type() == ScopeType::InlineBlock)) {
                return m_parent->function_exists(_name, _searchType);
            }
        }

        return false;
    }

    VariablePlan& Scope::find_variable(std::string_view _name, SearchType _searchType) const {
        if (variable_exists(_name, SearchType::LocalStrict)) return *(m_variables.at(std::string(_name)));
        else if (m_parent != nullptr) {
            if (_searchType == SearchType::Deep) return m_parent->find_variable(_name, _searchType);
            else if ((_searchType == SearchType::LocalInline) && (m_parent->type() != ScopeType::ProgramBlock)) {
                return m_parent->find_variable(_name, _searchType);
            }
            else if ((_searchType == SearchType::InlineStrict) && (m_parent->type() == ScopeType::InlineBlock)) {
                return m_parent->find_variable(_name, _searchType);
            }
        }

        throw(VariableLookupException(std::make_unique<ExceptionStack>("re::Scope::find_variable()"), (*this), "Failed to find variable", _name));
    }

    FunctionPlan& Scope::find_function(std::string_view _name, SearchType _searchType) const {
        if (function_exists(_name, SearchType::LocalStrict)) return *(m_functions.at(std::string(_name)));
        else if (m_parent != nullptr) {
            if (_searchType == SearchType::Deep) return m_parent->find_function(_name, _searchType);
            else if ((_searchType == SearchType::LocalInline) && (m_parent->type() != ScopeType::ProgramBlock)) {
                return m_parent->find_function(_name, _searchType);
            }
            else if ((_searchType == SearchType::InlineStrict) && (m_parent->type() == ScopeType::InlineBlock)) {
                return m_parent->find_function(_name, _searchType);
            }
        }

        throw(VariableLookupException(std::make_unique<ExceptionStack>("re::Scope::find_variable()"), (*this), "Failed to find variable", _name));
    }

    VariablePlan& Scope::register_variable(std::string_view _name, VariablePlan&& _variablePlan) {
        if (function_exists(_name, SearchType::Deep)) {
            throw(VariableLookupException(std::make_unique<ExceptionStack>("re::Scope::register_variable"), (*this), "A function with requested name already exists", _name));
        }
        else if (variable_exists(_name, SearchType::Deep)) {
            throw(VariableLookupException(std::make_unique<ExceptionStack>("re::Scope::register_variable"), (*this), "A variable with requested name already exists", _name));
        }
        else {
            VariablePlan& ret = *(m_variables[std::string(_name)] = new VariablePlan(std::move(_variablePlan)));
            ret.set_scope_reference(this);
            ret.set_byte_offset(ret.byte_offset() + m_fakeOffset);

            return ret;
        }
    }

    VariablePlan& Scope::register_anonymous_variable(VariablePlan&& _variablePlan) {
        VariablePlan* ret = new VariablePlan(std::move(_variablePlan));
        ret->set_scope_reference(this);
        ret->set_byte_offset(ret->byte_offset() + m_fakeOffset);

        m_anonymousVariables.push_back(ret);
        return *(ret);
    }

    FunctionPlan& Scope::register_function(std::string_view _name, FunctionPlan&& _functionPlan)  {
        if (function_exists(_name, SearchType::Deep)) {
            throw(VariableLookupException(std::make_unique<ExceptionStack>("re::Scope::register_variable"), (*this), "A function with requested name already exists", _name));
        }
        else if (variable_exists(_name, SearchType::Deep)) {
            throw(FunctionLookupException(std::make_unique<ExceptionStack>("re::Scope::register_function"), (*this), "A variable with given name already exists", _name));
        }
        else {
            return *(m_functions[std::string(_name)] = new FunctionPlan(std::move(_functionPlan)));
        }
    }

    FunctionPlan& Scope::register_anonymous_function(FunctionPlan&& _functionPlan) {
        FunctionPlan* ret = new FunctionPlan(std::move(_functionPlan));

        m_anonymousFunctions.push_back(ret);
        return *(ret);
    }

    void Scope::set_depth(size_t _depth) {
        m_depth = _depth;
    }

    void Scope::set_id(size_t _id) {
        m_id = _id;
    }
    
    void Scope::set_fake_offset(size_t _fakeOffset) {
        m_fakeOffset = _fakeOffset;
    }

    void Scope::increase_fake_offset(size_t _fakeOffset) {
        m_fakeOffset += _fakeOffset;
    }

    bool operator==(const Scope& _left, const Scope& _right) {
        return (_left.depth() == _right.depth()) && (_left.id() == _right.id());
    }
}