#include <Remi/CodeSource.hpp>

#include <Remi/Default.hpp>

#include <Remi/Error.hpp>

#if REMI_PRINT_CODE_SOURCE_INFO
    #include <Remi/Logger.hpp>
#endif

#include <fstream>
#include <sstream>

#include <filesystem>

namespace re {

    const std::vector<Line>& CodeSource::lines() const noexcept {
        return m_lines;
    }

    void CodeSource::read(std::string_view _filename) {
        std::fstream file(std::string(_filename), std::ios::in);

        if (!std::filesystem::exists(_filename))
            throw(CodeSourceException(std::make_unique<ExceptionStack>("re::CodeSource::read()"), "Tried to read a file that doesn't exist or couldn't be located (filename = " + std::string(_filename) + ")"));

        if (!file) {
            file.close();
            throw(ProgramException(std::make_unique<ExceptionStack>("re::CodeSource::read()"), "Failed to open a file! This should never happen! Check if the file exists before creating a script for it!"));
        }
        else {
            LexingState state{LexingState::Normal};
            
            size_t lCount{0};    // Counts accepted lines (non-empty lines, relative numbers)
            size_t lCountAll{0}; // Counts all lines (all lines, global numbers)

        #if REMI_PRINT_CODE_SOURCE_INFO
            Logger l(LogMode::Console);
            LogStream ls(l);

        #if REMI_PRINT_ERROR_CLASS_INFO
            ls << "[->] [re::CodeSource @" << this << ", re::CodeSource::read()]:\n";
        #endif
            ls << " -> Filename: \"" << _filename << "\"\n";
            ls << " -> Source file opened succesfully\n";
        #endif

            while(!file.eof()) {
                std::string s;
                std::getline(file, s);

                lCountAll++;
                state = cleanup(state, s);

                if (!s.empty()) {
                    lCount++;

                    m_lines.push_back(Line(lCountAll, lCount, std::move(s)));
                        // std::move the string to avoid copying. s is not to be used after this
                }
            }

            file.close();

            if (m_lines.empty()) {
                throw(CodeSourceException(std::make_unique<ExceptionStack>("re::CodeSource::read()"), "File was empty after cleanup"));
            }

            switch(state) {
                // The following two cases are encountered if a string or a comment in the script wasn't terminated properly
            case LexingState::Comment:
                throw(LexingException(std::make_unique<ExceptionStack>("re::CodeSource::read()"), m_lines.back(), "Multiline comment reaches the end of the script without being terminated"));
                break;
            case LexingState::String:
                throw(LexingException(std::make_unique<ExceptionStack>("re::CodeSource::read()"), m_lines.back(), "A string reaches the end of the script without being terminated"));
                break;
                
            default:
                break;
            }

        #if REMI_PRINT_CODE_SOURCE_INFO
            ls << " -> " << lCountAll << " lines: " << lCount << " lines accepted, ";
            ls << (lCountAll - lCount) << " empty lines ignored\n";
        #endif
        }
    }

    void CodeSource::tokenize(bool _printTokens) {
        Tokenizer t(m_lines);
        t.tokenize(_printTokens);
    }

    LexingState CodeSource::cleanup(LexingState _state, std::string& _str) {
        std::stringstream ss;
        LexingState state{_state};

        // s and sp are used to make multi-character comments easier to detect
        std::string s{"  "};  // Uses current character and next character
        std::string sp{"  "}; // Uses previous character and next character

        char last{' '};

        // Make sure there is enough empty space after the string so that s and sp don't cause a segfault
        for(size_t i{0}; i < max(RemiCommentMultiBegin.size(), RemiCommentMultiEnd.size()); i++) {
            _str.append(" ");
        }

        for(size_t i{0}; i < _str.size(); i++) {
            char c = _str[i]; // Current character

            s[0] = c;
            s[1] = _str[i + 1];

            sp[0] = last;
            sp[1] = c;

            switch(state) {
                case LexingState::Normal:
                    if (s == RemiCommentSingle) {
                        _str = ss.str();

                        while(_str.back() == ' ') { // Remove any spaces left at the end of the string
                            _str.pop_back();
                        }

                        return LexingState::Normal; // Since we've hit a line comment we are done with this line
                    }
                    else if (s == RemiCommentMultiBegin) {
                        state = LexingState::Comment;
                        break;
                    }
                    else if ((c == '"') && (sp != "\\\"")) { // Make sure not to enter string mode if you
                        state = LexingState::String;         // encounter a <\">
                    }

                    if (s != "  ") {
                        if (ss.str().empty() && (c == ' ')) {
                            break;
                        }
                        
                        ss << c; 
                    }
                    break;

                case LexingState::String:
                    if ((c == '"') && (sp != "\\\"")) { // Make sure not to exit string mode if you
                        state = LexingState::Normal;    // encounter a <\">
                    }

                    ss << c; // Accept all characters while in string mode
                    break;

                case LexingState::Comment:
                    if (sp == RemiCommentMultiEnd) {
                        state = LexingState::Normal;
                    }
                    break;
            }

            last = c;
        }

        _str = ss.str();

        while(_str.back() == ' ') { // Remove any spaces left at the end of the string
            _str.pop_back();
        }

        return state;
    }
}