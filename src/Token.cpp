#include <Remi/Token.hpp>

#include <Remi/Default.hpp>

#include <locale>

namespace re {

    Token::Token(size_t _start, size_t _length, std::string_view _source)
        : m_type(TokenType::None), m_start(_start) {
            m_str = _source.substr(_start, _length);
            decide_type();
        }

    std::string_view Token::str() const noexcept {
        return m_str;
    }

    size_t Token::start() const noexcept {
        return m_start;
    }

    TokenType Token::type() const noexcept {
        return m_type;
    }

    bool Token::is_keyword() const {
        return (RemiKeywords.find(std::string(m_str)) != RemiKeywords.end());
    }

    bool Token::is_identifier() const {
        return remi_is_identifier(m_str);
    }

    bool Token::is_symbol() const {
        return ((m_str.size() == 1) && RemiSymbols.find(m_str[0]));
    }

    bool Token::is_numeric() const {
        return ((m_type == TokenType::Number) || (m_type == TokenType::Digit));
    }

    bool Token::is_operator() const {
        return RemiOperators.find(std::string(m_str)) != RemiOperators.end();
    }

    TokenType Token::decide_type() {
        if (RemiOperators.find(std::string(m_str)) != RemiOperators.end()) {
            m_type = TokenType::Operator;
            return m_type;
        }

        if (m_str.size() == 1) {
            if (RemiSymbols.find(m_str) != std::string::npos) {
                m_type = TokenType::Symbol;
            } else if (isdigit(m_str[0])) {
                m_type = TokenType::Digit;
            } else if (isalpha(m_str[0])) {
                m_type = TokenType::Letter;
            }
        } else {
            bool isNum = true;

            if (RemiKeywords.find(std::string(m_str)) != RemiKeywords.end()) {
                m_type = TokenType::Keyword;
                return m_type;
            }

            for(auto c : m_str) {
                if (!isdigit(c)) {
                    isNum = false;
                    break;
                }
            }

            m_type = (isNum) ? TokenType::Number : TokenType::String;
        }

        return m_type;
    }

    bool operator==(const Token& _token, RemiKeyword _keyword) {
        if (_token.type() == TokenType::Keyword) {
            return (_token.str() == remi_keyword_str(_keyword));
        }
        
        return false;
    }

    bool operator!=(const Token& _token, RemiKeyword _keyword) {
        return !(_token == _keyword);
    }

    bool operator==(const Token& _token, RemiOperator _operator) {
        return (_token.str() == remi_operator_str(_operator));
    }

    bool operator!=(const Token& _token, RemiOperator _operator) {
        return !(_token == _operator);
    }

    bool operator==(const Token& _token, std::string_view _str) {
        return (_token.str() == _str);
    }

    bool operator!=(const Token& _token, std::string_view _str) {
        return !(_token == _str);
    }
    
    bool operator==(const Token& _token, char _symbol) {
        if (_token.str().size() != 1) {
            return false;
        }
        else {
            return (_token.str().front() == _symbol);
        }
    }

    bool operator!=(const Token& _token, char _symbol) {
        return !(_token == _symbol);
    }

}