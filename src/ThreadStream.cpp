#include <Remi/ThreadStream.hpp>

namespace re {

    ThreadStream::ThreadStream(std::ostream& _os)
       : m_os(_os) {
           imbue(_os.getloc());
           precision(_os.precision());
           width(_os.width());
           setf(std::ios::fixed, std::ios::floatfield);
    }

    ThreadStream::~ThreadStream() {
        std::lock_guard<std::mutex> guard(s_thread_mutex);
        m_os << this->str();
        m_os.flush();
    }

    std::mutex ThreadStream::s_thread_mutex{};

}