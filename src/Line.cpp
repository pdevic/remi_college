#include <Remi/Line.hpp>

namespace re {

    Line::Line(size_t _number, size_t _relativeNumber, const std::string& _source)
        : m_source(_source) {
        m_number = _number;
        m_relativeNumber = _relativeNumber;
    }

    const std::string_view Line::source() const noexcept {
        return m_source;
    }

    const std::vector<Token>& Line::tokens() const noexcept {
        return m_tokens;
    }

    size_t Line::number() const noexcept {
        return m_number;
    }
    
    size_t Line::relative_number() const noexcept {
        return m_relativeNumber;
    }

    void Line::add_token(const Token& _token) {
        m_tokens.push_back(_token);
    }

    const Token& Line::operator[](size_t _pos) const {
        return m_tokens[_pos];
    }

    Line& Line::operator<<(const Token& _token) {
        add_token(_token);
        return (*this);
    }
}