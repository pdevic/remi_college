#include <Remi/FunctionArgument.hpp>

namespace re {

    FunctionArgument::FunctionArgument(std::string_view _name, VariablePlan _vplan)
    : m_name(_name), m_vPlan(_vplan) {}

    std::string_view FunctionArgument::name() const {
        return m_name;
    }

    const VariablePlan& FunctionArgument::variable_plan() const {
        return m_vPlan;
    }

}