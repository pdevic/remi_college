#include<Remi/Parser.hpp>

namespace re {

#define REMI_EXPAND_INCREMENT_OPERATION_CASE(___type) \
    if (_positivity) _actionVector.push_back(new ActionIncrementValue<___type, true >(generate_variable_reference(_var, _scope))); \
    else             _actionVector.push_back(new ActionIncrementValue<___type, false>(generate_variable_reference(_var, _scope)));

    VariablePlan& Parser::increment_variable(ScriptTraverser& _traverser, VariablePlan& _var, std::vector<Action*>& _actionVector, Scope& _scope, bool _positivity) const {
        if (!Context::is_numeric_type(_var.type_id()))
            _traverser.throw_standard_parse_error("re::Parser::increment_variable()", "Expression is not of a numeric type (tried to apply sign -)");

        switch(_var.type_id()) {
        case REMI_BOOL_TYPE:
            REMI_EXPAND_INCREMENT_OPERATION_CASE(bool)
            break;
        case REMI_CHAR_TYPE:
            REMI_EXPAND_INCREMENT_OPERATION_CASE(int)
            break;
        case REMI_INT_TYPE:
            REMI_EXPAND_INCREMENT_OPERATION_CASE(int)
            break;
        case REMI_FLOAT_TYPE:
            REMI_EXPAND_INCREMENT_OPERATION_CASE(float)
            break;
        }

        return _var;
    }

#undef REMI_EXPAND_INCREMENT_OPERATION_CASE

    VariablePlan& Parser::apply_negative_sign(ScriptTraverser& _traverser, const VariablePlan& _var, std::vector<Action*>& _actionVector, Scope& _scope) const {
        if (!Context::is_numeric_type(_var.type_id()))
            _traverser.throw_standard_parse_error("re::Parser::apply_negative_sign()", "Expression is not of a numeric type (tried to apply sign -)");

        VariablePlan* sign = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, _var.type_id()), _scope);

        switch (_var.type_id()) {
        case REMI_INT_TYPE:
            _actionVector.push_back(new ActionWriteConst<int>(generate_variable_reference(*sign, _scope), -1));
            break;
        case REMI_CHAR_TYPE:
            _actionVector.push_back(new ActionWriteConst<char>(generate_variable_reference(*sign, _scope), -1));
            break;
        case REMI_FLOAT_TYPE:
            _actionVector.push_back(new ActionWriteConst<float>(generate_variable_reference(*sign, _scope), -1));
            break;
        default:
            _traverser.throw_standard_parse_error("re::Parser::apply_negative_sign()", "Unsupported numeric type " + std::string(m_targetContext.find_type_name(_var.type_id())));
            break;
        }

        VariablePlan* ret = &generate_anonymous_variable(VariablePlan(_var), _scope);
        generate_variable_operation(_var, *sign, *ret, _scope, RemiOperator::Multiply, _actionVector, ParseSnapshot(_traverser));

        return *ret;
    }

#define PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES(___type_name) \
    switch(_to.type_id()) {                                      \
    case REMI_BOOL_TYPE:                                         \
        _actionVector.push_back(new ActionSpecialBoolCast<___type_name>(generate_variable_reference(_from, _scope), generate_variable_reference(*ret, _scope))); \
        break;                                                   \
    case REMI_CHAR_TYPE:                                         \
        _actionVector.push_back(new ActionCastValue<___type_name, char>(generate_variable_reference(_from, _scope), generate_variable_reference(*ret, _scope))); \
        break;                                                   \
    case REMI_INT_TYPE:                                          \
        _actionVector.push_back(new ActionCastValue<___type_name, int>(generate_variable_reference(_from, _scope), generate_variable_reference(*ret, _scope))); \
        break;                                                   \
    case REMI_FLOAT_TYPE:                                        \
        _actionVector.push_back(new ActionCastValue<___type_name, float>(generate_variable_reference(_from, _scope), generate_variable_reference(*ret, _scope))); \
        break;                                                   \
    default:                                                     \
        std::ostringstream ss;                                   \
        ss << "No known type cast exists for types ";            \
        ss << m_targetContext.full_type_name(_from) << " and " << m_targetContext.full_type_name(_to); \
        _traverser.throw_standard_parse_error("re::Parser::generate_value_type_cast()", ss.str()); \
    }

    VariablePlan& Parser::generate_value_type_cast(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _from, VariablePlan& _to, bool _allowDirectWrite) const {
        VariablePlan* ret = nullptr;
        
        if (_allowDirectWrite) {
            ret = &_to;
        }
        else {
            ret = &generate_anonymous_variable(VariablePlan(_to), _scope);
        }

        switch(_from.type_id()) {
        case REMI_BOOL_TYPE:
            PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES(bool);
            break;
        case REMI_CHAR_TYPE:
            PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES(char);
            break;
        case REMI_INT_TYPE:
            PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES(int);
            break;
        case REMI_FLOAT_TYPE:
            PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES(float);
            break;
        default:
            std::ostringstream ss;
            ss << "No known type cast exists for types ";
            ss << m_targetContext.full_type_name(_from) << " and " << m_targetContext.full_type_name(_to);
            _traverser.throw_standard_parse_error("re::Parser::generate_value_type_cast()", ss.str());
        }

        return *ret;
    }

#undef PARSER_GENERATE_VALUE_TYPE_CONVERSION_CASES

    VariablePlan& Parser::generate_binary_operation(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _left, RemiOperator _operator, VariablePlan& _directReturn) const {
        VariablePlan* right = nullptr;
        VariablePlan* ret = nullptr;
        VariablePlan rightDirectDummy = VariablePlan::uninitialized_variable(_scope);
        ParseSnapshot snapshot(_traverser);

        if (remi_is_logic_operator(_operator)) {
            if (_directReturn.type_id() == REMI_BOOL_TYPE) ret = &_directReturn;
            else ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), _scope);
        }
        else if (_left.can_equate(_directReturn)) {
            if (_directReturn.type_id() == REMI_ANY_TYPE) ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Mutable, ValueType::Value, _left.type_id()), _scope);
            else ret = &_directReturn;
        }
        else {
            ret = &generate_anonymous_variable(VariablePlan(_left), _scope);
        }

        right = &parse_subexpression(_traverser, _actionVector, _scope, rightDirectDummy);

        if (!_left.can_equate(*right)) {
            right = &generate_value_type_cast(_traverser, _actionVector, _scope, *right, _left);
        }

        generate_variable_operation(_left, *right, *ret, _scope, _operator, _actionVector, snapshot);

        return *ret;
    }

#define REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(___op)             \
    case ___op:                                                             \
        return new ActionBinaryOperation<T, ___op>(generate_variable_reference(_left, _scope), generate_variable_reference(_right, _scope), generate_variable_reference(_return, _scope)); \
        break;

    template<class T>
    Action* Parser::generate_variable_operation_action(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, ParseSnapshot _snapshot) const {
        switch(_operator) {
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Add)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Subtract)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Multiply)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Divide)

        case RemiOperator::Modulo:
            if (_left.type_id() == REMI_INT_TYPE)
                return new ActionBinaryOperation<int, RemiOperator::Modulo>(generate_variable_reference(_left, _scope), generate_variable_reference(_right, _scope), generate_variable_reference(_return, _scope));
            break;

        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Less)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::LessOrEqual)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Greater)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::GreaterOrEqual)

        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::And)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Or)
        
        case RemiOperator::Not:
            break;

        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::Equal)
        REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR(RemiOperator::NotEqual)
        default:
            break;
        }

        std::ostringstream ss;
        ss << "Operator " << remi_operator_str(_operator) << " for types " << m_targetContext.find_type_name(_left.type_id()) << " and " << m_targetContext.find_type_name(_right.type_id()) << " isn't supported";
        _snapshot.throw_standard_parse_error("re::Parser::generate_print_for_expression()", ss.str());

        return nullptr;
    }

#undef REMI_EXPAND_VARIABLE_OPERATION_CASE_FOR_OPERATOR

    template Action* Parser::generate_variable_operation_action<int>(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, ParseSnapshot _snapshot) const;
    template Action* Parser::generate_variable_operation_action<float>(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, ParseSnapshot _snapshot) const;

    void Parser::generate_variable_operation(const VariablePlan& _left, const VariablePlan& _right, const VariablePlan& _return, Scope& _scope, RemiOperator _operator, std::vector<Action*>& _actionVector, ParseSnapshot _snapshot) const {
        switch(_left.type_id()) {
        case REMI_BOOL_TYPE:
            _actionVector.push_back(generate_variable_operation_action<bool>(_left, _right, _return, _scope, _operator, _snapshot));
            break;
        case REMI_CHAR_TYPE:
            _actionVector.push_back(generate_variable_operation_action<char>(_left, _right, _return, _scope, _operator, _snapshot));
            break;
        case REMI_INT_TYPE:
            _actionVector.push_back(generate_variable_operation_action<int>(_left, _right, _return, _scope, _operator, _snapshot));
            break;
        case REMI_FLOAT_TYPE:
            _actionVector.push_back(generate_variable_operation_action<float>(_left, _right, _return, _scope, _operator, _snapshot));
            break;
        default:
            std::ostringstream ss;
            ss << "Type " << m_targetContext.find_type_name(_left.type_id()) << " isn't supported for operations";
            _snapshot.throw_standard_parse_error("re::Parser::generate_print_for_expression()", ss.str());
            break;
        }
    }



}