#include <Remi/Parser.hpp>

#include <Remi/Remi.hpp>

namespace re {

    VariablePlan& Parser::parse_engine_directive(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope) const {
        _traverser.expect_symbol('$');

        _traverser.next();
        std::string directive(_traverser.current_token().str());
        _traverser.next();

        try {
            if (directive == "program_argument") {
                _traverser.expect_symbol('('); _traverser.next();
                _traverser.expect_symbol('"'); _traverser.next();

                std::string_view argName = _traverser.current_token().str();
                _traverser.next();

                VariablePlan& str = generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Array, REMI_CHAR_TYPE), _scope);
                _actionVector.push_back(new ActionWriteConstStringPtr(generate_variable_reference(str, _scope), m_remi.arguments().find_argument(argName)));

                _traverser.expect_symbol('"'); _traverser.next();
                _traverser.expect_symbol(')');_traverser.next();

                return str;
            }
            else if (directive == "define") {
                _traverser.expect_symbol('('); _traverser.next();
                _traverser.expect_symbol('"'); _traverser.next();

                std::string argName(_traverser.current_token().str());
                _traverser.next();

                if (m_remi.arguments().is_argument_set(argName))
                    _traverser.throw_standard_parse_error("re::Parser::parse_engine_directive()", "An attempt was made to re-define an existing program argument \'" + argName + "\'");

                _traverser.expect_symbol('"'); _traverser.next();
                _traverser.expect_operator(RemiOperator::Define);
                _traverser.next();
                _traverser.expect_symbol('"'); _traverser.next();

                m_remi.arguments().register_argument(argName, _traverser.current_token().str());
                _traverser.next();

                _traverser.expect_symbol('"'); _traverser.next();
                _traverser.expect_symbol(')'); _traverser.next();
            }
            else _traverser.throw_standard_parse_error("re::Parser::parse_engine_directive()", "Uknown engine directive " + directive);
        }
        catch(ProgramArgumentLookupException& _e) {
            throw ParsingException(std::move(_e.exception_stack_ptr()), _traverser.current_line(), _traverser.current_token_pos(), std::string(_e.what()) + "\n -> Requested name: " + std::string(_e.requested()));
        }

        return generate_anonymous_variable(VariablePlan::void_variable(_scope), _scope);
    }

}