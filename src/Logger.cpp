#include <Remi/Logger.hpp>

#include <Remi/Error.hpp>
#include <Remi/ThreadStream.hpp>

#include <iostream>
#include <iomanip>

#include <sstream>

#include <ctime>
#include <chrono>

#include <thread>
#include <filesystem>

namespace re {

    Logger::Logger(LogMode _mode)
        : m_filename("log.txt") {
        set_mode(_mode);
    }

    Logger::Logger(LogMode _mode, std::string_view _filename)
        : m_filename(_filename) {
        set_mode(_mode);
    }

    LogMode Logger::mode() const {
        return m_mode;
    }

    LogMode Logger::set_mode(LogMode _mode) {
        m_mode = _mode;
        return m_mode;
    }

    void Logger::set_filename(std::string_view _filename) {
        m_filename = _filename;
    }

    void Logger::log(LogMode _mode, const std::string& _str) const {
        switch(_mode) {
        case LogMode::Console:
            println(_str);
            break;

        case LogMode::File:
            file_log(_str);
            break;

        case LogMode::ConsoleAndFile:
            file_log(_str);
            println(_str);
            break;

        default:
            std::ostringstream ss;
            ss << "Invalid re::Logger::LogMode (bad logger: " << this << ")";

            throw(ProgramException(std::make_unique<ExceptionStack>("re::Logger::log()"), ss.str()));
            break;
        }
    }

    void Logger::log(const std::string& _str) const {
        log(m_mode, _str);
    }

    void Logger::file_log(const std::string& _str) const {
        auto now = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(now);

        std::ofstream file;

        if (!std::filesystem::exists(m_filename)) clear_file();

        open_file(file, std::ios::app);
        file << '[' << std::put_time(std::localtime(&in_time_t), "%d-%m-%Y, %X")  << "]:\n" << _str << std::endl;
        file.close();
    }

    void Logger::print(const std::string& _str) {
        ThreadStream(std::cout) << _str;
    }

    void Logger::println(const std::string& _str) {
        ThreadStream(std::cout) << _str << std::endl;
    }

    void Logger::open_file(std::ofstream& _file, std::ios::openmode _option) const {
        if (!std::filesystem::exists(m_filename))
            throw(ProgramException(std::make_unique<ExceptionStack>("re::Logger::open_file()"), "Failed to open a file for logging (filename = " + m_filename + ")\n -> File doesn't exist or couldn't be located"));

        _file.open(m_filename, _option);
    }

    void Logger::clear_file() const {
        std::ofstream file;

        file.open(m_filename, std::ios::trunc);
        file.close();
    }

    LogStream::LogStream(const Logger& _logger) : m_logger(_logger) {}

    LogStream::~LogStream() {
        if (!(this->str().empty())) {
            log();
        }
    }

    void LogStream::log() {
        m_logger.log(this->str());

        this->clear();
        this->str("");
    }

}