#include <Remi/Remi.hpp>

#include <Remi/Default.hpp>
#include <Remi/Parser.hpp>

#include <Remi/RemiTutorial.hpp>

#include <Remi/ExecutionContext.hpp>
#include <Remi/FunctionInstance.hpp>

#include <iostream>

namespace re {

    Remi::Remi(int _argc, char* _argv[])
        : m_arguments(nullptr), m_logger(std::make_unique<Logger>(LogMode::Console)), m_context(std::make_unique<Context>(*m_logger)), m_startTime(nullptr) {
        init(_argc, _argv);
    }
    
    Remi::Remi(std::unique_ptr<ProgramArguments> _arguments) 
        : m_arguments(std::move(_arguments)), m_logger(std::make_unique<Logger>(LogMode::Console)), m_context(std::make_unique<Context>(*m_logger)), m_startTime(nullptr) {
        init();
    }

    void Remi::init(int _argc, char* _argv[]) {
        m_verbosity = 0;

        try {
            if (m_arguments == nullptr) m_arguments = std::make_unique<ProgramArguments>(_argc, _argv);

            determine_run_mode();
            determine_verbosity();
            init_logger();
        }
        catch (ProgramArgumentUseException& _e) {
            report_program_argument_use_exception("re::Remi::Remi()", _e);
        }
        catch(ProgramArgumentInputException& _e) {
            report_program_argument_input_exception("re::Remi::Remi()", _e);
        }
        catch(ProgramArgumentException& _e) {
            report_program_argument_exception("re::Remi::Remi()", _e);
        }
    }

    void Remi::init_logger() {
        LogMode mode = determine_log_mode();
        std::string loggerFilename = std::string(m_arguments->find_argument("remi::log_file::filename"));

        if (loggerFilename == "default")
            loggerFilename = "log.txt";
        else if (loggerFilename == "auto") {
            std::ostringstream ss;
            Time now;

            ss << "log_" << now.day() << '-' << now.month() << '-' << now.year() << '_';
            ss << now.hour() << '-' << now.minute() << '-' << now.second() << ".txt";

            loggerFilename = ss.str();
        }

        m_logger->set_mode(mode);
        m_logger->set_filename(loggerFilename);

        if (mode != LogMode::Console) m_logger->clear_file();
    }

    const Time& Remi::start_time() const {
        if (m_startTime == nullptr) {
            throw(ProgramException(std::make_unique<ExceptionStack>("re::Remi::start_time()"), "re::Remi::start_time() accessed before Remi was started"));
        }
        return (*m_startTime);
    }

    const ProgramArguments& Remi::arguments() const noexcept {
        return (*m_arguments);
    }

    ProgramArguments& Remi::arguments() noexcept {
        return (*m_arguments);
    }

    const Logger& Remi::logger() const {
        return (*m_logger);
    }

    void Remi::start() {
        std::string fname("re::Remi::start()");

        try {
            m_startTime = std::make_unique<Time>();

            if (m_arguments->is_argument_set("remi::usage_demo")) {
                print_tutorial();
                return;
            }

            if (m_arguments->is_argument_set("remi::program_variables"))
                print_program_arguments();

            if (m_arguments->is_argument_set("remi::print_keywords"))
                print_keywords();

            if (m_arguments->is_argument_set("remi::print_operators"))
                print_operators();

            switch(m_mode) {
            case RemiMode::Version:
                print_version();

                if (m_arguments->is_argument_set("remi::print_type_sizes")) {
                    parse_main_script();
                    print_type_sizes();
                }
                else if (m_arguments->is_argument_set("remi::print_basic_type_sizes"))
                    print_basic_type_sizes();

                break;

            case RemiMode::Help:
                print_version();
                print_help();
                break;
            
            case RemiMode::LoadAndRun:
                parse_main_script();
                //m_context->create_script(nullptr, m_arguments->find_argument("remi::load_script::filename"));

                if (m_arguments->is_argument_set("remi::print_tokens")) {
                    LogStream ls(*m_logger);
                    ls << "[Printing all tokens]: <flag -T>\n";

                    for(auto& p : m_context->scripts()) {
                        ls << "\n[Script " << p.first << "]:\n";
                        for(const Line& l : p.second->lines()) {
                            ls << l.number() << '/' << l.relative_number() << ": |";
                            for(const Token& t : l.tokens()) {
                                ls << t.str() << '|';
                            }
                            ls << '\n';
                        }
                    }
                }

                execute_main_script();
                break;

            default:
                throw(ProgramException(std::make_unique<ExceptionStack>(fname), "Remi started with invalid run mode (RemiMode::InvalidRunMode set)\n -> Are you missing a program option?\n -> Run Remi with [-h] or [--help] for a list of all options"));
                break;
            }

            if (m_arguments->is_argument_set("remi::load_script") && (m_mode != RemiMode::LoadAndRun)) {
                LogStream ls(*m_logger);
                ls << "[Note]: A script was given using the [-l] flag but its execution has been shadowed by other program arguments (probably [-h] or [-v])";
            }
        }
        catch(ProgramArgumentLookupException& _e) {
            report_program_argument_lookup_exception(fname, _e);
        }
        catch(ProgramArgumentUseException& _e) {
            report_program_argument_use_exception(fname, _e);
        }
        catch(ProgramArgumentInputException& _e) {
            report_program_argument_input_exception(fname, _e);
        }
        catch(ProgramArgumentException& _e) {
            report_program_argument_exception(fname, _e);
        }
        catch(ScriptException& _e) {
            report_script_exception(fname, _e);
        }
    }

    void Remi::parse_main_script() {
        double time[2];
        Clock clock;    

        std::string fname = "re::Remi::load_main_script()";

        try {
            Parser p(*this, *m_context);

            clock.restart();
            Script& mainScript = m_context->create_script(nullptr, m_arguments->find_argument("remi::load_script::filename"));

            // Read time
            time[0] = clock.restart();

            p.parse_script(mainScript);

            // Parse time
            time[1] = clock.restart();  

            if (!m_context->root()->function_exists("main")) {
                throw(FunctionLookupException(std::make_unique<ExceptionStack>("re::Remi::parse_main_script()"), (*m_context->root()), "main function not defined", "main"));
            }
        }
        catch(ScriptLookupException& _e) {
            report_script_lookup_exception(fname, _e);
        }
        catch(ScopeDuplicateException& _e) {
            report_scope_duplicate_exception(fname, _e);
        }
        catch(ScopeLookupException& _e) {
            report_scope_lookup_exception(fname, _e);
        }
        catch(VariableLookupException& _e) {
            report_variable_lookup_exception(fname, _e);
        }
        catch(FunctionLookupException& _e) {
            report_function_lookup_exception(fname, _e);
        }

        if (m_verbosity >= 2) {
            LogStream ls(*m_logger);

        #if REMI_PRINT_ERROR_CLASS_INFO
            ls << "[->] [re::Remi @" << this << ", re::Remi::load_main_script()]:\n";
        #endif
            ls << "[Time measurements #1]:\n";
            ls << " -> Read and tokenize time: " << time[0] << "s\n";
            ls << " -> Parse time: " << time[1] << "s\n";
        }
    }

    void Remi::execute_main_script() {
        double time;
        Clock clock;

        // Start measuring execute time
        time = clock.restart();

        // Main thread execution context
        ExecutionContext mainExecutionContext(*m_context);

        // Run actions that initialize static variables
        mainExecutionContext.execute_init_functions();

        // Ensure that the main() function exists within this Remi instance
        if (!m_context->root()->function_exists("main"))
            throw(FunctionLookupException(std::make_unique<ExceptionStack>("re::Remi::execute_main_script()"), (*m_context->root()), "main function not defined", "main"));

        mainExecutionContext.init_function_call(m_context->root()->find_function("main"));
        mainExecutionContext.execute_current_function();
        mainExecutionContext.end_function_call();

        // Save the elapsed time
        time = clock.restart();

         if (m_verbosity >= 2) {
            LogStream ls(*m_logger);
        #if REMI_PRINT_ERROR_CLASS_INFO
            ls << "[->] [re::Remi @" << this << ", re::Remi::execute_main_script()]:\n";
        #endif
            ls << "\n[Time measurements #2]:\n";
            ls << " -> Main script execution time: " << time;
        }
    }

    void Remi::print_version() const {
        LogStream ls(*m_logger);
        
    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::print_version()]:\n";
    #endif
        ls << remi_release_str() << " [" << RemiCompilerName << ' ' << remi_compiler_version_str() << "]\n";
        ls << "Which: " << m_arguments->find_argument("remi::which") << '\n';
    }

    void Remi::print_help() const {
        LogStream ls(*m_logger);

        ls << "Remi is an interpreted programming language currently in development.\n";
        ls << "To run a Remi program, use the [-l] flag with the filename of the main script.\n";
        ls << "Use the flag [-h] or [--help] to get more information on all available options\n";
        ls << "Visit https://gitlab.com/pdevic/remi for more information.\n";
        ls << "\n -> Use the flag [-d] or [--usage_demo] for a short tutorial on main features of the language\n";
        ls << " -> The demo flag expects one of the following options: \"variables\", \"types\", \"functions\", \"conditions\" or \"loops\"\n\n";

        for(auto p : RemiProgramArguments) {
            const ProgramArgumentMeta& cam{p.second};
            decltype(cam.arguments()) args(cam.arguments());

            std::ostringstream ss;
            ss << ":  [-" << cam.flag() << ", --" << cam.name() << ']';

            if (!args.empty()) {
                ss << " [" << args.front();

                for(size_t i{1}; i < args.size(); i++) ss << ", " << args[i];
                ss << ']';
            }

            ls << ss.str() << ' ' << cam.description() << '\n';
        }
    }

    void Remi::print_tutorial() const {
        LogStream ls(*m_logger);
        std::string topic(m_arguments->find_argument("remi::usage_demo::option"));

        ls << "\nWelcome to the Remi tutorial! Here I will try to teach you the basics of Remi through simple examples\n";
        ls << " -> Available topics are: \"variables\", \"types\", \"functions\", \"conditions\" and \"loops\"\n";
        ls << " -> You have chosen the following topic: " << topic << "\n";
        ls << " -> Remember, every Remi program starts its execution with the main() function!\n\n";
        ls << "====================================| " << topic << " |=======================================\n\n\n";

        decltype(RemiTutorialVariables)* linesVector = nullptr;

        if (topic == "variables") linesVector = &RemiTutorialVariables;
        else if (topic == "types") linesVector = &RemiTutorialTypes;
        else if (topic == "functions") linesVector = &RemiTutorialFunctions;
        else throw(ProgramException(std::make_unique<ExceptionStack>("re::Remi::print_tutorial()"), "Unknown tutorial topic " + topic));

        for(auto& s : *linesVector) ls << "    " << s;

        ls << "\n\n==============================| End of " << topic << " tutorial |==============================";
    }

    void Remi::print_program_arguments() const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::print_program_arguments()]:\n";
    #endif
        ls << "[re::Remi program argument variables]:\n";

        for(auto p : m_arguments->arguments()) {
            ls << p.first << " = \"" << p.second << "\"\n";
        }
    }

    void Remi::print_keywords() const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::print_keywords()]:\n";
    #endif
        ls << "[Remi keywords]:\n";

        for(auto p : RemiKeywords) {
            ls << ": " << p.first << "\n";
        }
    }

    void Remi::print_operators() const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::print_operators()]:\n";
    #endif
        ls << "[Supported operators]:\n";

        for(auto p : RemiOperators) {
            ls << ": " << p.first << "\n";
        }
    }

    void Remi::print_type_sizes() const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::print_type_sizes()]:\n";
    #endif

        ls << "Type sizes in bytes [flag -ts]:\n";

        for(auto t : m_context->types_by_name()) {
             ls << ": " << t.first << "\t\t" << m_context->get_type_byte_size(t.second) << '\n';
        }
    }

    void Remi::print_basic_type_sizes() const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::print_basic_type_sizes()]:\n";
    #endif

        ls << "Built-in type sizes in bytes [flag -bts]:\n";

        for(auto s : {"bool", "char", "int", "float"})
            ls << ": " << s << "\t\t" << m_context->get_type_byte_size(m_context->find_type_id(s)) << '\n';
    }

    void Remi::parse_program_arguments(int _argc, char* _argv[]) {
        m_arguments->parse_program_arguments(_argc, _argv);
    }

    void Remi::determine_run_mode() {
        if (m_arguments->is_argument_set("remi::version")) {
            m_mode = RemiMode::Version;
        }
        else if (m_arguments->is_argument_set("remi::help")) {
            m_mode = RemiMode::Help;
        }
        else if (m_arguments->is_argument_set("remi::print_keywords")
                 || m_arguments->is_argument_set("remi::print_operators")
                 || m_arguments->is_argument_set("remi::print_type_sizes")
                 || m_arguments->is_argument_set("remi::print_basic_type_sizes")) {
            m_mode = RemiMode::Version;
        }
        else { 
            if (m_arguments->is_argument_set("remi::load_script")) m_mode = RemiMode::LoadAndRun;
            else m_mode = RemiMode::InvalidRunMode;
        }
    }

    void Remi::determine_verbosity() {
        std::istringstream ss{std::string(m_arguments->find_argument("remi::verbosity::value"))};

        // Try to convert the string value of verbosity from the program argument into an int
        if (!(ss >> m_verbosity)) {
            // Fail miserably
            throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::Remi::determine_verbosity()"), (*m_arguments), "Couldn't determine verbosity. Value should be an integer [0-4]", ss.str()));
        }

        if ((m_verbosity > 4) || (m_verbosity < 0)) {
            throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::Remi::determine_verbosity()"), (*m_arguments), "Invalid verbosity value. Accepted values are [0-4]", ss.str()));
        }
    }

    LogMode Remi::determine_log_mode() const {
        std::istringstream ss{std::string(m_arguments->find_argument("remi::log_mode::value"))};

        size_t value;
        LogMode mode;

        if (!(ss >> value)) {
            throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::Remi::determine_log_mode()"), (*m_arguments), "Couldn't determine log mode. Value should be an integer [0-2]", ss.str()));
        }

        switch(value) {
            case 0:
                mode = LogMode::Console;
                break;
            case 1:
                mode = LogMode::File;
                break;
            case 2:
                mode = LogMode::ConsoleAndFile;
                break;
            default:
                throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::Remi::determine_log_mode()"), (*m_arguments), "Invalid log mode value. Accepted values are [0-2]", ss.str()));
                break;
        }

        return mode;
    }

}