#include <Remi/ProgramArguments.hpp>

#include <Remi/Default.hpp>
#include <Remi/Error.hpp>

#include <sstream>

namespace re {

    ProgramArguments::ProgramArguments() {
        init_default();
    }

    ProgramArguments::ProgramArguments(int _argc, char* _argv[]) {
        init_default();
        parse_program_arguments(_argc, _argv);
    }

    const std::map<std::string, std::string>& ProgramArguments::arguments() const {
        return m_arguments;
    }

    std::string_view ProgramArguments::find_argument(std::string_view _arg) const {
        auto it{m_arguments.find(std::string(_arg))};

        if (it == m_arguments.end()) {
            throw(ProgramArgumentLookupException(std::make_unique<ExceptionStack>("re::ProgramArguments::find_argument()"), _arg, "Requested program argument doesn't exist"));
        }

        return (it->second);
    }

    std::string_view ProgramArguments::operator[](const std::string& _arg) const {
        return find_argument(_arg);
    }

    std::string_view ProgramArguments::operator[](std::string_view _arg) const {
        return find_argument(_arg);
    }

    bool ProgramArguments::is_argument_set(std::string_view _arg) const {
        return (m_arguments.find(std::string(_arg)) != m_arguments.end());
   }

    size_t ProgramArguments::parse_program_arguments(int _argc, char* _argv[]) {
        size_t argCount{0};

        // Add remi executable path to arguments
        m_arguments["remi::which"] = _argv[0];

        for(int i = 1; i < _argc; i++) {
            std::string_view arg{_argv[i]}; // std::string_view wrapper for original c-string from argv to make it easier to use

            std::string flag; // Used to fetch the ProgramArgumentMeta instance
            std::string name; //

            size_t argCount; // The number of arguments the program option expects
            size_t valueStart; // Used to determine whether the value was set next to the flag or name

            size_t j{0};     // Changed from 0 to 1 if first value is set next to the flag

            if (arg.substr(0, 2) == "--") {
                auto p = (*deduce_argument_name(arg)); // Argument format is [--nameARG], ARG might be empty
                name = p.first;
                flag = p.second;

                valueStart = 2 + name.size();
            }
            else if (arg.front() == '-') {
                auto p = (*deduce_argument_flag(arg));
                flag = p.first;
                name = p.second;

                valueStart = 1 + flag.size(); // Marks the "end" of the flag or name
            }
            else {
                throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), "Program argument didn't start with \'-\' or \'--\'", arg));
            }

            const ProgramArgumentMeta& cam{RemiProgramArguments.at(flag)};

            argCount = cam.arguments().size();
            m_arguments["remi::" + name] = "<set>[-" + flag + "]";

            if ((argCount == 0) && (valueStart != arg.size())) {
                if (arg[valueStart] == '-')
                    throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), "A flag that doesn't accept arguments was given a value (did you forget to separate two flags with a whitespace?)", arg));
                throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), "A flag that doesn't accept arguments was given a value", arg));
            }
            else if ((argCount > 1) && (valueStart < arg.size())) {
                throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), "Multiple arguments should be separated from the argument flag or name", arg));
            }
            else if ((argCount == 1) && (arg.substr(0, 2) == "--")) {
                if (arg[valueStart] != '=') {
                    throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), "Expected \"=\" before argument value", arg));
                }

                valueStart++;
            }

            if (valueStart < arg.size()) {
                j = 1;
                m_arguments["remi::" + name + "::" + cam.arguments()[0]] = arg.substr(valueStart, arg.size());
            }

            for(; j < argCount; j++) {
                i++;

                if (i >= _argc) {
                    std::stringstream ss;

                    ss << "Not enough arguments for flag [-" << flag << ", --" << name;
                    ss << "]: " << argCount << " expected, " << j << " received";

                    throw(ProgramArgumentUseException(std::make_unique<ExceptionStack>("re::ProgramArguments::parse_program_arguments()"), (*this), cam, ss.str(), arg));
                }

                m_arguments["remi::" + name + "::" + cam.arguments()[j]] = _argv[i];
            }
        }

        if (is_argument_set("remi::load_script")) {
            m_arguments["remi::mode"].append("l");
        }

        if (is_argument_set("remi::interactive_mode")) {
            m_arguments["remi::mode"].append("I");
        }

        return argCount;
    }

    void ProgramArguments::register_argument(std::string_view _argName, std::string_view _argValue) {
        m_arguments[std::string(_argName)] = std::string(_argValue);
    }

    void ProgramArguments::init_default()  {
        std::stringstream ss;
        std::stringstream ssv;

        m_arguments["remi::which"] = "<Unknown>";

        m_arguments["remi::compiler"] = RemiCompilerName;
        m_arguments["remi::compiler::version"] = remi_compiler_version_str();
        m_arguments["remi::platform"] = RemiPlatformName;
        m_arguments["remi::model"] = RemiModelName;

        ss << RemiVersion::major() << ' ' << RemiVersion::minor() << ' ' << RemiVersion::build();
        ssv << RemiVersion::major() << '.' << RemiVersion::minor() << '.' << RemiVersion::build();

        m_arguments["remi::version::full"] = ssv.str();
        ss >> m_arguments["remi::version::major"];
        ss >> m_arguments["remi::version::minor"];
        ss >> m_arguments["remi::version::build"];

        m_arguments["remi::mode"] = "";

        m_arguments["remi::verbosity::value"] = "0";
        m_arguments["remi::log_mode::value"] = "0";
        m_arguments["remi::log_file::filename"] = "default";
    }

    decltype(RemiProgramArgumentFlags)::const_iterator ProgramArguments::deduce_argument_flag(std::string_view _str) const {
        std::stringstream ss;
        auto last = RemiProgramArgumentFlags.end();

        if (_str.empty()) {
            throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::find_argument_flag()"), *this, "Expected a flag name", _str));
        }

        for(size_t i{1}; i < _str.size(); i++) {
            ss << _str[i];

            auto it = RemiProgramArgumentFlags.find(ss.str());

            if (it != RemiProgramArgumentFlags.end())
                last = it;
        }

        if (last != RemiProgramArgumentFlags.end())
            return last;

        throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::find_argument_flag()"), *this, "Uknown program argument flag", _str));
    }

    decltype(RemiProgramArgumentNames)::const_iterator ProgramArguments::deduce_argument_name(std::string_view _str) const {
        std::stringstream ss;
        auto last = RemiProgramArgumentNames.end();

        if (_str.empty()) {
            throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::find_argument_flag()"), *this, "Expected a flag name", _str));
        }

        for(size_t i{2}; i < _str.size(); i++) {
            ss << _str[i];
            auto it = RemiProgramArgumentNames.find(ss.str());

            if (it != RemiProgramArgumentNames.end())
                last = it;
        }

        if (last != RemiProgramArgumentNames.end())
            return last;

        throw(ProgramArgumentInputException(std::make_unique<ExceptionStack>("re::ProgramArguments::find_argument_name()"), *this, "Uknown program argument flag", _str));
    }

}