#include <Remi/ScopeSpace.hpp>

#include <Remi/Error.hpp>

#include <memory>
#include <sstream>

//#include <iostream>

namespace re {

    ScopeSpace::ScopeSpace(Scope* _rootParent) {
        m_scope = register_scope(_rootParent, ScopeType::ProgramBlock, "");
        m_namedScopes[""] = m_scope;
    }

    Scope* ScopeSpace::scope() noexcept {
        return m_scope;
    }

    const Scope* ScopeSpace::scope() const noexcept {
        return m_scope;
    }

    const std::map<std::string, Scope*>& ScopeSpace::scopes() const noexcept {
        return m_namedScopes;
    }

    const ScopeMap& ScopeSpace::scope_map() const noexcept {
        return m_scopeLevels;
    }

    bool ScopeSpace::scope_exists(std::string_view _name) const {
        return (m_namedScopes.find(std::string(_name)) != m_namedScopes.end());
    }

    Scope* ScopeSpace::find_scope(std::string_view _name) {
        auto it = m_namedScopes.find(std::string(_name));

        if (it == m_namedScopes.end()) {
            throw(ScopeLookupException(std::make_unique<ExceptionStack>("re::ScopeSpace::find_scope()"), (*this), "No scope found with requested name", _name));
        }

        return (*it).second;
    }

    Scope* ScopeSpace::operator[](std::string_view _name) {
        return find_scope(_name);
    }

    const std::unordered_map<size_t, std::unique_ptr<Scope>>& ScopeSpace::operator[](size_t _depth) const {
        auto it = m_scopeLevels.find(_depth);

        if (it == m_scopeLevels.end()) {
            std::ostringstream ss;

            ss << "<size_t _depth = " << _depth << '>';
            throw(ParseLookupException(std::make_unique<ExceptionStack>("re::ScopeSpace::operator[]()"), (*m_scope), "Failed to locate scope at requested depth", ss.str()));
        }

        return m_scopeLevels.at(_depth);
    }

    Scope* ScopeSpace::register_scope(Scope* _parent, ScopeType _type, std::string_view _name) {
        std::unique_ptr<Scope> newScope(new Scope(_parent, _type));

        if (!_name.empty()) {
            if (scope_exists(_name)) {
                throw(ScopeDuplicateException(std::make_unique<ExceptionStack>("re::ScopeSpace::register_scope()"), (*this), "Scope with given name already exists", _name));
            }
            else {
                m_namedScopes[std::string(_name)] = newScope.get();
            }
        }

        newScope->set_id(m_scopeLevels[newScope->depth()].size());
        return (m_scopeLevels[newScope->depth()][newScope->id()] = std::move(newScope)).get();
    }

    Scope& ScopeSpace::generate_inline_anonymous_scope(Scope* _parent) {
        std::unique_ptr<Scope> newScope(new Scope(_parent, ScopeType::InlineBlock));

        newScope->set_depth(_parent->depth());
        newScope->set_id(_parent->id());

        m_anonymousScopes.push_back(std::move(newScope));
        return *m_anonymousScopes.back();
    }

}