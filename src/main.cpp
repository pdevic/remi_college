
#include <Remi/Remi.hpp>

int main(int argc, char* argv[]) {
    re::Logger l(re::LogMode::Console);

#if REMI_PRINT_EXEC_TIME
    re::Clock clock;
#endif

    try {
        re::Remi remi(argc, argv);
        remi.start();
    }
    catch (re::ProgramException& _e) {
        re::LogStream ls(l);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [main(), re::ExceptionStack @" << (&_e.exception_stack()) << "]:\n";
    #endif

        if (_e.exception_stack().depth() == 1) {
            ls << "[Error]: Remi caught a fatal program exception:\n";
            ls << " -> " << _e.what() << '\n';
        #if REMI_PRINT_EXCEPTION_STACK
            ls << '\n';
        #endif
        }

    #if REMI_PRINT_EXCEPTION_STACK
        ls << "[Exception stack]: <exception type> : <caught by>\n";
        _e.exception_stack().add_name(_e, "main()");

        for(std::string name : _e.exception_stack().names()) {
            ls << " | " << name << '\n';
        }
    #endif

    #if REMI_PRINT_EXEC_TIME
        ls << "\nFinished in " << clock.seconds() << "s\nExit code -1 [Errors thrown]";
    #endif

        return -1;
    }
    catch(re::Exception& _e) {
        re::LogStream ls(l);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [main(), re::ExceptionStack @" << (&_e.exception_stack()) << "]:\n";
    #endif

        if (_e.exception_stack().depth() == 1) {
            ls << "[Error]: Remi caught a generic re::Exception (possibly a stray exception)\n";
            ls << " -> " << _e.what() << '\n';
        #if REMI_PRINT_EXCEPTION_STACK
            ls << '\n';
        #endif
        }

    #if REMI_PRINT_EXCEPTION_STACK
        ls << "[Exception stack]: <exception type> : <caught by>\n";
        _e.exception_stack().add_name(_e, "main()");

        for(std::string name : _e.exception_stack().names()) {
            ls << " | " << name << '\n';
        }
    #endif
    }
    catch(...) {
        re::LogStream ls(l);
        ls << "[Error]: Remi caught an unhandled exception";

        throw;
    }

#if REMI_PRINT_EXEC_TIME
    re::LogStream(l) << "\nFinished in " << clock.seconds() << "s\nExit code 0 [All ok]";
#endif

    return 0;
}