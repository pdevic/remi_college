#include <Remi/Context.hpp>

#include <Remi/Error.hpp>

#include <sstream>
#include <iostream>

namespace re {

    Context::Context(const Logger& _logger, size_t _typeCounter)
    : m_scopeSpace(new ScopeSpace(nullptr)), m_logger(_logger), m_typeCounter(_typeCounter) {

        m_typesByID[REMI_UNINITIALIZED_TYPE] = "none";
        m_typesByName["none"] = REMI_UNINITIALIZED_TYPE;

        m_typesByID[REMI_ANY_TYPE] = "any";
        m_typesByName["any"] = REMI_ANY_TYPE;

        register_type("void",   REMI_VOID_TYPE);
        register_type("bool",   REMI_BOOL_TYPE);
        register_type("char",   REMI_CHAR_TYPE);
        register_type("int",    REMI_INT_TYPE);
        register_type("float",  REMI_FLOAT_TYPE);
        //register_type("double", REMI_DOUBLE_TYPE);

        m_typeByteSizes[REMI_UNINITIALIZED_TYPE] = 0;
        m_typeByteSizes[REMI_ANY_TYPE]    = 0;
        m_typeByteSizes[REMI_VOID_TYPE]   = 0;
        m_typeByteSizes[REMI_BOOL_TYPE]   = sizeof(bool);
        m_typeByteSizes[REMI_CHAR_TYPE]   = sizeof(char);
        m_typeByteSizes[REMI_INT_TYPE]    = sizeof(int);
        m_typeByteSizes[REMI_FLOAT_TYPE]  = sizeof(float);
        //m_typeByteSizes[REMI_DOUBLE_TYPE] = sizeof(double);
    }

    Scope* Context::root() noexcept {
        return m_scopeSpace->scope();
    }

    const Scope* Context::root() const noexcept {
        return m_scopeSpace->scope();
    }

    const std::unordered_map<std::string, std::unique_ptr<Script>>& Context::scripts() const {
        return m_scripts;
    }

    ScopeSpace& Context::scope_space() noexcept {
        return (*m_scopeSpace);
    }

    const ScopeSpace& Context::scope_space() const noexcept {
        return (*m_scopeSpace);
    }

    const Logger& Context::logger() const {
        return m_logger;
    }

    std::vector<FunctionPlan*>& Context::init_functions() {
        return m_initFunctions;
    }

    const std::vector<FunctionPlan*>& Context::init_functions() const {
        return m_initFunctions;
    }

    size_t Context::type_counter() const {
        return m_typeCounter;
    }
    
    const std::map<std::string, size_t>& Context::types_by_name() const {
        return m_typesByName;
    }

    bool Context::script_exists(std::string_view _name) const {
        return (m_scripts.find(std::string(_name)) != m_scripts.end());
    }

    Script& Context::find_script(std::string_view _name) {
        auto it = m_scripts.find(std::string(_name));

        if (it == m_scripts.end()) {
            throw(ScriptLookupException(std::make_unique<ExceptionStack>("re::Context::find_script()"), (*this), _name, "Failed to find a script"));
        }

        return (*it->second);
    }

    const Script& Context::find_script(std::string_view _name) const {
        return find_script(_name);
    }

    bool Context::type_exists(size_t _typeID) const {
        return (m_typesByID.find(_typeID) != m_typesByID.end());
    }

    bool Context::type_exists(std::string_view _typeName) const {
        return (m_typesByName.find(std::string(_typeName)) != m_typesByName.end());
    }

    std::string Context::find_type_name(size_t _typeID) const {
        auto it = m_typesByID.find(_typeID);

        if (it == m_typesByID.end()) {
            std::ostringstream ss;
            ss << "<type ID = " << _typeID << ">";

            if (_typeID == REMI_UNINITIALIZED_TYPE) {
                throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::find_type_name()"), (*root()), "Uninitialized type ID given" + ss.str(), ss.str()));
            }
            else {
                throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::find_type_name()"), (*root()), "Failed to find type name by ID " + ss.str(), ss.str()));
            }
        }

        return (it->second);
    }

    std::string Context::find_type_name(const VariablePlan& _var) const {
        return find_type_name(_var.type_id());
    }

    size_t Context::find_type_id(std::string_view _typeName) const {
        auto it = m_typesByName.find(std::string(_typeName));

        if (it == m_typesByName.end()) {
            throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::find_type_name()"), (*root()), "Unknown type name", _typeName));
        }

        return (it->second);
    }

    std::string Context::type_prefix(ValueType _vType) const {
        if (_vType == ValueType::Array) return "[]";
        else return "";

        /*switch(_vType) {
        case ValueType::Value:
            return "value";
            break;
        case ValueType::Pointer:
            return "ptr";
            break;
        case ValueType::Array:
            return "array";
            break;
        default:
            throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::type_prefix()"), (*root()), "Unhandled enum type requested", "<re::ValueType>"));
            break;
        }*/
    }

    std::string Context::type_prefix(const VariablePlan& _var) const {
        return type_prefix(_var.value_type());
    }

    std::string Context::full_type_name(const VariablePlan& _var) const {
        std::ostringstream ss;

        if (_var.value_constness() == Constness::Const) ss << "const ";
        ss << find_type_name(_var.type_id());

        return ss.str();
    }

    std::string Context::full_function_declaration(std::string_view _fName, const FunctionPlan& _fun) {
        std::ostringstream ss1, ss2;

        ss1 << _fName << '(';
        for(auto aa : _fun.type_plan().arguments()) ss2 << full_type_name(aa.variable_plan()) << " " << aa.name() << ", ";

        ss1 << ss2.str().substr(0, ss2.str().size() - 2);
        ss1 << ") -> " << full_type_name(_fun.type_plan().return_type());

        return ss1.str();
    }

    size_t Context::get_type_byte_size(size_t _typeID) const {
        auto it = m_typeByteSizes.find(_typeID);

        if (it == m_typeByteSizes.end()) {
            std::ostringstream ss;
            ss << "type ID = " << _typeID;

            throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::find_type_name()"), (*root()), "Failed to find type size by ID", ss.str()));
        }

        return (it->second);
    }

    size_t Context::get_scope_byte_size(const Scope& _scope) const {
        size_t res = 0;

        for(auto v : _scope.variables()) {
            switch(v.second->value_type()) {
            case ValueType::Value:
                res += get_type_byte_size(v.second->type_id());
                break;
            default:
                res += sizeof(int*);
            }
        }

        for(auto v : _scope.anonymous_variables()) {
            switch(v->value_type()) {
            case ValueType::Value:

                res += get_type_byte_size(v->type_id());
                break;
            default:
                res += sizeof(int*);
            }
        }

        return res;
    }

    Script& Context::create_script(Script* _parentScript, std::string_view _filename) {
        Script& ref = *(m_scripts[std::string(_filename)] = std::make_unique<Script>(_parentScript, _filename));

        ref.read();
        ref.tokenize();

        return (*m_scripts[std::string(_filename)]);
    }

    void Context::register_type(std::string_view _typeName, size_t _typeID) {
        if (type_exists(_typeID) || type_exists(_typeName)) {
            std::ostringstream ss;
            ss << "Tried to register an already existing type (requested typeID = " << _typeID << ")";

            throw(ParseLookupException(std::make_unique<ExceptionStack>("re::Context::register_type()"), (*root()), ss.str(), _typeName));
        }

        size_t typeID = (_typeID == REMI_UNINITIALIZED_TYPE) ? (m_typeCounter++) : _typeID;

        m_typesByID[typeID] = std::string(_typeName);
        m_typesByName[std::string(_typeName)] = typeID;
    }

    void Context::register_init_function(FunctionPlan& _f) {
        m_initFunctions.push_back(&_f);
    }

    bool Context::is_numeric_type(size_t _typeID) {
        switch (_typeID) {
        case REMI_INT_TYPE:
        case REMI_FLOAT_TYPE:
        case REMI_DOUBLE_TYPE:
            return true;
            break;
        
        default:
            return false;
            break;
        }
    }
}