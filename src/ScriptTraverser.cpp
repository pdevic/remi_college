#include <Remi/ScriptTraverser.hpp>

#include <Remi/Error.hpp>

#include <sstream>

namespace re {

    ScriptTraverser::ScriptTraverser(const Script& _script)
    : m_targetScript(_script) {
        if (_script.lines().empty()) {
            throw(ScriptException(std::make_unique<ExceptionStack>("re::ScriptTraverser::ScriptTraverser()"), _script, "Tried to parse an empty script"));
        }

        m_linesIt = _script.lines().begin();
        m_currentLine = &_script.lines().front();

        if (m_currentLine->tokens().empty()) {
            throw(ScriptException(std::make_unique<ExceptionStack>("re::ScriptTraverser::ScriptTraverser()"), _script, "Tried to parse an empty script"));
        }

        else {
            m_currentToken = &(_script.lines().front().tokens().front());
            m_tokensIt = m_currentLine->tokens().begin();
            m_tokenPos = 0;

            if (m_currentLine->tokens().size() == 1) {
                m_state = AdvanceState::LineEnd;
            }
            else {
                m_state = AdvanceState::Normal;
            }
        }
    }

    const Token& ScriptTraverser::current_token() const {
        return (*m_currentToken);
    }

    const Line& ScriptTraverser::current_line() const {
        return (*m_currentLine);
    }

    size_t ScriptTraverser::current_token_pos() const {
        return m_tokenPos;
    }

    AdvanceState ScriptTraverser::state() const {
        return m_state;
    }

    AdvanceState ScriptTraverser::next() {
        if (m_state == AdvanceState::LineEnd) {
            m_linesIt++;

            if (m_linesIt == m_targetScript.lines().end()) {
                m_state = AdvanceState::ScriptEnd;
                return m_state;
            }

            m_currentLine = &(*m_linesIt);

            if (m_currentLine->tokens().empty()) {
                throw(ParsingException(std::make_unique<ExceptionStack>("re::ScriptTraverser::next()"), (*m_currentLine), 0, "Tried to traverse an empty line"));
            }

            m_tokensIt = m_currentLine->tokens().begin();
            m_currentToken = &m_currentLine->tokens().front();
            m_tokenPos = 0;

            if (m_tokenPos == (m_currentLine->tokens().size() - 1)) {
                 m_state = (m_linesIt == m_targetScript.lines().end()) ? AdvanceState::ScriptEnd : AdvanceState::LineEnd;
            }
            else {
                m_state = AdvanceState::Normal;
            }
        }
        else if (m_state == AdvanceState::Normal) {
            m_tokensIt++;
            m_tokenPos++;

            m_currentToken = &(*m_tokensIt);
            
            if (m_tokenPos == (m_currentLine->tokens().size() - 1)) {
                 m_state = (m_linesIt == m_targetScript.lines().end()) ? AdvanceState::ScriptEnd : AdvanceState::LineEnd;
            }
        }

        return m_state;
    }
    
    void ScriptTraverser::expect_keyword(RemiKeyword _keyword) const {
        if (!(current_token() == _keyword)) {
            throw_standard_parse_error("ScriptTraverser::expect_keyword()", "Expected \"" + std::string(remi_keyword_str(_keyword)) + "\" keyword");
        }
    }

    void ScriptTraverser::expect_operator(RemiOperator _operator) const {
        if (!(current_token() == _operator)) {
            throw_standard_parse_error("ScriptTraverser::expect_keyword()", "Expected the operator " + std::string(remi_operator_str(_operator)));
        }
    }

    RemiKeyword ScriptTraverser::expect_one_of_keywords(std::vector<RemiKeyword> _keywords) const {
        std::ostringstream ss;

        if (_keywords.empty()) throw_standard_parse_error("re::ScriptTraverser::expect_one_of_keywords()", "No keywords were given (empty vector)");

        for(RemiKeyword k : _keywords) {
            if (current_token() == k) return k;
        }

        ss << "Expected one of the following keywords: ";
        for(RemiKeyword k : _keywords) {
            ss << remi_keyword_str(k) << ", ";
        }

        throw_standard_parse_error("re::ScriptTraverser::expect_one_of_keywords()", ss.str().substr(0, ss.str().size() - 2));
        return RemiKeyword::Void;
    }

    void ScriptTraverser::expect_symbol(char _symbol) const {
        const Token& token = current_token();

        if (token.str().front() == _symbol) {
            return;
        }
        else {
            std::ostringstream ss;

            ss << "Expected the symbol \'" << _symbol << "\'";
            throw_standard_parse_error("re::ScriptTraverser::expect_symbol()", ss.str());
        }
    }

    void ScriptTraverser::expect_symbols(std::string_view _symbols) {
        for(char c : _symbols) {
            expect_symbol(c);
            next();
        }
    }

    std::string_view ScriptTraverser::expect_identifier() const {
        std::string_view str = current_token().str();

        if (!remi_is_identifier(str)) {
            throw_standard_parse_error("re::ScriptTraverser::expect_identifier()", "Expected an identifier");
        }

        return str;
    }

    int ScriptTraverser::expect_number() const {
        int ret = 0;

        if ((current_token().type() != TokenType::Number) || (current_token().type() != TokenType::Digit)) {
            throw_standard_parse_error("re::ScriptTraverser::expect_identifier()", "Expected a number");
        }

        std::istringstream ss(std::string(current_token().str()));
        ss >> ret;

        return ret;
    }

    void ScriptTraverser::expect_string() const {
        TokenType tokenType = current_token().type();
        if ((tokenType != TokenType::String)  &&
            (tokenType != TokenType::Keyword) &&
            (tokenType != TokenType::Letter)
           ) {
            throw_standard_parse_error("re::ScriptTraverser::expect_string()", "Expected a string");
        }
    }

    void ScriptTraverser::expect_string(std::string_view _str) const {
        if (current_token().str() != _str) {
            throw_standard_parse_error("re::ScriptTraverser::expect_string()", "Expected \"" + std::string(_str) + "\"");
        }
    }

    void ScriptTraverser::throw_standard_parse_error(std::string_view _fname, std::string_view _what) const {
        throw(ParsingException(std::make_unique<ExceptionStack>(_fname),
            current_line(),
            current_token_pos(), _what));
    }

}