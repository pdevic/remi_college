#include <Remi/FunctionPlan.hpp>

#include <Remi/Scope.hpp>
#include <Remi/Action.hpp>

namespace re {

    FunctionPlan::FunctionPlan(Scope* _scope, FunctionTypePlan&& _typePlan)
    : m_scope(_scope), m_typePlan(std::move(_typePlan)), m_name("") {}

    /*FunctionPlan::FunctionPlan(FunctionPlan&& _source)
    : m_scope(std::make_unique<Scope>(_source.scope()) {
        m_typePlan = std::move(_source.type_plan());
        m_actions = std::move(_source.actions());
    }*/

    FunctionPlan::~FunctionPlan() {
        for(auto p : m_actions) {
            delete p;
            p = nullptr;
        }
    }

    Scope& FunctionPlan::scope() noexcept {
        return (*m_scope);
    }

    const Scope& FunctionPlan::scope() const noexcept {
        return (*m_scope);
    }

    const FunctionTypePlan& FunctionPlan::type_plan() const noexcept {
        return m_typePlan;
    }

    const std::vector<Action*>& FunctionPlan::actions() const noexcept {
        return m_actions;
    }

    FunctionTypePlan& FunctionPlan::type_plan() {
        return m_typePlan;
    }

    std::vector<Action*>& FunctionPlan::actions() {
        return m_actions;
    }

    std::string_view FunctionPlan::name() const noexcept {
        return m_name;
    }

    void FunctionPlan::add_action(Action* _action) {
        m_actions.push_back(_action);
    }

    void FunctionPlan::set_name(std::string_view _name) {
        m_name = std::string(_name);
    }

}