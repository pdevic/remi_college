#include <Remi/Tokenizer.hpp>

#include <Remi/Logger.hpp>
#include <Remi/Error.hpp>

#include <sstream>

namespace re {

    Tokenizer::Tokenizer(std::vector<Line>& _lines) noexcept
        : m_lines(_lines) {}

    void Tokenizer::tokenize(bool _printTokens) {
        LexingState state{LexingState::Normal};

        for(Line& line : m_lines) {
            state = tokenize_line(line, state);
        }

        if (_printTokens) {
            std::ostringstream ss;

            for(const Line& line : m_lines) {
                ss << "Line " << line.relative_number() << '(' << line.number() << "):\n";

                for(const Token& token : line.tokens()) {
                    ss << token.str() << "\t\t| " << remi_token_type_str(token.type()) << '\n';
                }

                ss << '\n';
            }

            Logger::print(ss.str());
        }
    }

    LexingState Tokenizer::tokenize_line(Line& _line, LexingState _state) {
        LexingState state{_state};
        const std::string_view& source{_line.source()};

        char last{' '};

        size_t start{0};
        size_t count{0};

        for(size_t i{0}; i < _line.source().size(); i++) {
            char c = _line.source()[i];

            switch(state) {
                case LexingState::Normal:
                    if ((last != '\\') && ((c == '"') || (c == '\''))) {
                       if (((count - start) > 0) && (source.substr(start, count - start) != " ")) {
                            _line << Token(start, count - start, source);
                        } 

                        _line << Token(count, 1, source);
                        start = count + 1;

                        state = LexingState::String; 
                        break;
                    }

                    if (is_operator_symbol(c)) {
                        if ((last == ' ') && (_line.tokens().size() > 0)) start++;
                        else if (!is_operator_symbol(last) && (count - start > 0)) {
                            _line << Token(start, count - start, source);
                            start = count;
                        }

                        break;
                    }

                    if (is_divider(c) || is_divider(last)) {
                        size_t s{count - start};

                        if (source.substr(start, s) == " ") {
                            start++;
                            break;
                        }

                        if (s > 0) {
                            _line << Token(start, s, source);
                            start = count;
                        }
                    }

                    break;

                case LexingState::String:
                    if ((last != '\\') && ((c == '"') || (c == '\''))) {
                       if ((count - start) > 0) {
                            _line << Token(start, count - start, source);
                        } 

                        _line << Token(count, 1, source);
                        start = count + 1;

                        state = LexingState::Normal; 
                    }

                    break;

                default:
                    break;
            }

            last = c;
            count++;
        }

        if ((count - start) > 0) {
            _line << Token(start, count - start, source);
            start = count + 1;
        }

        return state;
    }

    inline bool Tokenizer::is_divider(char c) {
        return ((c == ' ') || (RemiSymbols.find(c) != std::string::npos));
    }

    inline bool Tokenizer::is_operator_symbol(char c) {
        return (RemiOperatorSymbols.find(c) != std::string::npos);
    }
}