#include <Remi/FunctionTypePlan.hpp>

#include <Remi/Default.hpp>
#include <Remi/Error.hpp>

namespace re {

    FunctionTypePlan::FunctionTypePlan()
    : m_returnType() {}

    const std::vector<FunctionArgument>& FunctionTypePlan::arguments() const noexcept {
        return m_arguments;
    }

    const VariablePlan& FunctionTypePlan::return_type() const noexcept {
        return m_returnType;
    }

    void FunctionTypePlan::add_argument(FunctionArgument _arg) {
        m_arguments.push_back(std::move(_arg));
    }

    void FunctionTypePlan::set_return_type(VariablePlan _arg) {
        if (m_returnType.type_id() == REMI_UNINITIALIZED_TYPE) {
            m_returnType = _arg;
        }
        else {
            throw(FunctionArgumentException(std::make_unique<ExceptionStack>("re::FunctionTypePlan::set_return_type()"), "Tried to set the return type for a re::FunctionTypePlan but it was already initialized"));
        }
    }

    const FunctionArgument& FunctionTypePlan::operator[](size_t _pos) const {
        return m_arguments[_pos];
    }

    VariablePlan& FunctionTypePlan::return_type() noexcept {
        return m_returnType;
    }

    FunctionTypePlan& operator<<(FunctionTypePlan& _ftplan, FunctionArgument _arg) {
        _ftplan.add_argument(_arg);
        return _ftplan;
    }

}