#include <Remi/Script.hpp>

#include <Remi/Default.hpp>

#include <Remi/Error.hpp>
#include <Remi/Logger.hpp>

#include <filesystem>

namespace re {

    Script::Script(Script* _parent, std::string_view _filename)
        : m_status(RemiScriptStatus::Uninitialized), m_initFunction(nullptr) {
        init(_parent, _filename);
    }

    void Script::init(Script* _parent, std::string_view _filename) {
        if (m_status != RemiScriptStatus::Uninitialized) {
            throw(ScriptException(std::make_unique<ExceptionStack>("re::Script::init()"), (*this), "re::Script::init() called on an already initialized re::Script"));
        }

        if (this == &(*_parent)) {
            throw(ScriptException(std::make_unique<ExceptionStack>("re::Script::init()"), (*this), "Script set to parent itself"));
        }

        m_parent = _parent;
        m_filename = _filename;
        m_status = RemiScriptStatus::Initialized;
    }

    Script* Script::parent() const noexcept {
        return m_parent;
    }

    std::string_view Script::filename() const noexcept {
        return std::string_view(m_filename);
    }

    const std::unique_ptr<CodeSource>& Script::source() const noexcept {
        return m_source;
    }

    const std::vector<Line>& Script::lines() const {
        return m_source->lines();
    }

    void Script::read() {
        expect_status(RemiScriptStatus::Initialized);
        m_source = std::make_unique<CodeSource>();

        try {
            if (!std::filesystem::exists(m_filename))
                throw(ScriptException(std::make_unique<ExceptionStack>("re::Script::read()"), *this, "Tried to read a file that doesn't exist or couldn't be located (filename = " + m_filename + ")"));

            m_source->read(m_filename);
        }
        catch (CodeSourceException& _e) {
            Logger l(LogMode::Console);
            LogStream ls(l);

        #if REMI_PRINT_ERROR_CLASS_INFO
            ls << "[->] [re::Script @" << this << ", re::CodeSource @" << &(*m_source) << ", re::Script::read()]:\n";
        #endif
            ls << "[Error]: Source file manipulation error thrown for script " << m_filename;
            ls << "\n -> " << _e.what() << '\n';

            _e.exception_stack().add_name(_e, "re::Script::read()");
            throw(ScriptException(std::move(_e.exception_stack_ptr()), (*this), "Failed to open or find the source file"));
        }
        catch (LexingException& _e) {
            Logger l(LogMode::Console);
            LogStream ls(l);

        #if REMI_PRINT_ERROR_CLASS_INFO
            ls << "[->] [re::Script @" << this << ", re::CodeSource @" << &(*m_source) << ", re::Script::read()]:\n";
        #endif
            ls << "[Error]: Lexing error thrown in script " << m_filename << " for line " << _e.ref().number();
            ls << "\n -> " << _e.what() << "\n -> Line source: \"" << _e.ref().source() << "\"\n";

            _e.exception_stack().add_name(_e, "re::Script::read()");
            throw(ScriptException(std::move(_e.exception_stack_ptr()), (*this), "Failed to lex"));
       }

       set_status(RemiScriptStatus::Read);
    }

    void Script::tokenize(bool _printTokens) { 
        expect_status(RemiScriptStatus::Read);
        m_source->tokenize(_printTokens); // Doesn't throw re:: exceptions
        set_status(RemiScriptStatus::Tokenized);
    }
 
    void Script::expect_status(RemiScriptStatus _status) const {
        if (m_status != _status) {
            std::ostringstream ss;

            ss << "Innapropriate script state. Status is <" << remi_script_status_name(m_status);
            ss << ">, expected status is <" << remi_script_status_name(_status);

            throw(ScriptException(std::make_unique<ExceptionStack>("re::Script::expect_status()"), (*this), ss.str()));
        }
    }

    void Script::set_status(RemiScriptStatus _status) noexcept {
        m_status = _status;
    }

    const FunctionPlan& Script::init_function() const {
        return (*m_initFunction);
    }

    FunctionPlan& Script::init_function() {
        return (*m_initFunction);
    }

    bool Script::has_init_function() const {
        return (m_initFunction.get() != nullptr);
    }

    void Script::set_init_function(std::unique_ptr<FunctionPlan>&& _fPlan) {
        m_initFunction = std::move(_fPlan);
    }
}