#include<Remi/Parser.hpp>

namespace re {

    void Parser::parse_print_statement(ScriptTraverser& _traverser, FunctionPlan& _function) const {
        bool endl = (_traverser.current_token() == "println");

        if (!endl) {
            _traverser.expect_string("print");
        }

        _traverser.next();
        _traverser.expect_symbol('(');

        do {
            _traverser.next();

            if (_traverser.current_token() == "endl") {
                _function.add_action(new ActionPrintEndl);
                _traverser.next();
            }
            else {
                ActionExpression* expr = parse_expression(_traverser, _function.scope(), &generate_anonymous_variable(VariablePlan::any_type(_function.scope()), _function.scope()));
                _function.add_action(expr);
                generate_print_for_expression(_traverser, *expr, _function);
            }
        } while(_traverser.current_token() == ',');

        _traverser.expect_symbol(')');
        _traverser.next();

        if (endl) _function.add_action(new ActionPrintEndl);
        _function.add_action(new ActionPrintFlush);
    }

    void Parser::generate_print_for_expression(ScriptTraverser& _traverser, ActionExpression& _root, FunctionPlan& _targetFunction) const {
        const VariablePlan& var = _root.return_target();

        switch(var.type_id()) {
        case REMI_BOOL_TYPE:
            _targetFunction.add_action(new ActionPrintVariable<bool>(generate_variable_reference(var, _targetFunction.scope())));
            break;
        case REMI_INT_TYPE:
            _targetFunction.add_action(new ActionPrintVariable<int>(generate_variable_reference(var, _targetFunction.scope())));
            break;
        case REMI_FLOAT_TYPE:
            _targetFunction.add_action(new ActionPrintVariable<float>(generate_variable_reference(var, _targetFunction.scope())));
            break;
        case REMI_CHAR_TYPE:
            if (var.value_type() == ValueType::Array) {
                _targetFunction.add_action(new ActionPrintVariable<std::string>(generate_variable_reference(var, _targetFunction.scope())));
            }
            else {
                _targetFunction.add_action(new ActionPrintVariable<char>(generate_variable_reference(var, _targetFunction.scope())));
            }
            break;
        default:
            std::ostringstream ss;
            ss << "Type isn't supported for printing <Type ID = " << var.type_id() << " | name = " << m_targetContext.find_type_name(var.type_id()) << ">";
            _traverser.throw_standard_parse_error("re::Parser::generate_print_for_expression()", ss.str());
            break;
        }
    }

}