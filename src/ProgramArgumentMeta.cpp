#include <Remi/ProgramArgumentMeta.hpp>

#include <Remi/Error.hpp>

namespace re {
    
    ProgramArgumentMeta::ProgramArgumentMeta(std::string_view _flag, std::string_view _name, std::string_view _description, std::vector<std::string>&& _arguments)
        : m_flag(_flag), m_name(_name), m_description(_description), m_arguments(std::move(_arguments)) {}

    std::string_view ProgramArgumentMeta::flag() const noexcept {
        return m_flag;
    }

    std::string_view ProgramArgumentMeta::name() const noexcept {
        return m_name;
    }

    std::string_view ProgramArgumentMeta::description() const noexcept {
        return m_description;
    }

    const std::vector<std::string>& ProgramArgumentMeta::arguments() const noexcept {
        return m_arguments;
    }

}