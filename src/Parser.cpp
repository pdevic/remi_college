#include <Remi/Parser.hpp>

#include <Remi/Error.hpp>
#include <Remi/Logger.hpp>

#include <Remi/Action.hpp>

#include <Remi/FunctionInstance.hpp>

#include <type_traits>
#include <sstream>
#include <filesystem>

#include <iostream>

namespace re {

    Parser::Parser(Remi& _remi, Context& _target)
    : m_targetContext(_target), m_remi(_remi) {}

    Parser::ParseSnapshot::ParseSnapshot(const ScriptTraverser& _traverser)
    : line(_traverser.current_line()), tokenPos(_traverser.current_token_pos()) {}



    Context& Parser::target_context() {
        return m_targetContext;
    }

    void Parser::ParseSnapshot::throw_standard_parse_error(std::string_view _fname, std::string_view _what) const {
        throw(ParsingException(std::make_unique<ExceptionStack>(_fname),
            line,
            tokenPos, _what));
    }

    const Context& Parser::target_context() const {
        return m_targetContext;
    }

    void Parser::parse_script(Script& _script, FunctionPlan* _previousInitFunction) {
        m_traverserStack.push(ScriptTraverser(_script));
        ScriptTraverser& traverser = m_traverserStack.top();

        Scope* targetScope = m_targetContext.root();
        FunctionPlan* initFunction = nullptr;

        if (_previousInitFunction == nullptr) {
            _script.set_init_function(std::make_unique<FunctionPlan>(targetScope, FunctionTypePlan()));

            initFunction = &_script.init_function();
            m_targetContext.register_init_function(*initFunction);

            initFunction->set_name("<" + std::string(_script.filename()) + ">::init_function");
        }
        else {
            if (targetScope != &_previousInitFunction->scope()) {
               _script.set_init_function(std::make_unique<FunctionPlan>(targetScope, FunctionTypePlan()));

               initFunction = &_script.init_function();
               m_targetContext.register_init_function(*initFunction);
               initFunction->set_name("<" + std::string(_script.filename()) + ">::init_function");
            }
            else {
                initFunction = &_script.parent()->init_function();
            }
        }

        while(traverser.state() != AdvanceState::ScriptEnd) {
            const Token& token = traverser.current_token();

            try {
                if (token == RemiKeyword::Load) {
                    parse_load_statement(traverser, _script);
                }
                else if (token == RemiKeyword::Def) {
                    traverser.next();
                    parse_function_declaration(traverser, *targetScope);
                }
                else if (m_targetContext.type_exists(token.str())) {
                    //traverser.next();
                    parse_variable_declaration(traverser, *initFunction);
                }
                else {
                    traverser.throw_standard_parse_error("re::Parser::parse_script()", "Encountered unexpected tokens in program scope");
                }
            }
            catch(ParsingException& _e) {
                report_parsing_exception("re::Parser::parse_script()", _script, _e);
            }
            catch(FunctionArgumentException& _e) {
                ParsingException p(std::move(_e.exception_stack_ptr()), traverser.current_line(), traverser.current_token_pos(), _e.what());
                report_parsing_exception("re::Parser::parse_script()", _script, p);
            }
            catch(ParseLookupException& _e) {
                std::ostringstream ss;
                ss << _e.what() << "\n -> Requested name: " + std::string(_e.target_name());

                ParsingException p(std::move(_e.exception_stack_ptr()), traverser.current_line(), traverser.current_token_pos(), ss.str());
                report_parsing_exception("re::Parser::parse_script()", _script, p);
            }
        }

        _script.set_status(RemiScriptStatus::Parsed);
    }

    void Parser::parse_load_statement(ScriptTraverser& _traverser, Script& _scriptParent) {
        std::string scriptName;

        _traverser.expect_keyword(RemiKeyword::Load);
        _traverser.next();
        _traverser.expect_symbol('"');
        _traverser.next();

        _traverser.expect_string();
        scriptName = _traverser.current_token().str();

        if (m_targetContext.script_exists(scriptName)) {
            _traverser.throw_standard_parse_error("Parser::parse_load_statement()", "Tried to load script \'" + scriptName + "\' but it was already loaded before");
        }

        if (!std::filesystem::exists(scriptName)) {
            _traverser.throw_standard_parse_error("Parser::parse_load_statement()", "File \'" + scriptName + "\' doesn't exist");
        }

        _traverser.next();
        _traverser.expect_symbol('"');
        _traverser.next();

        parse_script(m_targetContext.create_script(&_scriptParent, scriptName), &_scriptParent.init_function());
    }

    VariablePlan Parser::parse_type(ScriptTraverser& _traverser, Scope& _scope) const {
        Constness refConstness = Constness::Mutable;
        ReferenceType refType = ReferenceType::Value;

        Constness vConstness = Constness::Mutable;
        ValueType vType = ValueType::Value;
        size_t typeID = REMI_UNINITIALIZED_TYPE;

        if (_traverser.current_token() == RemiKeyword::Void) {
            _traverser.next();
            return VariablePlan::void_variable(_scope);
        }

        if (_traverser.current_token() == RemiKeyword::Const) {
            vConstness = Constness::Const;
            _traverser.next();
        }

        typeID = m_targetContext.find_type_id(_traverser.current_token().str());
        _traverser.next();

        if (_traverser.current_token() == '[') {
            _traverser.next();
            _traverser.expect_symbol(']');
            _traverser.next();
            vType = ValueType::Array;
        }

        if (typeID == REMI_UNINITIALIZED_TYPE) {
            _traverser.throw_standard_parse_error("re::Parser::parse_type()", "Failed to deduce type");
        }

        if ((typeID == REMI_VOID_TYPE) && (vType == ValueType::Array))
            _traverser.throw_standard_parse_error("re::Parser::parse_type()", "Cannot initialize an array of void type");

        return VariablePlan(_scope, refConstness, refType, vConstness, vType, typeID);
    }

    void Parser::parse_function_declaration(ScriptTraverser& _traverser, Scope& _targetScope) {
        std::string_view fName(_traverser.current_token().str());
        FunctionTypePlan tPlan;
        size_t offset = 0;

        Scope* newFunctionScope = m_targetContext.scope_space().register_scope(&_targetScope, ScopeType::FunctionBlock);

        if (_targetScope.function_exists(fName))
            _traverser.throw_standard_parse_error("re::Parser::parse_function_declaration()", "A function with this name was already defined in this scope");

        _traverser.next();
        _traverser.expect_symbol('(');

        do {
            _traverser.next();

            if (_traverser.current_token().str() == ")") break;

            VariablePlan vPlan = parse_type(_traverser, *newFunctionScope);

            if (vPlan.type_id() == REMI_VOID_TYPE) _traverser.throw_standard_parse_error("re::Parser::parse_function_declaration()", "void type used as function argument type");

            std::string_view name = _traverser.expect_identifier();
            _traverser.next();

            offset = m_targetContext.get_scope_byte_size(*newFunctionScope);
            VariablePlan& newVar = newFunctionScope->register_variable(name, VariablePlan(vPlan));
            newVar.set_byte_offset(offset + newFunctionScope->fake_offset());

            FunctionArgument ta(name, VariablePlan(newVar));
            tPlan.add_argument(ta);
        } while(_traverser.current_token().str() == ",");

        if ((fName == "main") && !tPlan.arguments().empty())
            throw(FunctionArgumentException(std::make_unique<ExceptionStack>("re::Remi::parse_function_declaration()"), "main function should not take arguments"));

        _traverser.expect_symbol(')');
        _traverser.next();
        _traverser.expect_operator(RemiOperator::Imply);
        _traverser.next();

        tPlan.set_return_type(parse_type(_traverser, *newFunctionScope));

        if (tPlan.return_type().type_id() != REMI_VOID_TYPE) {
            offset = m_targetContext.get_scope_byte_size(*newFunctionScope);
            VariablePlan& retVar = newFunctionScope->register_anonymous_variable(VariablePlan(tPlan.return_type()));
            retVar.set_byte_offset(offset + newFunctionScope->fake_offset());
            tPlan.return_type().set_byte_offset(offset);
        }

        FunctionPlan& newFunction = m_targetContext.root()->register_function(fName, FunctionPlan(newFunctionScope, std::move(tPlan)));
        newFunction.set_name(fName);

        _traverser.expect_symbol('{');
        _traverser.next();

        m_functionStack.push_back(&newFunction);
        parse_function_body(_traverser, newFunction);
        m_functionStack.pop_back();

        _traverser.expect_symbol('}');
        _traverser.next();
    }

    VariablePlan& Parser::generate_anonymous_variable(const VariablePlan& _vPlan, Scope& _scope) const {
        VariablePlan* ret = nullptr;
        size_t offset = m_targetContext.get_scope_byte_size(_scope);

        ret = &_scope.register_anonymous_variable(VariablePlan(_vPlan));
        ret->set_byte_offset(offset + _scope.fake_offset());

        return *ret;
    }

    void Parser::parse_statement(ScriptTraverser& _traverser, FunctionPlan& _function) const {
        const Token& token = _traverser.current_token();

        if (m_targetContext.type_exists(token.str()) || (token == RemiKeyword::Const)) {
            parse_variable_declaration(_traverser, _function);
            return;
        }
        else if ((token == "print") || (token == "println")) {
            parse_print_statement(_traverser, _function);
            return;
        }
        else if (token == RemiKeyword::If) {
            parse_if_statement(_traverser, _function);
            return;
        }
        else if ((token == RemiKeyword::While) || (token == RemiKeyword::For) || (token == RemiKeyword::Do)) {
            parse_loop_statement(_traverser, _function);
            return;
        }
        else if (token == RemiKeyword::Return) {
            _traverser.next();

            if (m_functionStack.empty()) 
                _traverser.throw_standard_parse_error("re::Parser::parse_statement()", "Keyword return used outside of a function call");

            if (_function.type_plan().return_type().type_id() != REMI_VOID_TYPE) {
                ActionExpression* expr = parse_expression(_traverser, _function.scope(), &_function.type_plan().return_type());
                _function.add_action(expr);

                VariablePlan& exprVar = expr->return_target();
                VariablePlan& fRet = _function.type_plan().return_type();

                if (exprVar.byte_offset() != fRet.byte_offset()) {
                    _function.add_action(new ActionCopyValue(generate_variable_reference(exprVar, _function.scope()), generate_variable_reference(fRet, _function.scope()), m_targetContext.get_type_byte_size(exprVar.type_id())));
                }
            }

            _function.add_action(new ActionAbortFunctionCall);
            return;
        }
        else if (remi_is_identifier(token.str())) {
            std::string_view name = token.str();

            if (_function.scope().function_exists(name, SearchType::Deep)) {
                _traverser.next();
                _function.add_action(parse_function_call(_traverser, name, _function.scope(), nullptr));
                return;
            }
        }

        _function.add_action(parse_expression(_traverser, _function.scope(), nullptr));
    }

    void Parser::parse_function_body(ScriptTraverser& _traverser, FunctionPlan& _function) const {
        while(_traverser.current_token().str() != "}") {
            parse_statement(_traverser, _function);
        }
    }

    ActionFunctionCall* Parser::parse_function_call(ScriptTraverser& _traverser, std::string_view _fName, Scope& _callerScope, VariablePlan* _directReturn) const {
        const FunctionPlan& function = _callerScope.find_function(_fName, SearchType::Deep);
        std::unique_ptr<ActionExecuteActions> argumentExpressions(new ActionExecuteActions);

        _traverser.expect_symbol('(');

        size_t i = 0;
        size_t argCount = function.type_plan().arguments().size();
        FunctionArgument* a = nullptr;

        if ((_traverser.current_token() == ')') && (argCount > 0)) {
            std::ostringstream ss;
            ss << "No arguments given in call to function " << std::string(_fName) << " (" << argCount << " expected)\n";
            ss << "\nFunction declaration is:\n      " << m_targetContext.full_function_declaration(_fName, function);
            _traverser.throw_standard_parse_error("re::Parser::parse_function_call()", ss.str());
        }

        if (argCount == 0) {
            _traverser.next();
        }
        else {
            for(; i < argCount; i++) {
                try {
                    _traverser.next();
                    a = &(FunctionArgument&)(function.type_plan().arguments().at(i));

                    ActionExpression* expr = parse_expression(_traverser, _callerScope, &generate_anonymous_variable(VariablePlan(a->variable_plan()), _callerScope));
                    argumentExpressions->add_action(expr);

                    if ((i != (argCount - 1)) && !(_traverser.current_token() == ',')) {
                        std::ostringstream ss;
                        ss << "Insufficient amount of arguments in function call: " << i + 1 << " given, " << argCount << " expected (expected symbol \',\')\n";
                        ss << "\n Function declaration is:\n      " << m_targetContext.full_function_declaration(_fName, function);
                        _traverser.throw_standard_parse_error("re::Parser::parse_function_call()", ss.str());
                    }

                    argumentExpressions->add_action(new ActionCopyValue(
                        VariableReference(expr->return_target(), VariableReferenceModel::OnCaller),
                        VariableReference(a->variable_plan(), VariableReferenceModel::OnCalled),
                        m_targetContext.get_type_byte_size(expr->return_target().type_id())
                    ));
                }
                catch(ParsingException& _e) {
                    std::ostringstream ss;
                    ss << "Error thrown for argument #" << i + 1 << " of type " << m_targetContext.full_type_name(a->variable_plan()) << " of function " << _fName << ':';
                    ss << "\n -> " << _e.what();

                    ParsingException p(std::move(_e.exception_stack_ptr()), _traverser.current_line(), _traverser.current_token_pos(), ss.str());
                    _traverser.throw_standard_parse_error("re::Parser::parse_function_call()", ss.str());
                }
            }
        }

        std::unique_ptr<ActionFunctionCall> ret = std::make_unique<ActionFunctionCall>(&function, m_targetContext.get_scope_byte_size(function.scope()), argumentExpressions.release());

        if (_directReturn != nullptr) {
            VariablePlan rType = VariablePlan(function.type_plan().return_type());
            std::vector<Action*> actions;

            if (rType.type_id() == REMI_VOID_TYPE) {
                std::ostringstream ss;
                ss << "Function without return value used in expression that expects a return of type " << m_targetContext.full_type_name(*_directReturn) << '\n';
                ss << "\n Function declaration is:\n      " << m_targetContext.full_function_declaration(_fName, function);
                _traverser.throw_standard_parse_error("re::Parser::parse_function_call()", ss.str());
            }

            if (_directReturn->type_id() == REMI_ANY_TYPE) {
                *_directReturn = VariablePlan(rType);
                _directReturn->set_scope_reference(&_callerScope);
                _directReturn->set_byte_offset(m_targetContext.get_scope_byte_size(_callerScope));
            }

            actions.push_back(new ActionCopyValue(VariableReference(rType, VariableReferenceModel::OnCalled), VariableReference(*_directReturn, VariableReferenceModel::OnCaller), m_targetContext.get_type_byte_size(rType.type_id())));
            ret->set_return_target(new ActionExecuteActions(std::move(actions)));
        }

        _traverser.expect_symbol(')');
        _traverser.next();

        return ret.release();
    }

    void Parser::parse_variable_declaration(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const {
        VariablePlan vPlan(parse_type(_traverser, _targetFunction.scope()));
        std::string_view name;

        do {
            ActionExpression* expr = nullptr;
            bool hasExpr = false;

            name = _traverser.expect_identifier();

            size_t offset = m_targetContext.get_scope_byte_size(_targetFunction.scope());
            VariablePlan& current = _targetFunction.scope().register_variable(name, VariablePlan(vPlan));
            current.set_byte_offset(offset + _targetFunction.scope().fake_offset());

            _traverser.next();

            if (_traverser.current_token() == '(') {
                hasExpr = true;
                _traverser.next();

                expr = parse_expression(_traverser, _targetFunction.scope(), &current);
                _targetFunction.add_action(expr);

                _traverser.expect_symbol(')');
                _traverser.next();
            }
            else if (_traverser.current_token() == RemiOperator::Assign) {
                hasExpr = true;
                _traverser.next();

                expr = parse_expression(_traverser, _targetFunction.scope(), &current);
                _targetFunction.add_action(expr);
            }

            if (hasExpr) {
                if (expr->return_target().byte_offset() != current.byte_offset()) 
                    _targetFunction.add_action(new ActionCopyValue(generate_variable_reference(expr->return_target(), _targetFunction.scope()), generate_variable_reference(current, _targetFunction.scope()), m_targetContext.get_type_byte_size(expr->return_target().type_id())));
            }

            if (_traverser.current_token().str() != ",") break;
            _traverser.next();
        } while(true);
    }

    void Parser::parse_if_statement(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const {
        std::unique_ptr<ActionIfStatement> ifAction = std::make_unique<ActionIfStatement>();

        do {
            ActionExpression* conditionExpression = nullptr;
            ActionExecuteActions* actions = nullptr;
            bool hasIf = false;

            Scope& inlineScope = m_targetContext.scope_space().generate_inline_anonymous_scope(&_targetFunction.scope());
            inlineScope.set_fake_offset(m_targetContext.get_scope_byte_size(*inlineScope.parent()));

            if (_traverser.current_token() == RemiKeyword::If) {
                hasIf = true;
                _traverser.next();

                _traverser.expect_symbol('(');
                _traverser.next();

                conditionExpression = parse_expression(_traverser, inlineScope, &generate_anonymous_variable(VariablePlan::normal_variable(inlineScope, Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), inlineScope));

                _traverser.expect_symbol(')');
                _traverser.next();
            }

            _traverser.expect_symbol('{');
            _traverser.next();

            FunctionPlan& newFunction = _targetFunction.scope().register_anonymous_function(FunctionPlan(&_targetFunction.scope(), FunctionTypePlan()));
            newFunction.type_plan().set_return_type(VariablePlan(_targetFunction.type_plan().return_type()));

            parse_function_body(_traverser, newFunction);
            _targetFunction.scope().increase_fake_offset(m_targetContext.get_scope_byte_size(inlineScope));

            actions = new ActionExecuteActions(std::move(newFunction.actions()));

            _traverser.expect_symbol('}');
            _traverser.next();

            if (hasIf) ifAction->add_conditional_action(generate_variable_reference(conditionExpression->return_target(), _targetFunction.scope()), conditionExpression, actions);
            else {
                ifAction->set_final_else(actions);
                break;
            }

            if (_traverser.current_token() == RemiKeyword::Else) _traverser.next();
            else break;
        } while(true);

        _targetFunction.add_action(ifAction.release());
    }

    void Parser::parse_loop_statement(ScriptTraverser& _traverser, FunctionPlan& _targetFunction) const {
        ActionExecuteActions* actions = nullptr;
        ActionExpression* conditionExpression = nullptr;
        FunctionPlan* stepFunction = nullptr;

        std::unique_ptr<ActionExecuteActions> forInit(nullptr);

        RemiKeyword keyword = _traverser.expect_one_of_keywords({RemiKeyword::While, RemiKeyword::For, RemiKeyword::Do});
        _traverser.next();

        Scope& inlineScope = m_targetContext.scope_space().generate_inline_anonymous_scope(&_targetFunction.scope());
        inlineScope.set_fake_offset(m_targetContext.get_scope_byte_size(*inlineScope.parent()));

        FunctionPlan& newFunction = _targetFunction.scope().register_anonymous_function(FunctionPlan(&inlineScope, FunctionTypePlan()));
        newFunction.type_plan().set_return_type(VariablePlan(_targetFunction.type_plan().return_type()));

        switch(keyword) {
        case RemiKeyword::While:
            _traverser.expect_symbol('(');
            _traverser.next();

            conditionExpression = parse_expression(_traverser, inlineScope, &generate_anonymous_variable(VariablePlan::normal_variable(inlineScope, Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), inlineScope));

            _traverser.expect_symbol(')');
            _traverser.next();
            break;
        case RemiKeyword::For:
            _traverser.expect_symbol('(');
            _traverser.next();

            if (m_targetContext.type_exists(_traverser.current_token().str())) {
                parse_variable_declaration(_traverser, newFunction);
                forInit = std::make_unique<ActionExecuteActions>(std::move(newFunction.actions()));
            }
            else {
                forInit = std::make_unique<ActionExecuteActions>();
                forInit->add_action(parse_expression(_traverser, _targetFunction.scope(), nullptr));
            }

            _traverser.expect_symbol(';');
            _traverser.next();

            conditionExpression = parse_expression(_traverser, inlineScope, nullptr);

            _traverser.expect_symbol(';');
            _traverser.next();

            stepFunction = &_targetFunction.scope().register_anonymous_function(FunctionPlan(&inlineScope, FunctionTypePlan()));
            parse_statement(_traverser, *stepFunction);

            _traverser.expect_symbol(')');
            _traverser.next();
            break;
        default: break;
        }

        _traverser.expect_symbol('{');
        _traverser.next();

        parse_function_body(_traverser, newFunction);
        _targetFunction.scope().increase_fake_offset(m_targetContext.get_scope_byte_size(inlineScope));

        actions = new ActionExecuteActions(std::move(newFunction.actions()));

        _traverser.expect_symbol('}');
        _traverser.next();

        switch(keyword) {
        case RemiKeyword::While:
            _targetFunction.add_action(new ActionWhileLoop(generate_variable_reference(conditionExpression->return_target(), _targetFunction.scope()), conditionExpression, actions));
            break;
        case RemiKeyword::For:
            for(Action* ap : stepFunction->actions()) actions->add_action(ap->clone());
            
            _targetFunction.add_action(forInit.release());
            _targetFunction.add_action(new ActionWhileLoop(generate_variable_reference(conditionExpression->return_target(), _targetFunction.scope()), conditionExpression, actions));
            break;
        case RemiKeyword::Do:
            _traverser.expect_keyword(RemiKeyword::While);
            _traverser.next();

            _traverser.expect_symbol('(');
            _traverser.next();

            conditionExpression = parse_expression(_traverser, _targetFunction.scope(), &generate_anonymous_variable(VariablePlan::normal_variable(_targetFunction.scope(), Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), _targetFunction.scope()));

            _traverser.expect_symbol(')');
            _traverser.next();

            _targetFunction.add_action(new ActionDoWhileLoop(generate_variable_reference(conditionExpression->return_target(), _targetFunction.scope()), conditionExpression, actions));
            break;
        default: break;
        }
    }

    VariableReference Parser::generate_variable_reference(const VariablePlan& _source, const Scope& _callerScope) const {
        if (_source.scope() == _callerScope) {
            return VariableReference::dynamic_reference(_source);
        }
        else {
            return VariableReference::static_reference(_source);
        }
    }

    void Parser::report_parsing_exception(std::string_view _functionName, const Script& _script, ParsingException& _e) const {
        LogStream ls(m_targetContext.logger());

        const Line& line = _e.ref();
        const Token& token = line[_e.token_pos()];

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Parser @" << this << "]:\n";
    #endif

        ls << "[Error]: Parsing error on line " << line.number() << " in script \'" << _script.filename() << "\':\n";
        ls << " -> " << _e.what() << "\n\n";

        /*ls << ": " << line.source() << '\n';
        for(size_t i = 0; i < (token.start() + 2); i++) { ls << ' '; }
        ls << '^';
        for(size_t i = 0; i < (token.str().size() - 1); i++) { ls << '~'; }
        ls << '\n';*/
        ls << ": " << generate_annotated_error_str(line.source(), token, 2) << '\n';

        _e.exception_stack().add_name(_e, _functionName);
        throw(ScriptException(std::move(_e.exception_stack_ptr()), _script, "Encountered an error while parsing"));
    }

    std::string Parser::generate_annotated_error_str(std::string_view _str, const Token& _token, size_t _startOffset) {
        std::ostringstream ss;

        ss << _str << '\n';
        for(size_t i = 0; i < (_token.start() + _startOffset); i++) { ss << ' '; }
        ss << '^';
        for(size_t i = 0; i < (_token.str().size() - 1); i++) { ss << '~'; }

        return ss.str();
    }

}