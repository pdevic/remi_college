#include <Remi/Default.hpp>

#include <Remi/Error.hpp>

#include <sstream>
#include <regex>

namespace re {

    std::string RemiVersion::version() {
        return generateVersion();
    }

    size_t RemiVersion::major() noexcept {
        return ms_majorVersion;
    }

    size_t RemiVersion::minor() noexcept {
        return ms_minorVersion;
    }

    size_t RemiVersion::build() noexcept {
        return ms_buildVersion;
    }

    std::string RemiVersion::generateVersion() {
        std::ostringstream ss;

        ss << major() << '.' << minor() << '.' << build();
        return ss.str();
    }

    std::string remi_model_name(RemiModel _model) {
        switch(_model) {
        case RemiModel::Development: return "Development"; break;
        case RemiModel::Debug:       return "Debug";       break;
        case RemiModel::Release:     return "Release";     break;

        default:
            throw(Exception(std::make_unique<ExceptionStack>("re::remi_model_name()"), "Failed to find a name for a platform enum element"));
            break;
        }
    }

    std::string remi_platform_name(RemiPlatform _platform) {
        switch(_platform) {
        case RemiPlatform::Win64:   return "Win64";   break;
        case RemiPlatform::Win32:   return "Win32";   break;
        case RemiPlatform::Linux:   return "Linux";   break;
        case RemiPlatform::Android: return "Android"; break;
        case RemiPlatform::Apple:   return "Apple";   break;

        default:
            throw(Exception(std::make_unique<ExceptionStack>("re::remi_platform_name()"), "Failed to find a name for a platform enum element"));
            break;
        }
    }

    std::string remi_compiler_name(RemiCompiler _compiler) {
        switch(_compiler) {
        case RemiCompiler::Msc:     return "MSC";     break;
        case RemiCompiler::MinGW64: return "MinGW64"; break;
        case RemiCompiler::MinGW32: return "MinGW32"; break;
        case RemiCompiler::Gcc:     return "Gcc";     break;
        case RemiCompiler::Clang:   return "Clang";   break;

        default:
            throw(Exception(std::make_unique<ExceptionStack>("re::remi_compiler_name()"), "Failed to find a name for a compiler enum element"));
            break;
        }
    }

    const std::string& RemiModelName{remi_model_name(RemiCurrentModel)};
    const std::string& RemiPlatformName{remi_platform_name(RemiCurrentPlatform)};
    const std::string& RemiCompilerName{remi_compiler_name(RemiCurrentCompiler)};

    std::string remi_release_str() {
        std::ostringstream ss;

        ss << "Remi " << RemiVersion::version() << ' ' << RemiPlatformName << " [" << RemiModelName << ']';
        return ss.str();
    }

    std::string remi_compiler_version_str() {
        std::stringstream ss;

    #ifdef _MSC_VER
        ss << _MSC_VER;
        #ifdef _MSC_FULL_VER
        ss << " (" << _MSC_FULL_VER << ")";
        #endif
    #elif __GNUC__
        #if defined(__MINW64__) || defined(__MINGW32__)
        ss << "gcc ";
        #endif
        ss << __GNUC__ << '.' << __GNUC_MINOR__;
        #ifdef __GNUC_PATCHLEVEL__
        ss << '.' << __GNUC_PATCHLEVEL__;
        #endif
    #elif __clang__
        ss << __clang_version__;
        #ifdef __apple_build_version__
        ss << " (Apple)";
        #endif
    #endif

        return ss.str();
    }

    const std::unordered_map<std::string, std::string> RemiProgramArgumentNames{
        {RemiProgramArgumentFlags.at("l"), "l"},
        {RemiProgramArgumentFlags.at("h"), "h"},
        {RemiProgramArgumentFlags.at("v"), "v"},
        {RemiProgramArgumentFlags.at("V"), "V"},
        {RemiProgramArgumentFlags.at("Lf"), "Lf"},
        {RemiProgramArgumentFlags.at("L"), "L"},
        {RemiProgramArgumentFlags.at("K"), "K"},
        {RemiProgramArgumentFlags.at("T"), "T"},
        {RemiProgramArgumentFlags.at("op"), "op"},
        {RemiProgramArgumentFlags.at("bts"), "bts"},
        {RemiProgramArgumentFlags.at("ts"), "ts"},
        {RemiProgramArgumentFlags.at("p"), "p"}
    };

    std::string_view remi_program_argument_flag(const std::string& _argumentName) {
        auto it{RemiProgramArgumentNames.find(_argumentName)};

        if (it == RemiProgramArgumentNames.end()) {
            throw(ProgramArgumentLookupException(std::make_unique<ExceptionStack>("re::remi_program_argument_flag()"), _argumentName, "Failed to find a flag associated with the requested argument name (no flag name matches the requested name)"));
        }

        return (*it).second;
    }

    std::string_view remi_program_argument_name(const std::string& _flagName) {
        auto it{RemiProgramArgumentFlags.find(_flagName)};

        if (it == RemiProgramArgumentFlags.end()) {
            throw(ProgramArgumentLookupException(std::make_unique<ExceptionStack>("re::remi_program_argument_name()"), _flagName, "Failed to find the name of the requested flag (no such flag)"));
        }

        return (*it).second;
    }

    std::string remi_script_status_name(RemiScriptStatus _status) {
        switch(_status) {
        case RemiScriptStatus::Uninitialized: return "Uninitialized"; break;
        case RemiScriptStatus::Initialized:   return "Initialized";   break;
        case RemiScriptStatus::Read:          return "Read";          break;
        case RemiScriptStatus::Tokenized:     return "Tokenized";     break;
        case RemiScriptStatus::Parsed:        return "Parsed";        break;

        default:
            throw(ProgramException(std::make_unique<ExceptionStack>("re::remi_script_status_name()"), "Failed to find a name for a re::RemiScriptStatus"));
            break;
        }
    }

    const std::map<std::string, ProgramArgumentMeta> RemiProgramArguments{
        {"d", ProgramArgumentMeta{"d", RemiProgramArgumentFlags.at("d"), "Print a short tutorial on the requested topic. Topics are \"variables\", \"types\", \"functions\", \"conditions\" and \"loops\"", {"option"}}},
        {"l", ProgramArgumentMeta{"l", RemiProgramArgumentFlags.at("l"), "Start by parsing and loading a given script. Executes the script if [-I] flag is not set", {"filename"}}},
        {"h", ProgramArgumentMeta{"h", RemiProgramArgumentFlags.at("h"), "Print the list of all argument flags with their full names and usage before exiting. Makes Remi ignore other flags", {}}},
        {"v", ProgramArgumentMeta{"v", RemiProgramArgumentFlags.at("v"), "Print the version of the language along with some other technical information before exiting. Makes Remi ignore other flags", {}}},
        {"V", ProgramArgumentMeta{"V", RemiProgramArgumentFlags.at("V"), "[0-4] Sets the output verbosity", {"value"}}},
        {"Lf", ProgramArgumentMeta{"Lf", RemiProgramArgumentFlags.at("Lf"), "Set default log file. Use \"auto\" to generate a log file with current date (d-m-y_h-m-s). Use \"default\" to use \'log.txt\'", {"filename"}}},
        {"L", ProgramArgumentMeta{"L", RemiProgramArgumentFlags.at("L"), "[0-2] (0: console only, 1: file only, 2: both console and file) ", {"value"}}},
        {"K", ProgramArgumentMeta{"K", RemiProgramArgumentFlags.at("K"), "Print all keywords", {}}},
        {"T", ProgramArgumentMeta{"T", RemiProgramArgumentFlags.at("T"), "Print tokens from all scripts line-by-line", {}}},
        {"op", ProgramArgumentMeta{"op", RemiProgramArgumentFlags.at("op"), "Print supported operators", {}}},
        {"bts", ProgramArgumentMeta{"bts", RemiProgramArgumentFlags.at("bts"), "Print sizes of built-in types", {}}},
        {"ts", ProgramArgumentMeta{"ts", RemiProgramArgumentFlags.at("ts"), "Print type sizes - requires a script to parse [flag -l] (this flag overrides [-bts])", {}}},
        {"p", ProgramArgumentMeta{"p", RemiProgramArgumentFlags.at("p"), "Print program variables before anything else", {}}}
    };

    std::string_view remi_token_type_str(TokenType _type) {
        switch(_type) {
            case TokenType::None:    return "none";    break;
            case TokenType::Number:  return "number";  break;
            case TokenType::Digit:   return "digit";   break;
            case TokenType::Keyword: return "keyword"; break;
            case TokenType::String:  return "string";  break;
            case TokenType::Letter:  return "letter";  break;
            case TokenType::Symbol:  return "symbol";  break;

            default:
                throw(ProgramException(std::make_unique<ExceptionStack>("re::remi_token_type_str()"), "Failed to find a string for a token type (missing entry in [Default.cpp])"));
                break;
        }
    }

    const std::map<std::string, RemiKeyword> RemiKeywords {
            {"void",       RemiKeyword::Void      },
            {"bool",       RemiKeyword::Bool      },
            {"char",       RemiKeyword::Char      },
            {"int",        RemiKeyword::Int       },
            {"float",      RemiKeyword::Float     },

            {"const",      RemiKeyword::Const     },
            {"def",        RemiKeyword::Def       },

            {"true",       RemiKeyword::True      },
            {"false",      RemiKeyword::False     },

            {"if",         RemiKeyword::If        },
            {"else",       RemiKeyword::Else      },
            {"then",       RemiKeyword::Then      },
            {"return",     RemiKeyword::Return    },

            {"while",      RemiKeyword::While     },
            {"do",         RemiKeyword::Do        },
            {"for",        RemiKeyword::For       },
            {"in",         RemiKeyword::In        },

            {"load",       RemiKeyword::Load      }
    };

    const std::map<std::string, RemiOperator> RemiOperators {
        {"+",  RemiOperator::Add             },
        {"-",  RemiOperator::Subtract        },
        {"*",  RemiOperator::Multiply        },
        {"/",  RemiOperator::Divide          },
  
        {"%",  RemiOperator::Modulo          },
        {"++", RemiOperator::Increment       },
        {"--", RemiOperator::Decrement       },
        {"->", RemiOperator::Imply           },
        {"=>", RemiOperator::Define          },
  
        {"<",  RemiOperator::Less            },
        {"<=", RemiOperator::LessOrEqual     },
        {">",  RemiOperator::Greater         },
        {">=", RemiOperator::GreaterOrEqual  },
  
        {"&&", RemiOperator::And             },
        {"||", RemiOperator::Or              },
  
        {"=",  RemiOperator::Assign          },
        {"+=", RemiOperator::AddAssign      },
        {"-=", RemiOperator::SubtractAssign },
        {"*=", RemiOperator::MultiplyAssign },
        {"/=", RemiOperator::DivideAssign   },

        {"==", RemiOperator::Equal           },
        {"!",  RemiOperator::Not             },
        {"!=", RemiOperator::NotEqual        }
    };

    std::string_view remi_keyword_str(RemiKeyword _keyword) {
        switch(_keyword) {
            case RemiKeyword::Void:      return "void";       break;
            case RemiKeyword::Bool:      return "bool";       break;
            case RemiKeyword::Char:      return "char";       break;
            case RemiKeyword::Int:       return "int";        break;
            case RemiKeyword::Float:     return "float";      break;

            case RemiKeyword::Const:     return "const";      break;
            case RemiKeyword::Def:       return "def";        break;

            case RemiKeyword::True:      return "true";       break;
            case RemiKeyword::False:     return "false";      break;

            case RemiKeyword::If:        return "if";         break;
            case RemiKeyword::Else:      return "else";       break;
            case RemiKeyword::Then:      return "then";       break;
            case RemiKeyword::Return:    return "return";     break;

            case RemiKeyword::While:     return "while";      break;
            case RemiKeyword::Do:        return "do";         break;
            case RemiKeyword::For:       return "for";        break;
            case RemiKeyword::In:        return "in";         break;

            case RemiKeyword::Load:      return "load";       break;

            default:
                throw(ProgramException(std::make_unique<ExceptionStack>("re::remi_keyword_str()"), "Failed to find a string for a keyword (missing entry in [Default.cpp])"));
                break;
        }
    }

    std::string_view remi_operator_str(RemiOperator _operator) {
        switch(_operator) {
            case RemiOperator::Add:            return "+";  break;
            case RemiOperator::Subtract:       return "-";  break;
            case RemiOperator::Multiply:       return "*";  break;
            case RemiOperator::Divide:         return "/";  break;

            case RemiOperator::Modulo:         return "%";  break;
            case RemiOperator::Increment:      return "++"; break;
            case RemiOperator::Decrement:      return "--"; break;
            case RemiOperator::Imply:          return "->"; break;
            case RemiOperator::Define:         return "=>"; break;

            case RemiOperator::Less:           return "<" ; break;
            case RemiOperator::LessOrEqual:    return "<="; break;
            case RemiOperator::Greater:        return ">" ; break;
            case RemiOperator::GreaterOrEqual: return ">="; break;

            case RemiOperator::And:            return "&&"; break;
            case RemiOperator::Or:             return "||"; break;

            case RemiOperator::Assign:         return "=";  break;
            case RemiOperator::AddAssign:      return "+="; break;
            case RemiOperator::SubtractAssign: return "-="; break;
            case RemiOperator::MultiplyAssign: return "*="; break;
            case RemiOperator::DivideAssign:   return "/="; break;

            case RemiOperator::Equal:          return "=="; break;
            case RemiOperator::Not:            return "!";  break;
            case RemiOperator::NotEqual:       return "!="; break;

            default:
                throw(ProgramException(std::make_unique<ExceptionStack>("re::remi_operator_str()"), "Failed to find a string for an operator (missing entry in [Default.cpp])"));
                break;
        }
    }

    RemiOperator remi_find_operator(std::string_view _str) {
        auto it = RemiOperators.find(std::string(_str));

        if (it == RemiOperators.end()) {
            std::ostringstream ss;
            ss << '\"' << _str << "\" is not a recognized operator";
            throw(ProgramException(std::make_unique<ExceptionStack>("re::remi_find_operator()"), ss.str()));
        }
        return it->second;
    }

    bool remi_is_arithmetic_operator(RemiOperator _operator) {
        switch(_operator) {
            case RemiOperator::Add:
            case RemiOperator::Subtract:
            case RemiOperator::Multiply:
            case RemiOperator::Divide:
                return true;
                break;
            
            default:
                return false;
                break;
        }
    }

    bool remi_is_logic_operator(RemiOperator _operator) {
        switch(_operator) {
            case RemiOperator::Less:
            case RemiOperator::LessOrEqual:
            case RemiOperator::Greater:
            case RemiOperator::GreaterOrEqual:
            case RemiOperator::Equal:
            case RemiOperator::Not:
            case RemiOperator::NotEqual:
                return true;
                break;
            
            default:
                return false;
                break;
        }
    }

    bool remi_is_binary_operator(RemiOperator _operator) {
        switch(_operator) {
        case RemiOperator::Not:
        case RemiOperator::Increment:
        case RemiOperator::Decrement:
            return true;
            break;

        default:
            return false;
            break;
        }
    }

    bool remi_is_assign_variant(RemiOperator _operator) {
        switch(_operator) {
            case RemiOperator::Assign:
            case RemiOperator::AddAssign:
            case RemiOperator::SubtractAssign:
            case RemiOperator::MultiplyAssign:
            case RemiOperator::DivideAssign:
                return true;
                break;
            
            default:
                return false;
                break;
        }
    }

    bool remi_is_identifier(std::string_view _str) {
        static const std::regex identifierRegex("[a-zA-Z_][a-zA-Z_0-9]*");
        auto it = RemiKeywords.find(std::string(_str));

        if (it != RemiKeywords.end()) {
            return false;
        }

        if ((_str == "print") || (_str == "println")) {
            return false;
        }

        return std::regex_match(std::string(_str), identifierRegex);
    }

}