#include <Remi/FunctionInstance.hpp>

#include <Remi/ExecutionBlock.hpp>
#include <Remi/ExecutionContext.hpp>

#include <Remi/Action.hpp>

namespace re {

    FunctionInstance::FunctionInstance(const FunctionPlan& _source)
    : m_block(nullptr), m_source(&_source) {}

    FunctionInstance::FunctionInstance(const FunctionPlan& _source, const ExecutionBlock& _block)
    :  m_block(&_block), m_source(&_source) {}

    /*FunctionInstance::~FunctionInstance() {
        for(Action* p : m_actions) {
            delete p;
            p = nullptr;
        }
    }*/

    void FunctionInstance::execute(ExecutionContext& _context) {
        //std::cout << "Source = " << m_source << std::endl;
        //std::cout << "Actions for " << m_source->name() << ": " << m_source->actions().size() << std::endl;
        for(Action* ap : m_source->actions()) {
            if (ap->execute(_context)) break;
        }
    }

    void FunctionInstance::set_execution_block(const ExecutionBlock& _block) {
        m_block = &_block;
    }

}