#include <Remi/ExecutionContext.hpp>

#include <Remi/Scope.hpp>
#include <Remi/FunctionPlan.hpp>

#include <Remi/FunctionInstance.hpp>

#include <iostream>

namespace re {

    ExecutionContext::ExecutionContext(const Context& _context)
    : m_context(_context), m_functionCallCount(0) {
        for(auto& d : m_context.scope_space().scope_map()) {
            size_t depth = d.first;

            for(auto& i : d.second) {
                size_t id = i.first;

                m_stackCount[depth][id] = 0;
            }
        }

        //m_functionStack.reserve(2048);
        //m_callContexts.reserve(2048);
    }

    void ExecutionContext::execute_init_functions() {
        for(FunctionPlan* fp : m_context.init_functions()) {
            silent_function_execute(*fp);
        }
    }

    void ExecutionContext::silent_function_execute(const FunctionPlan& _calledFunction) {
        init_function_call(_calledFunction);
        execute_current_function();
        m_functionStack.pop_back();
    }

}