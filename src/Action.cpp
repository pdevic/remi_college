#include <Remi/Action.hpp>

#include <Remi/FunctionInstance.hpp>
#include <Remi/ExecutionContext.hpp>

#include <sstream>
#include <iostream>

namespace re {

    Action::Action() {}
    Action::~Action() {}



    VariableReference::VariableReference(const VariableReference& _source)
    : m_depth(_source.m_depth), m_id(_source.m_id), m_byteOffset(_source.m_byteOffset),
      m_ptrFunction(_source.m_ptrFunction) {}

    VariableReference VariableReference::dynamic_reference(const VariablePlan& _source) {
        return VariableReference(_source, VariableReferenceModel::Dynamic);
    }

    VariableReference VariableReference::static_reference(const VariablePlan& _source) {
        return VariableReference(_source, VariableReferenceModel::Static);
    }

    VariableReference VariableReference::on_current_caller(const VariablePlan& _source) {
        return VariableReference(_source, VariableReferenceModel::OnCaller);
    }

    VariableReference::VariableReference(const VariablePlan& _source, VariableReferenceModel _model)
    : m_depth(_source.scope().depth()), m_id(_source.scope().id()), m_byteOffset(_source.byte_offset()) {
        switch(_model) {
        case VariableReferenceModel::Dynamic:
            m_ptrFunction = &VariableReference::get_ptr_dynamic;
            break;
        case VariableReferenceModel::Static:
            m_ptrFunction = &VariableReference::get_ptr_static;
            break;
        case VariableReferenceModel::OnCaller:
            m_ptrFunction = &VariableReference::get_ptr_on_current_caller;
            break;
        case VariableReferenceModel::OnCalled:
            m_ptrFunction = &VariableReference::get_ptr_on_current_called;
            break;
        }
    }

    size_t VariableReference::depth() const noexcept {
        return m_depth;
    }
    size_t VariableReference::id() const noexcept {
        return m_id;
    }

    size_t VariableReference::byte_offset() const noexcept {
        return m_byteOffset;
    }

    std::string VariableReference::coords_str() const {
        std::ostringstream ss;

        ss << m_depth << ':' << m_id << " @" << m_byteOffset;
        return ss.str();
    }



    template <typename T>
    Action* ActionCloneBase<T>::clone() const {
        return new T(reinterpret_cast<const T&>(*this));
    }



    template<class T>
    ActionWriteConst<T>::ActionWriteConst(VariableReference&& _target, T _data)
    : m_target(std::move(_target)) , m_data(_data) {}

    template<class T>
    int ActionWriteConst<T>::execute(ExecutionContext& _context) {
        *(reinterpret_cast<T*>(m_target.get_ptr(_context))) = m_data;
        return 0;
    }

    template<class T>
    const T& ActionWriteConst<T>::data() const {
        return m_data;
    }

    template class ActionWriteConst<bool>;
    template class ActionWriteConst<char>;
    template class ActionWriteConst<int>;
    template class ActionWriteConst<float>;



    ActionCopyValue::ActionCopyValue(VariableReference&& _source, VariableReference&& _target, size_t _byteSize)
    : m_source(std::move(_source)), m_target(std::move(_target)), m_byteSize(_byteSize) {}

    int ActionCopyValue::execute(ExecutionContext& _context) {
        memcpy(m_target.get_ptr(_context), m_source.get_ptr(_context), m_byteSize);
        return 0;
    }

    const VariableReference& ActionCopyValue::source() const {
        return m_source;
    }

    const VariableReference& ActionCopyValue::target() const {
        return m_target;
    }

    size_t ActionCopyValue::byte_size() const {
        return m_byteSize;
    }



    ActionExecuteActions::ActionExecuteActions(const ActionExecuteActions& _source) {
        for(Action* a : _source.actions()) {
            add_action(a->clone());
        }
    }

    ActionExecuteActions::ActionExecuteActions(std::vector<Action*>&& _actions)
    : m_actions(std::move(_actions)) {}

    ActionExecuteActions::ActionExecuteActions(const std::vector<Action*>& _actions) {
        for(Action* a : _actions) {
            add_action(a->clone());
        }
    }

    ActionExecuteActions::~ActionExecuteActions() {
        for(Action* p : m_actions) {
            delete p;
            p = nullptr;
        }
    }

    const std::vector<Action*> ActionExecuteActions::actions() const noexcept {
        return m_actions;
    }

    void ActionExecuteActions::add_action(Action* _action) {
        m_actions.push_back(_action);
    }



    ActionExpression::ActionExpression(const VariablePlan& _return, std::vector<Action*>&& _actions)
    : m_return(_return), m_executeActions(std::move(_actions)) {}

    ActionExpression::ActionExpression(const ActionExpression& _source)
    : m_return(_source.return_target()), m_executeActions(_source.executer()) {}

    VariablePlan& ActionExpression::return_target() noexcept {
        return m_return;
    }

    const VariablePlan& ActionExpression::return_target() const noexcept {
        return m_return;
    }

    const ActionExecuteActions& ActionExpression::executer() const noexcept {
        return m_executeActions;
    }

    const std::vector<Action*> ActionExpression::actions() const noexcept {
        return m_executeActions.actions();
    }

    int ActionExpression::execute(ExecutionContext& _context) {
        return m_executeActions.execute(_context);
    }

    void ActionExpression::add_action(Action* _action) {
        m_executeActions.add_action(_action);
    }



    template<class T>
    ActionPrintVariable<T>::ActionPrintVariable(VariableReference&& _source)
    : m_ref(std::move(_source)) {}

    template<>
    int ActionPrintVariable<std::string>::execute(ExecutionContext& _context) {
        std::cout << **(reinterpret_cast<std::string**>(m_ref.get_ptr(_context)));
        return 0;
    }

    template<class T>
    int ActionPrintVariable<T>::execute(ExecutionContext& _context) {
        std::cout << *(reinterpret_cast<T*>(m_ref.get_ptr(_context)));
        return 0;
    }

    template class ActionPrintVariable<bool>;
    template class ActionPrintVariable<char>;
    template class ActionPrintVariable<int>;
    template class ActionPrintVariable<float>;
    template class ActionPrintVariable<std::string>;

    int ActionPrintEndl::execute(ExecutionContext& _context) {
        std::cout << '\n';
        return 0;
    }

    int ActionPrintFlush::execute(ExecutionContext& _context) {
        std::cout.flush();
        return 0;
    }



    ActionWriteConstStringPtr::ActionWriteConstStringPtr(VariableReference&& _varRef, std::string_view _str)
    : m_ref(std::move(_varRef)), m_str(_str) {}

    int ActionWriteConstStringPtr::execute(ExecutionContext& _context) {
        *(reinterpret_cast<std::string**>(m_ref.get_ptr(_context))) = &m_str;
        return 0;
    }



    template<class T1, class T2>
    ActionCastValue<T1, T2>::ActionCastValue(VariableReference&& _from, VariableReference&& _to)
    : m_from(std::move(_from)), m_to(std::move(_to)) {}

    template<class T1, class T2>
    int ActionCastValue<T1, T2>::execute(ExecutionContext& _context) {
        const T1& valueSource = *(reinterpret_cast<T1*>(m_from.get_ptr(_context)));
        T2& valueTarget = *(reinterpret_cast<T2*>(m_to.get_ptr(_context)));
        valueTarget = T2(valueSource);
        return 0;
    }

#define ACTION_GENERATE_VALUE_CAST_CLASS(___type1, ___type2) \
    template class ActionCastValue<___type1, ___type2>;      \
    template class ActionCastValue<___type2, ___type1>;

    template class ActionCastValue<int, int>;
    template class ActionCastValue<char, char>;
    template class ActionCastValue<float, float>;
    template class ActionCastValue<bool, bool>;

    ACTION_GENERATE_VALUE_CAST_CLASS(int, float);
    ACTION_GENERATE_VALUE_CAST_CLASS(int, char);
    ACTION_GENERATE_VALUE_CAST_CLASS(char, float);

    template class ActionCastValue<bool, int>;
    template class ActionCastValue<bool, char>;
    template class ActionCastValue<bool, float>;



    template<class T>
    ActionSpecialBoolCast<T>::ActionSpecialBoolCast(VariableReference&& _from, VariableReference&& _to)
    : m_from(std::move(_from)), m_to(std::move(_to)) {}

    template<class T>
    int ActionSpecialBoolCast<T>::execute(ExecutionContext& _context) {
        const T& valueSource = *(reinterpret_cast<T*>(m_from.get_ptr(_context)));
        bool& writeTarget = *(reinterpret_cast<bool*>(m_to.get_ptr(_context)));
        writeTarget = (valueSource == 0) ? false : true;
        return 0;
    }

    template class ActionSpecialBoolCast<bool>;
    template class ActionSpecialBoolCast<char>;
    template class ActionSpecialBoolCast<int>;
    template class ActionSpecialBoolCast<float>;

    ActionInvertBool::ActionInvertBool(VariableReference&& _var, VariableReference&& _writeTarget)
    : m_var(std::move(_var)), m_writeTarget(std::move(_writeTarget)) {}

    int ActionInvertBool::execute(ExecutionContext& _context) {
        const bool& var = *(reinterpret_cast<bool*>(m_var.get_ptr(_context)));
        bool& writeTarget = *(reinterpret_cast<bool*>(m_writeTarget.get_ptr(_context)));
        writeTarget = !var;
        return 0;
    }



    template<class T, bool ___positive>
    ActionIncrementValue<T, ___positive>::ActionIncrementValue(VariableReference&& _writeTarget)
    : m_writeTarget(_writeTarget) {}

#define REMI_EXPAND_ACTION_INCREMENT_VALUE(___type)                                             \
    template<>                                                                                  \
    int ActionIncrementValue<___type, true>::execute(ExecutionContext& _context) {              \
        ___type& writeTarget = *(reinterpret_cast<___type*>(m_writeTarget.get_ptr(_context)));  \
        writeTarget += 1;                                                                       \
        return 0;                                                                               \
    }                                                                                           \
                                                                                                \
    template<>                                                                                  \
    int ActionIncrementValue<___type, false>::execute(ExecutionContext& _context) {             \
        ___type& writeTarget = *(reinterpret_cast<___type*>(m_writeTarget.get_ptr(_context)));  \
        writeTarget -= 1;                                                                       \
        return 0;                                                                               \
    }                                                                                           \
                                                                                                \
    template class ActionIncrementValue<___type, true >;                                        \
    template class ActionIncrementValue<___type, false>;

    REMI_EXPAND_ACTION_INCREMENT_VALUE(bool)
    REMI_EXPAND_ACTION_INCREMENT_VALUE(char)
    REMI_EXPAND_ACTION_INCREMENT_VALUE(int)
    REMI_EXPAND_ACTION_INCREMENT_VALUE(float)

#undef REMI_EXPAND_ACTION_INCREMENT_VALUE



    template<class T, RemiOperator op>
    ActionBinaryOperation<T, op>::ActionBinaryOperation(VariableReference&& _varRefLeft, VariableReference&& _varRefRight, VariableReference&& _refReturn)
    : m_refLeft(std::move(_varRefLeft)), m_refRight(std::move(_varRefRight)), m_refReturn(std::move(_refReturn)) {}

    template<class T, RemiOperator op>
    const VariableReference& ActionBinaryOperation<T, op>::left_ref() const {
        return m_refLeft;
    }

    template<class T, RemiOperator op>
    const VariableReference& ActionBinaryOperation<T, op>::right_ref() const {
        return m_refRight;
    }

    template<class T, RemiOperator op>
    const VariableReference& ActionBinaryOperation<T, op>::return_ref() const {
        return m_refReturn;
    }

    template<>
    int ActionBinaryOperation<float, RemiOperator::Modulo>::execute(ExecutionContext& _context) { return 0; }

    template<>
    int ActionBinaryOperation<bool, RemiOperator::Modulo>::execute(ExecutionContext& _context) { return 0; }

    template<>
    int ActionBinaryOperation<int, RemiOperator::Not>::execute(ExecutionContext& _context) { return 0; }

    template<>
    int ActionBinaryOperation<float, RemiOperator::Not>::execute(ExecutionContext& _context) { return 0; }

#define REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(___type, ___op, ___op_symbol)    \
    template<>                                                                                  \
    int ActionBinaryOperation<___type, ___op>::execute(ExecutionContext& _context) {            \
        const ___type& left  = *(reinterpret_cast<___type*>(m_refLeft.get_ptr(_context)));      \
        const ___type& right = *(reinterpret_cast<___type*>(m_refRight.get_ptr(_context)));     \
        ___type& ret = *(reinterpret_cast<___type*>(m_refReturn.get_ptr(_context)));            \
        ret = left ___op_symbol right;                      \
        return 0;                                           \
    }                                                       \
                                                            \
    template class ActionBinaryOperation<___type, ___op>;

#define REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, ___op, ___op_symbol)     \
    template<>                                                                              \
    int ActionBinaryOperation<___type, ___op>::execute(ExecutionContext& _context) {        \
        const ___type& left  = *(reinterpret_cast<___type*>( m_refLeft.get_ptr(_context))); \
        const ___type& right = *(reinterpret_cast<___type*>(m_refRight.get_ptr(_context))); \
        bool& ret = *(reinterpret_cast<bool*>(m_refReturn.get_ptr(_context)));              \
        ret = left ___op_symbol right;                      \
        return 0;                                           \
    }                                                       \
                                                            \
    template class ActionBinaryOperation<___type, ___op>;

#define REMI_EXPAND_BINARY_ARITHMETIC_ACTIONS(___type) \
    REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(___type, RemiOperator::Add,            + ) \
    REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(___type, RemiOperator::Subtract,       - ) \
    REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(___type, RemiOperator::Multiply,       * ) \
    REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(___type, RemiOperator::Divide,         / )

#define REMI_EXPAND_BINARY_LOGIC_ACTIONS(___type) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::Less,           < ) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::LessOrEqual,    <=) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::Greater,        > ) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::GreaterOrEqual, >=) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::And,            &&) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::Or,             ||) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::Equal,          ==) \
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(___type, RemiOperator::NotEqual,       !=)

#define REMI_EXPAND_BINARY_ACTION_OPERATIONS_FOR_TYPE(___type) \
   REMI_EXPAND_BINARY_ARITHMETIC_ACTIONS(___type)             \
   REMI_EXPAND_BINARY_LOGIC_ACTIONS(___type)
    
    REMI_EXPAND_BINARY_ACTION_OPERATIONS_FOR_TYPE(int)
    REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE(int, RemiOperator::Modulo, %)

    REMI_EXPAND_BINARY_ACTION_OPERATIONS_FOR_TYPE(char)
    REMI_EXPAND_BINARY_ACTION_OPERATIONS_FOR_TYPE(float)

    // =========================== BOOL ================================
    REMI_EXPAND_BINARY_LOGIC_ACTIONS(bool)
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(bool, RemiOperator::Add,      + )
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(bool, RemiOperator::Subtract, - )
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(bool, RemiOperator::Multiply, &&)
    REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE(bool, RemiOperator::Divide,   / )

#undef REMI_EXPAND_BINARY_ACTION_ARITHMETIC_OPERATION_EXECUTE
#undef REMI_EXPAND_BINARY_ACTION_LOGIC_OPERATION_EXECUTE
#undef REMI_EXPAND_BINARY_ARITHMETIC_ACTIONS
#undef REMI_EXPAND_BINARY_LOGIC_ACTIONS
#undef REMI_EXPAND_BINARY_ACTION_OPERATION_FOR_TYPE

    ActionIfStatement::ActionIfStatement(): m_finalElse(nullptr) {}

    ActionIfStatement::ActionIfStatement(const ActionIfStatement& _source)
    : m_finalElse(new ActionExecuteActions(_source.final_else())) {
        for(auto p : _source.conditional_actions()) {
            m_conditionalActions.push_back(std::make_tuple(std::get<0>(p), (ActionExpression*)(std::get<1>(p)->clone()), (ActionExecuteActions*)(std::get<2>(p)->clone())));
        }
    }

    ActionIfStatement::~ActionIfStatement() {
        for(auto p : m_conditionalActions) {
            delete std::get<1>(p);
            std::get<1>(p) = nullptr;

            delete std::get<2>(p);
            std::get<2>(p) = nullptr;
        }

        delete m_finalElse;
        m_finalElse = nullptr;
    }

    const std::vector<std::tuple<VariableReference, ActionExpression*, ActionExecuteActions*>>& ActionIfStatement::conditional_actions() const {
        return m_conditionalActions;
    }

    const ActionExecuteActions& ActionIfStatement::final_else() const {
        return *m_finalElse;
    }

    void ActionIfStatement::add_conditional_action(VariableReference&& _conditionRef, ActionExpression* conditionExpr, ActionExecuteActions* _actions) {
        m_conditionalActions.push_back(std::make_tuple(std::move(_conditionRef), conditionExpr, _actions));
    }

    void ActionIfStatement::set_final_else(ActionExecuteActions* _actions) {
        m_finalElse = _actions;
    }

    int ActionIfStatement::execute(ExecutionContext& _context) {
        size_t execCount = 0;

        for(auto p : m_conditionalActions) {
            if (std::get<1>(p)->execute(_context)) return 1;
            bool condition = *(reinterpret_cast<bool*>(std::get<0>(p).get_ptr(_context)));

            if (condition) {
                if (std::get<2>(p)->execute(_context)) return 1;
                execCount++;
                break;
            }
        }

        if ((m_finalElse != nullptr) && (execCount == 0)) {
            if (m_finalElse->execute(_context)) return 1;
        }

        return 0;
    }



    ActionWhileLoop::ActionWhileLoop(VariableReference&& _conditionRef, ActionExpression* _conditionExpression, ActionExecuteActions* _actions)
    : m_conditionRef(std::move(_conditionRef)), m_conditionExpression(_conditionExpression), m_actions(_actions) {}

    ActionWhileLoop::ActionWhileLoop(const ActionWhileLoop& _source)
    : m_conditionRef(_source.m_conditionRef),
      m_conditionExpression((ActionExpression*)(_source.m_conditionExpression->clone())),
      m_actions((ActionExecuteActions*)(_source.m_actions->clone())) {}

    ActionWhileLoop::~ActionWhileLoop() {
        delete m_conditionExpression;
        m_conditionExpression = nullptr;

        delete m_actions;
        m_actions = nullptr;
    }

    int ActionWhileLoop::execute(ExecutionContext& _context) {
        bool* condition = (reinterpret_cast<bool*>(m_conditionRef.get_ptr(_context)));

        while(true) {
            if (m_conditionExpression->execute(_context)) return 1;
            if (!(*condition)) break;

            if (m_actions->execute(_context)) return 1;
        }

        return 0;
    }

    ActionDoWhileLoop::ActionDoWhileLoop(VariableReference&& _conditionRef, ActionExpression* _conditionExpression, ActionExecuteActions* _actions)
    : m_conditionRef(std::move(_conditionRef)), m_conditionExpression(_conditionExpression), m_actions(_actions) {}

    ActionDoWhileLoop::ActionDoWhileLoop(const ActionDoWhileLoop& _source)
    : m_conditionRef(_source.m_conditionRef),
      m_conditionExpression((ActionExpression*)(_source.m_conditionExpression->clone())),
      m_actions((ActionExecuteActions*)(_source.m_actions->clone())) {}

    ActionDoWhileLoop::~ActionDoWhileLoop() {
        delete m_conditionExpression;
        m_conditionExpression = nullptr;

        delete m_actions;
        m_actions = nullptr;
    }

    int ActionDoWhileLoop::execute(ExecutionContext& _context) {
        bool* condition = (reinterpret_cast<bool*>(m_conditionRef.get_ptr(_context)));

        do {
            if (m_actions->execute(_context)) return 1;
            if (m_conditionExpression->execute(_context)) return 1;
        } while(*condition);

        return 0;
    }



    ActionFunctionCall::ActionFunctionCall(const FunctionPlan* _fPlan, size_t _blockSize, ActionExecuteActions* _initValues)
    : m_fPlan(_fPlan), m_returnActions(nullptr), m_initValues(_initValues) {}

    ActionFunctionCall::ActionFunctionCall(const ActionFunctionCall& _source)
    : m_fPlan(_source.m_fPlan), m_returnActions(nullptr), m_initValues((ActionExecuteActions*)(_source.m_initValues->clone())) {}

    ActionFunctionCall::~ActionFunctionCall() {
        delete m_initValues;
        m_initValues = nullptr;

        if (m_returnActions != nullptr) {
            delete m_returnActions;
            m_returnActions = nullptr;
        }
    }

    void ActionFunctionCall::set_return_target(ActionExecuteActions* _actions) {
        m_returnActions = _actions;
    }

    int ActionFunctionCall::execute(ExecutionContext& _context) {
        _context.init_function_call_context(*m_fPlan);
        m_initValues->execute(_context);

        _context.execute_current_function_delayed_end();
        if (m_returnActions != nullptr) m_returnActions->execute(_context);
        _context.remove_current_function();

        _context.end_function_call_context();
        return 0;
    }



    ActionAbortFunctionCall::ActionAbortFunctionCall() {}

    int ActionAbortFunctionCall::execute(ExecutionContext& _context) {
        return 1;
    }

}