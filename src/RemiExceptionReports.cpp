#include <Remi/Remi.hpp>

#include <Remi/Default.hpp>

namespace re {

    void Remi::report_program_argument_lookup_exception(std::string_view _functionName, ProgramArgumentLookupException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Remi::start()]:\n";
    #endif
        ls << "[Error]: Program argument lookup error:\n";
        ls << " -> " << _e.what() << "\n -> Argument is " << _e.requested();

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Encountered a problem with a program argument lookup operation"));
    }



    void Remi::report_program_argument_input_exception(std::string_view _functionName, ProgramArgumentInputException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::ProgramArguments @" << &_e << "]:\n";
    #endif
        ls << "[Error]: Program argument input error:\n";
        ls << " -> " << _e.what() << '\n';
        ls << " -> Thrown for input \"" << _e.input() << "\"\n";
        ls << " -> Pass [-h] for a list of all options\n";
        
        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Encountered a problem with given program argument input"));
    }



    void Remi::report_program_argument_use_exception(std::string_view _functionName, ProgramArgumentUseException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::ProgramArguments @" << &_e << "]:\n";
    #endif
        ls << "[Error]: Program argument usage error:\n";
        ls << " -> " << _e.what() << "\n -> Flag [-"    << _e.ref().flag() << "] expects ";
        ls << _e.ref().arguments().size() << " argument";
        /*ls << " -> " << _e.what() << "\n -> Expected argument";*/

        if (_e.ref().arguments().size() > 1) {
            ls << "s";
        }
        /*else {
            ls << " is ";
        }*/

        ls << ": ";

        for(size_t i{0}; i < (_e.ref().arguments().size() - 1); i++) {
            ls << _e.ref().arguments()[i] << ", ";
        }

        ls << _e.ref().arguments().back() << "\n -> Pass [-h] for a list of all options\n";

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Encountered a program argument usage error"));
    }



    void Remi::report_program_argument_exception(std::string_view _functionName, ProgramArgumentException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::ProgramArguments @" << &_e << "]:\n";
    #endif
        ls << "[Error]: Program argument error:\n";
        ls << " -> " << _e.what() << '\n';
        ls << " -> Pass [-h] for a list of all options\n";

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Encountered a problem with given program arguments"));
    }



    void Remi::report_script_exception(std::string_view _functionName, ScriptException& _e) const {
        LogStream ls(*m_logger);

        const Script* p(&_e.ref());

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Script @" << &p << "]:\n";
    #endif

        if (_e.exception_stack().depth() == 1) {
            ls << "[Error]: Error in script " << _e.ref().filename() << '\n';
            ls << " -> " << _e.what() << '\n';
        }

        ls << "Script inclusion order:";
        ls << "\n | [Source] " << p->filename() << ((p->parent() == nullptr) ? " <main>" : "") << '\n';

        // Print the script inclusion order to make error tracing easier for nested scripts
        while(p->parent() != nullptr) {
            p = p->parent();
            ls << " | -> Included from " << p->filename() << ((p->parent() == nullptr) ? " <main>" : "") << '\n';
        }

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Script " + std::string(_e.ref().filename()) + std::string((_e.ref().parent() == nullptr) ? " [main]" : "") + " produced a fatal error"));
    }



    void Remi::report_script_lookup_exception(std::string_view _functionName, ScriptLookupException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::Context @" << &_e.ref() << "]:\n";
    #endif

        ls << "[Error]: Script lookup error:\n";
        ls << " -> " << _e.what() << '\n';
        ls << " -> " << "Requested script name: \"" << _e.script_name() << "\"\n";

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Failed to find a script"));
    }

    void Remi::report_scope_lookup_exception(std::string_view _functionName, ScopeLookupException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::ScopeSpace @" << &_e.ref() << "]:\n";
    #endif

        ls << "[Error]: Scope lookup error:\n";
        ls << " -> " << _e.what() << '\n';
        ls << " -> " << "Requested scope name: \"" << _e.target_name() << "\"\n";

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Failed to find a scope"));
    }



    void Remi::report_scope_duplicate_exception(std::string_view _functionName, ScopeDuplicateException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << ", re::ScopeSpace @" << &_e.ref() << "]:\n";
    #endif

        ls << "[Error]: Scope registration error:\n";
        ls << " -> " << _e.what() << '\n';
        ls << " -> " << "Requested scope name: \"" << _e.target_name() << "\"\n";

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Failed to register a scope"));
    }



    void Remi::report_variable_lookup_exception(std::string_view _variableName, VariableLookupException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << "]:\n";
    #endif

        ls << "[Error]: Variable lookup error:\n";
        ls << " -> " << _e.what() << '\n';

        if (_e.target_name() != "main") {
            ls << " -> " << "Requested variable name: \"" << _e.target_name() << "\"\n";
        }

        _e.exception_stack().add_name(_e, _variableName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Error on variable lookup"));
    }



    void Remi::report_function_lookup_exception(std::string_view _functionName, FunctionLookupException& _e) const {
        LogStream ls(*m_logger);

    #if REMI_PRINT_ERROR_CLASS_INFO
        ls << "[->] [re::Remi @" << this << "]:\n";
    #endif

        ls << "[Error]: Function lookup error:\n";
        ls << " -> " << _e.what() << '\n';

        if (_e.target_name() != "main") {
            ls << " -> " << "Requested function name: \"" << _e.target_name() << "\"\n";
        }

        _e.exception_stack().add_name(_e, _functionName);
        throw(ProgramException(std::move(_e.exception_stack_ptr()), "Error on function lookup"));
    }



}