#include <Remi/Parser.hpp>

namespace re {

    ActionExpression* Parser::parse_expression(ScriptTraverser& _traverser, Scope& _scope, VariablePlan* _directReturn) const {
        VariablePlan ret = VariablePlan::any_type(_scope);
        ActionExpression* actionRet = nullptr;

        std::vector<Action*> actions;

        if (_directReturn != nullptr) {
            ret = VariablePlan(*_directReturn);
        }

        VariablePlan& rType = parse_subexpression(_traverser, actions, _scope, ret);

        if (ret.type_id() == REMI_ANY_TYPE) {
            actionRet = new ActionExpression(rType, std::move(actions));
        }
        else if (_directReturn->can_equate(rType)) {
            actionRet = new ActionExpression(rType, std::move(actions));
        }
        else {
            VariablePlan& rt = generate_value_type_cast(_traverser, actions, _scope, rType, ret, true);
            actionRet = new ActionExpression(rt, std::move(actions));
        }

        return actionRet;
    }

    VariablePlan& Parser::parse_subexpression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const {
        VariablePlan* left = nullptr;
        bool positive = true;

        if (_traverser.current_token() == RemiOperator::Subtract) {
            positive = false;
            _traverser.next();
        }

        if (_traverser.current_token() == '(') {
            _traverser.next();
            left = &parse_subexpression(_traverser, _actionVector, _scope, _directReturn);

            _traverser.expect_symbol(')');
            _traverser.next();
        }
        else if (_traverser.current_token().is_numeric()) {
            left = &parse_numeric_const_expression(_traverser, _actionVector, _scope, _directReturn);
        }
        else if (m_targetContext.type_exists(_traverser.current_token().str()) || (_traverser.current_token() == RemiKeyword::Const)) {
            size_t desiredType = m_targetContext.find_type_id(_traverser.current_token().str());
            VariablePlan& castedVar = generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Mutable, ValueType::Value, desiredType), _scope);

            _traverser.next();
            _traverser.expect_symbol('(');
            _traverser.next();

            VariablePlan& varToCast = parse_subexpression(_traverser, _actionVector, _scope, castedVar);
            castedVar = generate_value_type_cast(_traverser, _actionVector, _scope, varToCast, castedVar, true);

            _traverser.expect_symbol(')');
            _traverser.next();
            left = &castedVar;
        }
        else if ((_traverser.current_token() == RemiKeyword::True) || (_traverser.current_token() == RemiKeyword::False)) {
            bool truth = (_traverser.current_token() == RemiKeyword::True) ? true : false;
            _traverser.next();

            if (_directReturn.type_id() == REMI_BOOL_TYPE) {
               _actionVector.push_back(new ActionWriteConst<bool>(generate_variable_reference(_directReturn, _scope), truth));
               left = &_directReturn;
            }
            else {
                left = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, REMI_BOOL_TYPE), _scope);
               _actionVector.push_back(new ActionWriteConst<bool>(generate_variable_reference(*left, _scope), truth));
            }
        }
        else if (_traverser.current_token() == '!') {
            VariablePlan* writeTarget = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), _scope);
            _traverser.next();
            
            VariablePlan& var = parse_subexpression(_traverser, _actionVector, _scope, *writeTarget);

            if (var.type_id() != REMI_BOOL_TYPE)
                var = generate_value_type_cast(_traverser, _actionVector, _scope, var, *writeTarget, true);

            _actionVector.push_back(new ActionInvertBool(generate_variable_reference(var, _scope), generate_variable_reference(*writeTarget, _scope)));
            left = writeTarget;
        }
        else if ((_traverser.current_token() == RemiOperator::Increment) || (_traverser.current_token() == RemiOperator::Decrement)) {
            RemiOperator op = remi_find_operator(_traverser.current_token().str());
            _traverser.next();

            left = &generate_anonymous_variable(VariablePlan::any_type(_scope), _scope);
            left = &increment_variable(_traverser, parse_subexpression(_traverser, _actionVector, _scope, *left), _actionVector, _scope, op == RemiOperator::Increment);
        }
        else if (_traverser.current_token() == '$') {
            left = &parse_engine_directive(_traverser, _actionVector, _scope);
        }
        else if (remi_is_identifier(_traverser.current_token().str())) {
            std::string name(_traverser.current_token().str());

            if (_scope.variable_exists(name, SearchType::Deep)) {
                left = &_scope.find_variable(name, SearchType::Deep);
                _traverser.next();
            }
            else if (_scope.function_exists(name, SearchType::Deep)) {
                FunctionPlan& fPlan = _scope.find_function(name, SearchType::Deep);
                _traverser.next();

                left = &generate_anonymous_variable(VariablePlan(fPlan.type_plan().return_type()), _scope);
                _actionVector.push_back(parse_function_call(_traverser, name, _scope, left));
            }
            else {
                _traverser.throw_standard_parse_error("re::Parser::parse_subexpression()", "Unknown variable or function " + name + " referenced");
            }
        }
        else if (_traverser.current_token() == '\"') {
            left = &parse_string_const_expression(_traverser, _actionVector, _scope, _directReturn);
        }
        else if (_traverser.current_token() == '\'') {
            left = &parse_char_const_expression(_traverser, _actionVector, _scope, _directReturn);
        }
        else if (_traverser.current_token() == RemiKeyword::If) {
            ParseSnapshot snapshot(_traverser);
            _traverser.next();

            left = &_directReturn;

            std::unique_ptr<ActionIfStatement> ifAction = std::make_unique<ActionIfStatement>();
            std::vector<Action*> trueActions;
            std::vector<Action*> falseActions;

            ActionExpression* condition = parse_expression(_traverser, _scope, &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Mutable, ValueType::Value, REMI_BOOL_TYPE), _scope));
            _traverser.expect_keyword(RemiKeyword::Then);
            _traverser.next();

            VariablePlan& trueCase = parse_subexpression(_traverser, trueActions, _scope, _directReturn);
            _traverser.expect_keyword(RemiKeyword::Else);
            _traverser.next();

            if (trueCase.byte_offset() != left->byte_offset())
                trueActions.push_back(new ActionCopyValue(generate_variable_reference(trueCase, _scope), generate_variable_reference(*left, _scope), m_targetContext.get_type_byte_size(trueCase.type_id())));

            VariablePlan& falseCase = parse_subexpression(_traverser, falseActions, _scope, _directReturn);
            if (falseCase.byte_offset() != left->byte_offset())
                falseActions.push_back(new ActionCopyValue(generate_variable_reference(falseCase, _scope), generate_variable_reference(*left, _scope), m_targetContext.get_type_byte_size(falseCase.type_id())));

            ifAction->add_conditional_action(generate_variable_reference(condition->return_target(), _scope), condition, new ActionExecuteActions(trueActions));
            ifAction->set_final_else(new ActionExecuteActions(falseActions));

            _actionVector.push_back(ifAction.release());

            if (_traverser.current_token() != ';') {
                std::ostringstream ss;
                ss << "Expected the symbol \';\' after inline if-then-else expression\n";
                ss << " -> Expression starts on line " << snapshot.line.number() << ":\n\nHere:\n: ";
                ss << generate_annotated_error_str(snapshot.line.source(), snapshot.line[snapshot.tokenPos], 2) << "\n\n";
                ss << "Error caught here (line " << _traverser.current_line().number() << "):";
                _traverser.throw_standard_parse_error("re::Parser::parse_subexpression()", ss.str());
            }
            _traverser.next();
        }

        if (left == nullptr)
            _traverser.throw_standard_parse_error("re::Parser::parse_subexpression()", "Unsupported expression form in subexpression");

        if (!positive) left = &apply_negative_sign(_traverser, *left, _actionVector, _scope);

        if (_traverser.current_token().is_operator()) {
            RemiOperator op = remi_find_operator(_traverser.current_token().str());

            if (left == nullptr) _traverser.throw_standard_parse_error("re::Parser::parse_subexpression()", "Assignment to left-hand-value that failed to parse");
            _traverser.next();

            if (remi_is_assign_variant(op)) {
                if (op == RemiOperator::AddAssign)      op = RemiOperator::Add;
                if (op == RemiOperator::SubtractAssign) op = RemiOperator::Subtract;
                if (op == RemiOperator::MultiplyAssign) op = RemiOperator::Multiply;
                if (op == RemiOperator::DivideAssign)   op = RemiOperator::Divide;

                if (op == RemiOperator::Assign) {
                    VariablePlan& ret = parse_subexpression(_traverser, _actionVector, _scope, *left);

                    if (ret.byte_offset() != left->byte_offset())
                        _actionVector.push_back(new ActionCopyValue(generate_variable_reference(ret, _scope), generate_variable_reference(*left, _scope), m_targetContext.get_type_byte_size(ret.type_id())));

                    return *left;
                }

                return generate_binary_operation(_traverser, _actionVector, _scope, *left, op, *left);
            }

            left = &generate_binary_operation(_traverser, _actionVector, _scope, *left, op, _directReturn);
        }

        return *left;
    }

    VariablePlan& Parser::parse_numeric_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, bool _positive) const {
        std::stringstream ss;

        if (!_positive) ss << '-';
        ss << _traverser.current_token().str(); // First number/digit

        _traverser.next();

        if (_traverser.current_token() == '.') {
            ss << '.';
            _traverser.next();

            if (_traverser.current_token().is_numeric()) {
                ss << _traverser.current_token().str();
                _traverser.next();
            }
            else
                _traverser.throw_standard_parse_error("re::Parser::parse_numeric_const_expression()", "Expected a number or digit after decimal dot");

            return numeric_const_expression_generic<float>(_actionVector, _scope, _directReturn, REMI_FLOAT_TYPE, ss);
        }
        else {
            return numeric_const_expression_generic<int>(_actionVector, _scope, _directReturn, REMI_INT_TYPE, ss);
        }
    }

    template<class T>
    VariablePlan& Parser::numeric_const_expression_generic(std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const {
        VariablePlan* ret = nullptr;

        if (_directReturn.type_id() == REMI_ANY_TYPE)
            ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, _typeID), _scope);
        else if (_directReturn.can_equate(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, _typeID)))
            ret = &_directReturn;
        else ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, _typeID), _scope);

        T data;
        _ss >> data;

        _actionVector.push_back(new ActionWriteConst<T>(generate_variable_reference(*ret, _scope), data));
        return (*ret);
    }

    template VariablePlan& Parser::numeric_const_expression_generic<bool> (std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const;
    template VariablePlan& Parser::numeric_const_expression_generic<char> (std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const;
    template VariablePlan& Parser::numeric_const_expression_generic<int>  (std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const;
    template VariablePlan& Parser::numeric_const_expression_generic<float>(std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn, size_t _typeID, std::stringstream& _ss) const;

    VariablePlan& Parser::parse_char_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const {
        VariablePlan* ret = nullptr;
        std::string_view str;

        _traverser.expect_symbol('\'');
        _traverser.next();

        str = _traverser.current_token().str();

        if (str.size() != 1)
            _traverser.throw_standard_parse_error("re::Parser::parse_numeric_const_expression()", "Expected a single character for character constant");

        if ((_directReturn.type_id() == REMI_CHAR_TYPE) && (_directReturn.value_type() != ValueType::Array)) {
            ret = &_directReturn;
        }
        else {
            ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Value, REMI_CHAR_TYPE), _scope);
        }

        _actionVector.push_back(new ActionWriteConst<char>(generate_variable_reference(*ret, _scope), str.front()));
        _traverser.next();

        _traverser.expect_symbol('\'');
        _traverser.next();

        return *ret;
    }

    VariablePlan& Parser::parse_string_const_expression(ScriptTraverser& _traverser, std::vector<Action*>& _actionVector, Scope& _scope, VariablePlan& _directReturn) const {
        VariablePlan* ret = nullptr;
        std::string_view str;

        _traverser.expect_symbol('\"');
        _traverser.next();

        str = _traverser.current_token().str();

        if ((_directReturn.type_id() == REMI_CHAR_TYPE) && (_directReturn.value_type() == ValueType::Array)) {
            ret = &_directReturn;
        }
        else {
            ret = &generate_anonymous_variable(VariablePlan::normal_variable(_scope, Constness::Const, ValueType::Array, REMI_CHAR_TYPE), _scope);
        }

        _actionVector.push_back(new ActionWriteConstStringPtr(generate_variable_reference(*ret, _scope), str));
        _traverser.next();

        _traverser.expect_symbol('\"');
        _traverser.next();

        return *ret;
    }



}