#include <Remi/FunctionCallContext.hpp>

namespace re {

    FunctionCallContext::FunctionCallContext(size_t _callerFunction, size_t _calledFunction)
    : m_callerFunction(_callerFunction), m_calledFunction(_calledFunction) {}

    size_t FunctionCallContext::caller_function() const noexcept {
        return m_callerFunction;
    }

    size_t FunctionCallContext::called_function() const noexcept {
        return m_calledFunction;
    }

}