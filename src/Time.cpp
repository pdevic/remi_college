#include <Remi/Time.hpp>

#include <ctime>

namespace re {

    Time::Time() {
        update();
    }

    Time& Time::update() {
        std::time_t time_t(std::time(0));
        std::tm* now(std::localtime(&time_t));

        m_time[0] = now->tm_sec;
        m_time[1] = now->tm_min;
        m_time[2] = now->tm_hour;

        m_time[3] = now->tm_mday;
        m_time[4] = now->tm_mon + 1;
        m_time[5] = now->tm_year + 1900;

        return (*this);
    }

    uint Time::operator[](TimeComponent _component) const {
        return m_time[_component];
    }

    uint& Time::operator[](TimeComponent _component) {
        return m_time[_component];
    }

    Time Time::now() {
        return Time();
    }

    uint Time::second() const {
        return m_time[TimeComponent::Second];
    }

    uint Time::minute() const {
        return m_time[TimeComponent::Minute];
    }

    uint Time::hour() const {
        return m_time[TimeComponent::Hour];
    }

    uint Time::day() const {
        return m_time[TimeComponent::Day];
    }

    uint Time::month() const {
        return m_time[TimeComponent::Month];
    }

    uint Time::year() const {
        return m_time[TimeComponent::Year];
    }

}