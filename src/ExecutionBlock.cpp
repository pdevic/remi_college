#include <Remi/ExecutionBlock.hpp>

#include <Remi/Action.hpp>

#include<sstream>

namespace re {

    ExecutionBlock::ExecutionBlock(size_t _depth, size_t _id, size_t _callDepth, size_t _byteSize)
    : m_byteSize(_byteSize), m_depth(_depth), m_id(_id), m_callDepth(_callDepth) {
        m_memoryBlock = std::make_unique<std::byte[]>(m_byteSize);

#if REMI_PRINT_BLOCK_INIT
        std::cout << "Block init @" << coords_str() << std::endl;
#endif
    }

    std::string ExecutionBlock::coords_str() const {
        std::ostringstream ss;

        ss << m_depth << ':' << m_id << ':' << m_callDepth;
        return ss.str();
    }

}